import com.rabbitmq.client.*;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.KuduClient;
import org.junit.Assert;
import org.junit.Test;

import zz.kudu.*;
import zz.kudu.sql.KuduSQLLexer;
import zz.kudu.sql.KuduSQLParser;
import zz.kudu.sql.SelectResponse;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import zz.kudu.Statement;

public class TestCases {

    private static final String KUDU_MASTER = System.getProperty(
            "kuduMaster", "localhost");

//    @Test
//    public void testKuduSQLParser() {
//
//        String testStatements = "Create table someTable1(field1 float Primary key default 1.5e3, field2 float ) ;" +
//                "Create table someTable2(field1 int8 key null, field2 string default \"some\'Def Value\" Primary key )" +
//                "Create table someTable3(field1 bool default true, field2 string default null Primary key )" +
//                "Drop table someTable1";
//        KuduSQLLexer lexer = new KuduSQLLexer(CharStreams.fromString(testStatements));
//        CommonTokenStream tokens = new CommonTokenStream(lexer);
//        KuduSQLParser parser = new KuduSQLParser(tokens);
//
//        System.out.println("===================");
//
//        List<Statement> statements = parser.getStatements();
//
//        for (Statement statement : statements) {
//
//            switch (statement.getType()) {
//
//                case CREATE_TABLE:
//                    System.out.println("----------Create Table Statement----------");
//                    System.out.println("\tTable name: " + statement.getTableName());
//                    Object[] shemaAndOptions = (Object[]) statement.getOptions();
//                    Schema schema = (Schema) shemaAndOptions[0];
//                    List<ColumnSchema> columnSchemas = schema.getColumns();
//
//                    for (ColumnSchema columnSchema : columnSchemas) {
//
//                        System.out.println("\t\tColumn: " + columnSchema.getName() +
//                                "; " + columnSchema.getType() +
//                                "; isKey: " + columnSchema.isKey() +
//                                "; isNullable: " + columnSchema.isNullable() +
//                                "; defaultValue: " + columnSchema.getDefaultValue());
//                    }
//                    break;
//
//                case DROP_TABLE:
//                    System.out.println("----------Drop Table Statement----------");
//                    System.out.println("\tTable name: " + statement.getTableName());
//                    break;
//            }
//        }
//
//        System.out.println("===================");
//
//        Assert.assertEquals("OK", "OK");
//
//    }

//    @Test
//    public void testStatementExecutor() {
//
////                String testStatements =
////                "drop table testTable1; Create table testTable1(field1 int32 primary key, field2 string, field3 double, field4 double ); \n";
////
////        SimpleDateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss:SS");
////        StringBuilder sb = new StringBuilder("");
////        for(int i = 900001; i <= 1000000; i++) {
////
////            Calendar calendar = new GregorianCalendar();
////            sb.append("insert into testTable1( field1, field2, field3, field4 ) values( ");
////            sb.append(i);
////            sb.append(", '");
////            sb.append(format.format(calendar.getTime()));
////            sb.append("', ");
////            sb.append((double)calendar.getTimeInMillis());
////            sb.append(", ");
////            sb.append(Math.random() * 10l);
////            sb.append(" ); \n");
////        }
////
////        String testStatements = "";
////
////        testStatements += sb.toString();
////        System.out.println(testStatements);
//
////        String testStatements =
////                "Create table someTable1(field1 string Primary key default 'key1', field2 float, field3 string not null ) ;"
////                        +
////                        "insert into someTable1( field1, field2 ) VAlues( '', 4 )"
////                        +
////                        "Drop table someTable1";
//
////        String testStatements =
////                "Create table someTable1("
////                        + "field1 string Primary key default 'key1'"
////                        + ", field2 float"
////                        + ", field3 string"
////                        + ", field4 string )"
////
////                        +
////                          "insert into someTable1( field1, field2, field3, field4 ) VAlues( 'key9', -1.5, 'strValue9', 'strValue' )"
////                        + "insert into someTable1( field1, field2, field3, field4 ) VAlues( 'key10', -2.5, 'strValue10', 'strValue' )"
////                        + "insert into someTable1( field1, field2, field3, field4 ) VAlues( 'key11', -3.5, 'strValue11', 'strValue' )"
////                        + "insert into someTable1( field1, field2, field3, field4 ) VAlues( 'key12', -4.5, 'strValue12', 'strValue' )";
//
////        String testStatements =
////                "Select as columns field1, field2, field3, field4 from someTable1"
//////                + " where field1 = 'key3' && field3 = 'strValue' and field2 >= 2.5 and field1 in('key3', 'key4')"
////                ;
//
////        String testStatements = "Select field1, field2, field3, field4 from testTable1 where field1 <= 10000 and field3 is not null order by field3, field4 asc ; \n";
////        String testStatements = "Select sum(7), count(7), avg(7), min(7), max(7), sum(field4), min(field2), max(field2), min(field1), max(field1), count(field2), avg(field1) as f1, field2 f2, 31 as numConst, avg(7.77) from testTable1 where field1 <= 10 group by f2, numConst  order by field2 desc, f1 asc ; \n";
//
////        String testStatements = "Select count(field1), avg(field1) b, sum(field1) f1, field2 f2 from testTable1 where field1 <= 1000 and field3 is null group by f2 order by count(field1) desc, b desc ";
////        String testStatements = "Select count(1), sum(1), min(field1), max(field1), sum(field1), avg(field1) from testTable1 ; ";
//
////        String testStatements = "Select t1.field1, t1.field2, t1.field3, t1.field4,    t2.field1, t2.field2, t2.field3, t2.field4 from testTable1 t1 inner join testTable1 t2 on t1.field1 = t2.field1 where t1.field1 <= 1000 and t2.field1 <= 1000 ; \n";
////        String testStatements = "Select t1.field1, t1.field2, t1.field3, t1.field4,    t2.field1, t2.field2, t2.field3, t2.field4 from testTable1 t1, testTable1 t2 where t1.field1 <= 1000 and (t1.field1 = t2.field1 or t2.field1 <= 1) and t2.field1 <= 1000 ; \n";
////        String testStatements = "Select ta.in1 out1 from select t1.field1 as in1 from testTable1 t1 where t1.field1 >= 10 ta where ta.in1 <= 10 ; \n";
//
////        String testStatements = "Select ta.in1 out1, ta.field2, j1.field1, j1.field2 from testTable1 j1 inner join (select max(t1.field1) as in1, field2 as in2 from testTable1 t1 where t1.field1 <= 10 group by field2) ta on j1.field1=ta.in1 where ta.in2 in ('NULLS', 'Thu, 14 Sep 2017 14:36:38:151') ; \n";
////        String testStatements = "Select ta.in1 out1, ta.field2, j1.field1, j1.field2 from testTable1 j1 inner join (select t1.field1 as in1, field2 as in2 from testTable1 t1 where t1.field1 <= 10) ta on j1.field1=ta.in1 where ta.in2 in ('NULLS', 'Thu, 14 Sep 2017 14:36:38:151') ; \n";
////        String testStatements = "Select j1.*, ta.* from testTable1 j1 inner join (select max(t1.field1) as in1, field2 as in2 from testTable1 t1 where t1.field1 <= 10 group by field2) ta on j1.field1=ta.in1 where ta.in2 in ('NULLS', 'Thu, 14 Sep 2017 14:36:38:151') ; \n";
////        String testStatements = "Select j1.*, ta.*, * from testTable1 j1 inner join (select max(t1.field1) in1, count(t1.field1), field2 as in2 from testTable1 t1 where t1.field1 <= 10 group by field2) ta on j1.field1=ta.in1 where ta.in2 in ('NULLS', 'Thu, 14 Sep 2017 14:36:38:151') ; \n";
////        String testStatements = "Select * from testTable1 j1 join (select max(t1.field1) in1, count(t1.field1), field2 as in2 from testTable1 t1 where t1.field1 <= 10 group by field2) ta on j1.field1=ta.in1 where ta.in2 in ('NULLS', 'Thu, 14 Sep 2017 14:36:38:151') ; \n";
//
////        String testStatements = "select * from someTable1, someTable1 ; ";
////        String testStatements = " select count(field1), field2 from testTable1 t1 where field1 <= 10 group by field2 ; ";
//
////        String testStatements = "select t.field1 f from testTable1 t where f <= 10 ; \n";
////        String testStatements = "Select t1.field1, t1.field2, t1.field3, t1.field4,    t2.field1, t2.field2, t2.field3, t2.field4 from testTable1 t1 inner join testTable1 t2 on t1.field1 = t2.field1 and t1.field1 < 10 and t2.field1 <= 10 ; \n";
////        String testStatements = "Select t1.field1, t1.field2, t1.field3, t1.field4,    t2.field1, t2.field2, t2.field3, t2.field4 from testTable1 t1 inner join testTable1 t2 on t1.field1 = t2.field1 ; \n";
////        String testStatements = "Select t1.field1, t1.field2, t1.field3, t1.field4,    t2.field1, t2.field2, t2.field3, t2.field4 from testTable1 t1 inner join testTable1 t2 on t1.field1 <= 1000 and t2.field1 <= 1000 where t1.field1 = t2.field1 ; \n";
////        String testStatements = "Select t1.field2, t1.field3, t1.field4,    t2.field2, t2.field3, t2.field4 from testTable1 t1 inner join testTable1 t2 on t1.field1 <= 1000 where t1.field1 = t2.field1 and t2.field1 <= 1000 ; \n";
//
////        String testStatements = "Select sum(field1), field2, avg(field3), avg(field4), max(field4) from testTable1 where field1  <= 10 group by field2 ; \n";
////        String testStatements = "Select sum(t1.field1) f1, sum(t1.field3), t1.field2, sum(t2.field1) from testTable1 t1 left join testTable1 t2 on t1.field1  <= 10 and t2.field1 < -10000 group by t1.field2 ; \n";
//
////        String testStatements = "insert into testTable1(field1, field2) values(0, 'NULLS'); ";
//        System.out.println("===================");
//
////        String testStatements = "select field1, field2, field3, field4 from someTable1 ";
////        String testStatements = "select first.field1, first.field2, first.field3, first.field4, 'DELIM',  second.field1, second.field2, second.field3, second.field4  from someTable1 first left join someTable1 second on  first.field1 in('key1', 'key2')  and second.field1='zkey2' ";
////        String testStatements = "select first.field2, first.field3, first.field4, 'DELIM', second.field2, second.field3, second.field4  from someTable1 first left join someTable1 second on  first.field1 in('key1', 'key2')  and second.field1='zkey2' ";
////        String testStatements = "select max(first.field2), first.field3, first.field4, 'DELIM', avg(second.field2), second.field3, second.field4  from someTable1 first left join someTable1 second on  first.field1 in('key1', 'key2')  and second.field1='zkey2' group by first.field3, first.field4, 'DELIM', second.field3, second.field4 ";
////        String testStatements = "select first.field2, first.field3, first.field4, 'DELIM', second.field2, second.field3, second.field4  from someTable1 first, someTable1 second where  first.field1 in('key1', 'key2')  and second.field1='key2' ";
////        String testStatements = "insert into someTable1(field1, field2, field3, field4) values('key3', 2.2, 'val', 'val') ";
//
////        String testStatements = "select field1, field2 from someTable1 where field2 < 2 ";
//
////        String testStatements = "select     3,      field2,      field2,      field2  from someTable1 ";
////        testStatements += ";     select avg(3), sum(field2), min(field2), max(field2) from someTable1 ";
//
////        String testStatements = "delete from someTable1 where not field1 in('key1', 'key2'); select field1, field2, field3, field4 from someTable1";
////        String testStatements = "update someTable1 set field3 = 'val99', field4 = field3; select field1, field2, field3, field4 from someTable1";
//
//
////        String testStatements = "show tables like 'ome' ; \n";
////        String testStatements = "show tables where name = 'someTable1' ; \n";
////        String testStatements = "show columns in someTable1 where primary_key = true ; \n";
////        String testStatements = "describe someTable1 'field2' ; \n";
//
////        String testStatements = "select t.* from (values 1 as f1, 2 as f2, 3, 4) t(f1, zz, f3) ; \n";
//
//        String testStatements = "Drop table SAMPLE1;" +
//                "create\n" +
//                "table\n" +
//                "SAMPLE1(\n" +
//                "    ID int32 primary key,\n" +
//                "    DIM_X float,\n" +
//                "    DIM_Y float,\n" +
//                "    DIM_Z float,\n" +
//                "    DIM_S float\n" +
//                ");" +
//
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) values ( 0, -10, 100, 0, 1 ),( 1, -10, 100, 0, 1 ),( 2, -10, 100, 0, 1 );" +
////                "insert into SAMPLE1 values ( 0, -10, 100, 0, 1 ),( 1, -10, 100, 0, 1 ),( 2, -10, 100, 0, 1 );" +
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) select * from values ( 0, -10, 100, 0, 1 ),( 1, -10, 100, 0, 1 ),( 2, -10, 100, 0, 1 );" +
//                "insert into SAMPLE1 select id, id, id, id, id from values ( 0 id ),( 11 ),( 22 );" +
//
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) values ( 0, -10, 100, 0, 1 );" +
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) values ( 1, -10, 100, 0, 1 );" +
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) values ( 2, -10, 100, 0, 1 );" +
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) values ( 3, -10, 100, 0, 1 );" +
////                "insert into SAMPLE1 ( ID, DIM_X, DIM_Y, DIM_Z, DIM_S ) values ( 4, -10, 100, 0, 1 );" +
//
//                "select ID, DIM_X, DIM_Y, DIM_Z, DIM_S from SAMPLE1 order by ID desc;" +
//                "update SAMPLE1 set DIM_X = 77 where ID = 0;" +
//                "select ID, DIM_X, DIM_Y, DIM_Z, DIM_S from SAMPLE1 order by ID;";
//
//        KuduClient client = new KuduClient.KuduClientBuilder(KUDU_MASTER).build();
//
//        try {
//            KuduResponse kuduResponse = StatementExecutor.exec(client, testStatements);
//            System.out.println("\tError: " + kuduResponse.error);
//            HashMap<Integer, KuduResponse> responses = (HashMap<Integer, KuduResponse>) kuduResponse.data;
//            System.out.println("\tData: " + responses);
//            for (KuduResponse response : responses.values()) {
//                System.out.println("\t\tStatementType: " + response.statementType);
//                if (response.statementType == Statement.Type.SELECT
//                        || response.statementType == Statement.Type.SHOW_TABLES
//                        || response.statementType == Statement.Type.SHOW_COLUMNS
//                        ) {
//
//                    SelectResponse.SelectResponsePB responseBB =
//                            (SelectResponse.SelectResponsePB) response.data;
//                    SelectResponsePrinter.print(responseBB, "\t\t");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                client.shutdown();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
////        System.out.println("===================");
//
//        Assert.assertEquals("OK", "OK");
//
//    }

//    @Test(timeout = 10000)
//    public void testRabbit() {
//
//        System.out.println("\n========testRabbit===========");
//
//        String EXCHANGE_NAME = "sphere";
//
//        Connection connection = null;
//        try {
//
//            ConnectionFactory factory = new ConnectionFactory();
//            factory.setHost("localhost");
//            factory.setUsername("sphere");
//            factory.setPassword("sphere");
//
//            connection = factory.newConnection();
//            System.out.println("connection.getClientProvidedName() = " + connection.getClientProvidedName());
////            connection.getClientProperties().entrySet().stream()
////                    .forEach(
////                            entry ->{
////
////                                System.out.println("key = " + entry.getKey());
////                                System.out.println("value = " + entry.getValue());
////                            }
////                    );
//            Channel channelRegisterIn = connection.createChannel();
//
//            String routingKey = "register_in";
//            String message = "Select count(t2.field1), sum(t1.field1), t1.field2 from testTable1 t1, someTable1 t2 where not t1.field1 <= 0  group by t1.field2; ";
////            String message = "select sum(33.3) from someTable1 ";
//
////            for(int i = 0; i < 1; i++){
////
////                message += message;
////            }
//
//            EXCHANGE_NAME = "";
//            String registerOut = UUID.randomUUID().toString();
//            String correlationId = "someCorrelationId";
//            AMQP.BasicProperties props = new AMQP.BasicProperties
//                    .Builder()
//                    .correlationId(correlationId)
//                    .replyTo(registerOut)
//                    .userId("sphere")
//                    .build();
//
//            final Channel channelRegisterOut = connection.createChannel();
//            channelRegisterOut.basicQos(1);
//
//            String registerOutExchangeName = "register_out";
//            boolean noDurable = false;
//            boolean noExclusive = false;
//            boolean exclusive = true;
//            boolean noAutoDelete = true;
//            boolean autoDelete = true;
//            Map<String, Object> nullArguments = null;
//
//            channelRegisterOut.queueDeclare(
//                    registerOut,
//                    noDurable,
//                    exclusive,
//                    autoDelete,
//                    nullArguments
//            );
//
//            channelRegisterOut.queueBind(
//                    registerOut,
//                    registerOutExchangeName,
//                    registerOut // replyTo
//            );
//
//            String[] registerMessage = new String[1];
//
//            final Consumer consumerRegisterOut = new DefaultConsumer(channelRegisterOut) {
//                @Override
//                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
//
//                    try {
//
//                        registerMessage[0] = new String(body, "UTF-8");
//                        System.out.println(" [x] Received from register_out '" + registerMessage[0] + "', correlationId = " + properties.getCorrelationId());
//
//                    } catch (Exception e) {
//
//                        e.printStackTrace();
//
//                    } finally {
//
//                        System.out.println(" [x] Done");
//                        try {
//
//                            channelRegisterOut.basicAck(envelope.getDeliveryTag(), false);
//
//                            synchronized (this) {
//
//                                this.notifyAll();
//                            }
//
//                        } catch (Exception e) {
//
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            };
//
//            channelRegisterOut.basicConsume(registerOut, false, consumerRegisterOut);
//
//            channelRegisterIn.basicPublish(
//                    EXCHANGE_NAME,
//                    routingKey,
//                    props,
//                    message.getBytes("UTF-8")
//            );
//            System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");
//
//            synchronized (consumerRegisterOut) {
//                try {
//
//                    consumerRegisterOut.wait();
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            String queueOut = UUID.randomUUID().toString();
//            System.out.println("queueOut = " + queueOut);
//            byte[][] outMessage = new byte[1][];
//            Channel channelOut = connection.createChannel();
//            channelOut.basicQos(1);
//            channelOut.queueDeclare(
//                    queueOut,
//                    noDurable,
//                    exclusive,
//                    autoDelete,
//                    nullArguments
//            );
//            channelOut.queueBind(
//                    queueOut,
//                    "out",
//                    registerMessage[0] // routing key
//            );
//            final Consumer consumerOut = new DefaultConsumer(channelOut) {
//                @Override
//                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
//
//                    try {
//
//                        outMessage[0] = body;
//                        System.out.println(" [x] Received from out '" + outMessage[0] + "'");
//
//                        SelectResponse.SelectResponsePB selectResponse =
//                                SelectResponse.SelectResponsePB.parseFrom(outMessage[0]);
//
//                        SelectResponsePrinter.print(selectResponse);
//
//                    } catch (Exception e) {
//
//                        e.printStackTrace();
//
//                    } finally {
//
//                        System.out.println(" [x] Done");
//                        try {
//
//                            channelOut.basicAck(envelope.getDeliveryTag(), false);
//
//                            synchronized (this) {
//
//                                this.notifyAll();
//                            }
//
//                        } catch (Exception e) {
//
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            };
//            channelOut.basicConsume(queueOut, false, consumerOut);
//
//            System.out.println(" Try to refresh select '" + registerMessage[0] + "'");
//
//            channelRegisterOut.basicPublish(
//                    "select"
//                    , registerMessage[0]
//                    , null
//                    , "NO_DESTROY".getBytes("UTF-8")
//            );
//
//            synchronized (consumerOut) {
//                try {
//
//                    consumerOut.wait();
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            System.out.println(" Try to manipulate");
//
//            message = "delete from testTable1 where field1 < 0; insert into testTable1(field1, field2) values(-1, 'YYY'); ";
//
//            channelRegisterIn.basicPublish(
//                    "", // Epmrty Exchange
//                    "manipulate", // routingKey
//                    null, // props
//                    message.getBytes("UTF-8")
//            );
//            System.out.println(" [x] Sent '" + "manipulate" + "':'" + message + "'");
//
//            synchronized (consumerOut) {
//                try {
//
//                    consumerOut.wait();
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        } finally {
//
//            if (connection != null) {
//
//                try {
//
//                    connection.close();
//                } catch (Exception errOnClose) {
//
//                    errOnClose.printStackTrace();
//                }
//            }
//        }
//
//        Assert.assertEquals("OK", "OK");
//    }

//    @Test
//    public void testMessageDigest() {
//
//        System.out.println("\n========testMessageDigest===========");
//
//        String query = "Select sum(field1), field2 f2 from testTable1 where field1 <= 10 group by f2 order by f2 desc ";
//                //"Select field1 f1, sum(field2), count(*), 'dcdc' from testTable1 group by f1";
//
////        String testStatements = "Select j1.*, ta.* from testTable1 j1 join (select max(t1.field1) in1, count(t1.field1), field2 as in2 from testTable1 t1 where t1.field1 <= 10 group by field2) ta on j1.field1=ta.in1 where ta.in2 in ('NULLS', 'Thu, 14 Sep 2017 14:36:38:151') ; \n";
////        String testStatements = "select t.* from (values 1 as zf1, 2 as f2, 3, 4) t(f1, zz, f3) ; \n";
//        String testStatements = "select t.* from (values 1 as f1, 2 as zz, 3 f3, 4) as t ; \n";
//        query = testStatements;
//
//        KuduSQLLexer lexer = new KuduSQLLexer(CharStreams.fromString(query));
//        CommonTokenStream tokens = new CommonTokenStream(lexer);
//        KuduSQLParser parser = new KuduSQLParser(tokens);
//        List<Statement> statements = parser.getStatements();
//        for(Statement statement: statements){
//
//            byte[] key = statement.getMd().digest();
//            System.out.println(convertByteArrayToHexString(key));
//            System.out.println("statement.getMd().getDigestLength() = " + statement.getMd().getDigestLength());
//
//            // Convert to string to be able to use in a hash map
//            BigInteger mediator = new BigInteger(1,key);
//            String keyString = String.format("%040x", mediator);
//            System.out.println(keyString);
//        }
//
//        Assert.assertEquals("OK", "OK");
//    }

//    @Test
//    public void testSorceItemValues(){
////        List<Expression> firstRow = new ArrayList<>();
////        firstRow.add(new Expression(Expression.Type.CONSTANT, 1));
////        firstRow.add(new Expression(Expression.Type.CONSTANT, 2));
////        firstRow.add(new Expression(Expression.Type.CONSTANT, 3));
////        int fieldNumber = 1;
////        for (Expression expression: firstRow){
////            expression.setAlias("field" + (fieldNumber++));
////        }
////
//        Source.Item.Values valuesSourceItem = null;
//
//        Type[] columnTypes = new Type[3];
//        for (int i = 0; i < columnTypes.length; i++){
//            columnTypes[i] = Type.INT32;
//        }
//        String[] columnAliases = new String[columnTypes.length];
//        for (int i = 0; i < columnTypes.length; i++){
//            columnAliases[i] = "field" + (i + 1);
//        }
//        valuesSourceItem = new Source.Item.Values(columnTypes, columnAliases);
//
////        try {
////            valuesSourceItem = new Source.Item.Values(firstRow);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
//        for (int i = 4; i <= 4 + 3; i++) {
//            valuesSourceItem.add(new Integer(i));
//        }
//
//        try {
//            valuesSourceItem.open(null).forEach(
//                    row -> {
//                        System.out.println("------row = " + row + "; " + row.getFieldValue(0));
//                    }
//            );
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("----------------------------------");
//
//        SelectResponse.OutputType outputType = SelectResponse.OutputType.ROWS;
//        Projection projection = new Projection();
//        projection.add(new Projection.Element.Asterisk());
//        List<Source> sources = new ArrayList<>();
//        Source source = new Source(valuesSourceItem);
//        sources.add(source);
////        Predicate predicate = null;
//        Predicate predicate = new Predicate.Comparison(
//                new Expression(Expression.Type.COLUMN, "field1"),
//                Predicate.Comparison.Operator.EQUAL,
//                new Expression(Expression.Type.CONSTANT, 4)
//        );
//        List<Expression> groupByElements = null;
//        Map<Expression, Integer> orderByMap = null;
//        long limit = -1L;
//        long offset = 0L;
//        View view = new View(
//                outputType,
//                projection,
//                sources,
//                predicate,
//                groupByElements,
//                orderByMap,
//                limit,
//                offset
//        );
//
//        KuduClient client = new KuduClient.KuduClientBuilder(KUDU_MASTER).build();
//
//        try {
////            SelectResponse.SelectResponsePB responsePB =  StatementExecutor.select(client, view);
////            SelectResponse.SelectResponsePB responsePB =  StatementExecutor.showTables(client);
//            SelectResponse.SelectResponsePB responsePB =  StatementExecutor.showColumns(client, "testTable1", "field2");
//            SelectResponsePrinter.print(responsePB, "\t\t");
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                client.shutdown();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    @Test
    public void testMain() {
        StatementExecutor executor = new StatementExecutor();
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }
}
