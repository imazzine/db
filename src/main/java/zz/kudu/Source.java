package zz.kudu;

import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.*;
import zz.common.Digestable;
import zz.kudu.sql.SelectResponse;

import java.security.MessageDigest;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

public class Source implements Digestable {

    private Item item; // required
    private List<Join> joins; // optional

    public Source(String tableName) {
        this.item = Item.getInstance(tableName);
    }

    public Source(Item item) {
        this.item = item;
    }

    public Source(Item item, Join join) {
        this.item = item;
        this.joins = new ArrayList<>();
        this.joins.add(join);
    }

    public Set<String> getTables() {
        Set<String> tables = item.getTables();
        if (joins != null) {
            for (Join join : joins) {
                tables.addAll(join.getTables());
            }
        }
        return tables;
    }

    public Item getItemByAlias(String itemAlias) throws Exception {
        Item foundItem = null;
        if (item.alias != null && item.alias.equals(itemAlias)) {
            foundItem = item;
        }
        if (joins != null) {
            for (Join join : joins) {
                if (join.item.alias != null && join.item.alias.equals(itemAlias)) {
                    if (foundItem != null) {
                        throw new Exception("Ambiguity of sources for source item alias '" + itemAlias + "'.");
                    }
                    foundItem = join.item;
                }
            }
        }
        return foundItem;
    }

    public static Item getItemByAlias(String itemAlias, List<Source> sources) throws Exception {
        Item foundItem = null;
        for (Source source : sources) {
            Item currentItem = source.getItemByAlias(itemAlias);
            if (currentItem != null) {
                if (foundItem != null) {
                    throw new Exception("Ambiguity of sources for source item alias '" + itemAlias + "'.");
                }
                foundItem = currentItem;
            }
        }
        return foundItem;
    }

    public List<Expression> getAsterisk(boolean bindIfPossible) throws Exception {
        List<Expression> columns = item.getAsterisk(bindIfPossible);
        if (joins != null) {
            for (Join join : joins) {
                columns.addAll(join.item.getAsterisk(bindIfPossible));
            }
        }
        return columns;
    }

    public void appendJoin(Join join) {
        if (joins == null) {
            joins = new ArrayList<>();
        }
        joins.add(join);
    }

    public void bind(KuduClient client) throws Exception {
        item.bind(client);
        if (joins != null) {
            for (Join join : joins) {
                join.bind(client);
            }
            for (Join join : joins) {
                join.bind(this);
            }
        }
    }

    public Item findItem(Expression expression) throws Exception {
        if (expression.getType() != Expression.Type.COLUMN) {
            return null;
        }
        Item masterItem = item.findItem(expression);
        if (joins != null) {
            Item foundJoinItem = masterItem;
            for (Join join : joins) {
                Item joinItem = join.findItem(expression);
                if (foundJoinItem != null && joinItem != null) {
                    throw new Exception("Ambiguity of sources for expression '" + expression + "'.");
                }
                foundJoinItem = joinItem;
            }
            if (foundJoinItem != null) {
                return foundJoinItem;
            }
        }
        return masterItem;
    }

    public int setOffset(int beginOffset) {
        int offset = item.setOffset(beginOffset);
        if (joins != null) {
            for (Join join : joins) {
                offset = join.setOffset(offset);
            }
        }
        return offset;
    }

    public int getProjectColumnCount() {
        int count = item.getProjectColumnCount();
        if (joins != null) {
            for (Join join : joins) {
                count += join.getProjectColumnCount();
            }
        }
        return count;
    }

    public Stream<Row> open(KuduClient client) throws Exception {
        Stream<Row> stream = item.open(client);
        if (joins != null) {
            for (Join join : joins) {
                stream = join.apply(client, stream);
            }
        }
        return stream;
    }

    public Item getItem() {
        return item;
    }

    public List<Join> getJoins() {
        return joins;
    }

    @Override
    public void updateDigest(MessageDigest md) {
        item.updateDigest(md);
        Digestable.updateDigestForCollectionMember(md, joins);
    }

    public static abstract class Item implements Digestable {

        protected String alias; // optional
        protected int offset = 0;

        public abstract List<Expression> getAsterisk(boolean bindIfPossible) throws Exception;

        public abstract Set<String> getTables();

        public abstract void bind(KuduClient client) throws Exception;

        public abstract String getColumnByAlias(String columnAlias);

        public abstract Item findItem(Expression expression) throws Exception;

        public abstract int addProjectColumn(Expression expression);

        public abstract boolean addPredicate(Predicate predicate);

        public abstract org.apache.kudu.Type getFieldKuduType(Expression expression) throws Exception;

        public abstract int setOffset(int beginOffset);

        public abstract int getProjectColumnCount();

        public abstract Stream<Row> open(KuduClient client) throws Exception;

        public static Item getInstance(String tableName) {
            return new Table(tableName);
        }

        public static Item getInstance(List<Source> sources) {
            return new Sources(sources);
        }

        public static Item getInstance(Statement statement) {
            return new Subquery(statement);
        }

        public int getOffset() {
            return offset;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public static class Values extends Item {

            private List<Row> rows;
            private int columnCount;
            private String[] columnNames;
            private String[] columnAliases;
            private Type[] columnTypes;
            private Comparable[] buffer;
            int currentBufferIndex = 0;

            public Values(Type[] columnTypes, String[] columnAliases) {
                this.rows = new ArrayList<>();
                this.columnCount = columnTypes.length;
                this.columnTypes = columnTypes;
                this.columnNames = new String[this.columnCount];
                this.columnAliases = columnAliases;
                if (this.columnAliases == null) {
                    this.columnAliases = new String[this.columnCount];
                }
                for (int i = 0; i < this.columnCount; i++) {
                    if (this.columnAliases[i] == null) {
                        this.columnNames[i] = UUID.randomUUID().toString();
                        this.columnAliases[i] = ""; // Don't display system names.
                    } else {
                        this.columnNames[i] = this.columnAliases[i];
                    }
                }
                this.buffer = new Comparable[this.columnCount];
            }

            public Values(List<Expression> firstRow) throws Exception {
                this.rows = new ArrayList<>();
                this.columnCount = firstRow.size();
                this.columnNames = new String[this.columnCount];
                this.columnAliases = new String[this.columnCount];
                this.columnTypes = new Type[this.columnCount];
                List<Source> sources = null; // No kudu source.
                for (int i = 0; i < this.columnCount; i++) {
                    Expression column = firstRow.get(i);
                    column.bind(sources, false); // To know kudu type.
                    if (column.getAlias() == null) {
                        this.columnNames[i] = UUID.randomUUID().toString();
                        this.columnAliases[i] = ""; // Don't display system names.
                    } else {
                        this.columnNames[i] = column.getAlias();
                        this.columnAliases[i] = column.getAlias();
                    }
                    this.columnTypes[i] = column.getKuduType();
                }
                this.buffer = new Comparable[this.columnCount];
                firstRow.stream().forEach(this::add);
            }

            private synchronized void setNextValue(Comparable value) {
                buffer[currentBufferIndex] = value;
                currentBufferIndex++;
                if (currentBufferIndex >= columnCount) {
                    rows.add(new Row(rows.size(), buffer));
                    buffer = new Comparable[columnCount];
                    currentBufferIndex = 0;
                }
            }

            public void add(Comparable value) {
                setNextValue(value);
            }

            public void add(Expression value) {
                setNextValue(value.getValue(null));
            }

            public void setColumnAliases(String[] columnAliases) {
                for (int i = 0; i < columnCount && i < columnAliases.length; i++) {
                    if (columnAliases[i] == null) {
                        this.columnNames[i] = UUID.randomUUID().toString();
                        this.columnAliases[i] = ""; // Don't display system names.
                    } else {
                        this.columnNames[i] = columnAliases[i];
                        this.columnAliases[i] = columnAliases[i];
                    }
                }
            }

            @Override
            public List<Expression> getAsterisk(boolean bindIfPossible) throws Exception {
                List<Expression> projectExpressions = new ArrayList<>(columnCount);
                for (int i = 0; i < this.columnCount; i++) {
                    Expression projectExpression = new Expression(Expression.Type.COLUMN, columnNames[i]);
                    projectExpression.setAlias(columnAliases[i]);
                    if (alias != null) {
                        projectExpression.setSourceItemAlias(alias);
                    }
                    if (bindIfPossible) {
                        projectExpression.bind(this);
                    }
                    projectExpressions.add(projectExpression);
                }
                return projectExpressions;
            }

            @Override
            public Set<String> getTables() {
                return new HashSet<>(); // No tables presents.
            }

            @Override
            public void bind(KuduClient client) throws Exception {
                // Do nothing
            }

            @Override
            public String getColumnByAlias(String columnAlias) {
                for (int i = 0; i < columnAliases.length; i++) {
                    if (columnAlias.equals(columnAliases[i])) {
                        return columnNames[i];
                    }
                }
                return null;
            }

            @Override
            public Item findItem(Expression expression) throws Exception {
                if (expression.getType() == Expression.Type.COLUMN) {
                    boolean belongs = false;
                    String itemAlias = expression.getSourceItemAlias();
                    if (itemAlias != null && itemAlias.equals(this.alias)) {
                        belongs = true;
                    }
                    if (belongs || itemAlias == null) {
                        if (getIndex(expression) >= 0) {
                            return this;
                        }
                    }
                }
                return null;
            }

            private int getIndex(Expression expression) {
                for (int i = 0; i < columnAliases.length; i++) {
                    if (expression.getElement().equals(columnAliases[i])) {
                        return i;
                    }
                }
                for (int i = 0; i < columnNames.length; i++) {
                    if (expression.getElement().equals(columnNames[i])) {
                        return i;
                    }
                }
                return -1;
            }

            @Override
            public int addProjectColumn(Expression expression) {
                if (expression.getType() == Expression.Type.COLUMN) {
                    return getIndex(expression);
                }
                return -1;
            }

            @Override
            public boolean addPredicate(Predicate predicate) {
                return false; // It is not kudu source.
            }

            @Override
            public Type getFieldKuduType(Expression expression) throws Exception {
                int index = getIndex(expression);
                if (index >= 0) {
                    return columnTypes[index];
                }
                return null;
            }

            @Override
            public int setOffset(int beginOffset) {
                offset = beginOffset;
                return offset + getProjectColumnCount();
            }

            @Override
            public int getProjectColumnCount() {
                return columnCount;
            }

            @Override
            public Stream<Row> open(KuduClient client) throws Exception {
                return rows.stream();
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(
                        md,
                        this.getClass().getName(),
                        alias
                );
                for (int i = 0; i < columnTypes.length; i++) {
                    // Don't use columnNames[i] because they are generated automatically.
                    Digestable.updateDigestForNullableStrings(
                            md,
                            columnTypes[i].getName(),
                            columnAliases[i]
                    );
                }
                for (Row row : rows) {
                    row.updateDigestUsingKuduTypes(md, columnTypes);
                }
            }
        }

        public static class Table extends Item {

            private String tableName;

            private Schema schema = null; // Is initialized in the bind(KuduClient)
            private LinkedHashMap<String, ColumnSchema> projectColumns = new LinkedHashMap<>();
            private Map<String, String> columnAliases = new HashMap<>();
            private List<KuduPredicate> kuduPredicates = new ArrayList<>();

            public Table(String tableName) {
                this.tableName = tableName;
            }

            public String getTableName() {
                return tableName;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(
                        md,
                        this.getClass().getName(),
                        tableName,
                        alias
                );
            }

            @Override
            public List<Expression> getAsterisk(boolean bindIfPossible) throws Exception {
                if (schema == null) {
                    throw new Exception("Kudu sources were not bound.");
                }
                Exception[] error = new Exception[1];
                List<Expression> tableColumns = schema.getColumns().stream()
                        .map(
                                columnSchema -> {
                                    Expression expression =
                                            new Expression(Expression.Type.COLUMN, columnSchema.getName());
                                    if (alias != null) {
                                        expression.setSourceItemAlias(alias);
                                    }
                                    if (bindIfPossible) {
                                        try {
                                            expression.bind(this);
                                        } catch (Exception e) {
                                            error[0] = e;
                                        }
                                    }
                                    return expression;
                                }
                        ).collect(toList());
                if (error[0] != null) {
                    throw error[0];
                }
                return tableColumns;
            }

            @Override
            public Set<String> getTables() {
                Set<String> tables = new HashSet<>();
                tables.add(tableName);
                return tables;
            }

            @Override
            public void bind(KuduClient client) throws Exception {
                KuduTable kuduTable = client.openTable(tableName);
                schema = kuduTable.getSchema();
            }

            @Override
            public String getColumnByAlias(String columnAlias) {
                return columnAliases.get(columnAlias);
            }

            @Override
            public Item findItem(Expression expression) throws Exception {
                Item item = null;
                if (expression.getType() == Expression.Type.COLUMN) {
                    String itemAlias = expression.getSourceItemAlias();
                    if (itemAlias == null) { // Find by column name
                        String columnName = (String) expression.getElement();
                        String columnNameByAlias = getColumnByAlias(columnName);
                        if (columnNameByAlias != null) {
                            columnName = columnNameByAlias;
                        }
                        ColumnSchema columnSchema = projectColumns.get(columnName);
                        if (columnSchema == null) {
                            if (schema == null) {
                                throw new Exception("Kudu sources were not bound.");
                            }
                            try {
                                int columnIndex = schema.getColumnIndex(columnName);
                                if (columnIndex >= 0) {
                                    item = this;
                                }
                            } catch (Exception e) {// Do nothing
                            }
                        } else {
                            item = this;
                        }
                    } else { // Find by alias
                        if (itemAlias.equals(this.alias)) {
                            item = this;
                        }
                    }
                }
                return item;
            }

            @Override
            public int addProjectColumn(Expression expression) {
                int index = -1;
                if (expression.getType() == Expression.Type.COLUMN) {
                    String columnName = (String) expression.getElement();
                    String columnNameByAlias = getColumnByAlias(columnName);
                    if (columnNameByAlias != null) {
                        columnName = columnNameByAlias;
                    }
                    Iterator<String> iterator = projectColumns.keySet().iterator();
                    int indx = -1;
                    boolean isFound = false;
                    while (iterator.hasNext()) {
                        indx++;
                        if (iterator.next().equals(columnName)) {
                            isFound = true;
                            break;
                        }
                    }
                    if (isFound) {
                        index = indx;
                    } else {
                        if (schema != null) {
                            try {
                                ColumnSchema columnSchema = schema.getColumn(columnName);
                                if (columnSchema != null) {
                                    projectColumns.put(columnName, columnSchema);
                                    index = projectColumns.size() - 1;
                                }
                            } catch (Exception e) { // return -1
                            }
                        }
                    }
                    if (index >= 0 && expression.getAlias() != null) {
                        columnAliases.put(expression.getAlias(), columnName);
                    }
                }
                return index;
            }

            @Override
            public boolean addPredicate(Predicate predicate) {
                kuduPredicates.addAll(predicate.getKuduPredicates(schema));
                return true;
            }

            @Override
            public org.apache.kudu.Type getFieldKuduType(Expression expression) throws Exception {
                if (expression.getType() == Expression.Type.COLUMN) {
                    String columnName = columnAliases.get(expression.getElement());
                    if (columnName == null) {
                        columnName = (String) expression.getElement();
                    }
                    ColumnSchema columnSchema = projectColumns.get(columnName);
                    if (columnSchema == null) {
                        if (schema == null) {
                            throw new Exception("Kudu sources were not bound");
                        }
                        columnSchema = schema.getColumn(columnName);
                        if (columnSchema != null) {
                            return columnSchema.getType();
                        }
                    } else {
                        return columnSchema.getType();
                    }
                }
                return null;
            }

            @Override
            public int setOffset(int beginOffset) {
                this.offset = beginOffset;
                return this.offset + getProjectColumnCount();
            }

            @Override
            public int getProjectColumnCount() {
                return projectColumns.size();
            }

            @Override
            public Stream<Row> open(KuduClient client) throws Exception {
                KuduTable table = client.openTable(tableName);
                KuduScanner.KuduScannerBuilder scannerBuilder = client.newScannerBuilder(table);
                List<String> projectColumnList = new ArrayList<>(projectColumns.keySet()); /* TODO: Make sure it keeps order */
                scannerBuilder.setProjectedColumnNames(projectColumnList);
                for (KuduPredicate kuduPredicate : kuduPredicates) {
                    scannerBuilder.addPredicate(kuduPredicate);
                }

                // predefine the number of rows
                KuduScanner scannerForCountRows = scannerBuilder.build();
                long total = 0L;
                while (scannerForCountRows.hasMoreRows()) {
                    RowResultIterator rowResultIterator = scannerForCountRows.nextRows();
                    total += rowResultIterator.getNumRows();
                }

                final KuduScanner scanner = scannerBuilder.build();
                KuduScannerIterator kuduScannerIterator = new KuduScannerIterator(scanner);
                return StreamSupport.stream(
                        Spliterators.spliterator(
                                kuduScannerIterator,
                                total,
                                Spliterator.ORDERED),
                        false
                ).parallel();
            }
        }

        public static class Sources extends Item {

            private List<Source> sources;

            public Sources(List<Source> sources) {
                this.sources = sources;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
                for (Source source : sources) { // Order is important.
                    source.updateDigest(md);
                }
                Digestable.updateDigestForNullableStrings(md, alias);
            }

            @Override
            public List<Expression> getAsterisk(boolean bindIfPossible) throws Exception {
                List<Expression> columns = new ArrayList<>();
                for (Source source : sources) {
                    columns.addAll(source.getAsterisk(bindIfPossible));
                }
                return columns;
            }

            @Override
            public Set<String> getTables() {
                Set<String> tables = new HashSet<>();
                for (Source source : sources) {
                    tables.addAll(source.getTables());
                }
                return tables;
            }

            @Override
            public void bind(KuduClient client) throws Exception {
                for (Source source : sources) {
                    source.bind(client);
                }
            }

            @Override
            public String getColumnByAlias(String columnAlias) {
                return null; // Unsupported for Sources
            }

            @Override
            public Item findItem(Expression expression) throws Exception {
                Item item = null;
                if (expression.getType() == Expression.Type.COLUMN) {
                    int count = 0;
                    for (Source source : sources) {
                        item = source.findItem(expression);
                        if (item != null) {
                            if (count > 0) {
                                throw new Exception("Ambiguity of sources for expression '" + expression + "'.");
                            }
                            count++;
                        }
                    }
                }
                return item;
            }

            @Override
            public int addProjectColumn(Expression expression) {
                return -1; // Impossible operation addProjectColumn() for Sources.
            }

            @Override
            public boolean addPredicate(Predicate predicate) {
                return false; // It is a composite source.
            }

            @Override
            public org.apache.kudu.Type getFieldKuduType(Expression expression) throws Exception {
                return null; // Impossible operation getFieldKuduType() for Sources.
            }

            @Override
            public int setOffset(int beginOffset) {
                this.offset = beginOffset;
                int currentOffset = this.offset;
                for (Source source : sources) {
                    currentOffset += source.setOffset(currentOffset);
                }
                return currentOffset;
            }

            @Override
            public int getProjectColumnCount() {
                int count = 0;
                for (Source source : sources) {
                    count += source.getProjectColumnCount();
                }
                return count;
            }

            @Override
            public Stream<Row> open(KuduClient client) throws Exception {
                Stream<Row> stream = null;
                int sourceCounter = 0;
                stream = sources.get(sourceCounter).open(client);
                Supplier<Stream<Row>> rightStreamSupplier;
                while (++sourceCounter < sources.size()) {
                    List<Row> rightRows = sources.get(sourceCounter).open(client).collect(toList());
                    rightStreamSupplier = rightRows::stream;
                    stream = Source.Join.crossJoin(stream, rightStreamSupplier);
                }
                return stream;
            }
        }

        public static class Subquery extends Item {

            private Statement statement;
            private View subquery;

            public Subquery(Statement statement) {
                this.statement = statement;
                if (this.statement.getType() == Statement.Type.SELECT) {
                    this.subquery = new View(this.statement);
                }
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
                statement.updateDigest(md);
                Digestable.updateDigestForNullableStrings(md, alias);
            }

            @Override
            public List<Expression> getAsterisk(boolean bindIfPossible) throws Exception {
                List<Expression> columnLinks = new ArrayList<>();
                for (Expression expression : subquery.getProjectColumns()) {
                    Expression link = new Expression(Expression.Type.COLUMN, expression.toString());
                    if (alias != null) {
                        link.setSourceItemAlias(alias);
                    }
                    link.setLink(expression);
                    if (bindIfPossible) {
                        link.bind(this);
                    }
                    columnLinks.add(link);
                }
                return columnLinks;
            }

            @Override
            public Set<String> getTables() {
                Set<String> tables = new HashSet<>();
                for (Source source : statement.getSources()) {
                    tables.addAll(source.getTables());
                }
                return tables;
            }

            @Override
            public void bind(KuduClient client) throws Exception {
                if (!subquery.isBound()) {
                    subquery.bind(client);
                }
            }

            @Override
            public String getColumnByAlias(String columnAlias) {
                Expression foundExpression = null;
                int count = 0;
                for (Expression expression : subquery.getProjectColumns()) {
                    if (expression.getType() == Expression.Type.COLUMN) {
                        if (expression.getAlias() != null && expression.getAlias().equals(columnAlias)) {
                            foundExpression = expression;
                            count++;
                        }
                    }
                }
                if (count == 1 && foundExpression.getSourceItem() != null) {
                    return foundExpression.getSourceItem().getColumnByAlias(columnAlias);
                }
                return null;
            }

            public Item findItem(Expression expression) throws Exception {
                Item item = null;
                if (expression.getType() == Expression.Type.COLUMN) {
                    boolean belongs = false;
                    String itemAlias = expression.getSourceItemAlias();
                    if (itemAlias != null && itemAlias.equals(this.alias)) {
                        belongs = true;
                    }
                    if (belongs || itemAlias == null) {
                        if (subquery.linkAlias(expression)) {
                            item = expression.getLink().getSourceItem();
                        } else {
                            for (Expression selectElement : subquery.getProjectColumns()) {
                                if (selectElement.equalsNoAlias(expression)) {
                                    item = selectElement.getSourceItem();
                                    break;
                                }
                            }
                        }
                    }
                }
                return item;
            }

            @Override
            public int addProjectColumn(Expression expression) {
                int index = -1;
                if (expression.getType() == Expression.Type.COLUMN) {
                    /* TODO: use equalsNoAlias()? */
                    int position = subquery.getProjectColumns().indexOf(expression);
                    if (position >= 0) {
                        Expression subqueryExpression = subquery.getProjectColumns().get(position);
                        index = subqueryExpression.getSourceItemFieldIndex(); // We supposed that expression is bound.
                    }
                }
                return index; // Do nothing but return index if exists.
            }

            @Override
            public boolean addPredicate(Predicate predicate) {
                return false; // It is a composite source.
            }

            @Override
            public org.apache.kudu.Type getFieldKuduType(Expression expression) throws Exception {
                if (expression.getType() == Expression.Type.COLUMN) {
                    /* TODO: It might be wrong. */
                    int position = subquery.getProjectColumns().indexOf(expression);
                    if (position >= 0) {
                        Expression subqueryExpression = subquery.getProjectColumns().get(position);
                        return subqueryExpression.getKuduType();
                    }
                }
                return null;
            }

            @Override
            public int setOffset(int beginOffset) {
                this.offset = beginOffset;
                subquery.setSourceOffset(this.offset);
                return subquery.setSourceOffsets();
            }

            @Override
            public int getProjectColumnCount() {
                return subquery.getColumnCount();
            }

            @Override
            public Stream<Row> open(KuduClient client) throws Exception {
                return subquery.openStream(client);
            }
        }
    }

    public static class Join implements Digestable {

        public enum Type {
            INNER,
            CROSS,
            LEFT,
            RIGHT,
            FULL
        }

        private Type type; // required
        private Item item; // required
        private Predicate predicate; // optional

        public Set<String> getTables() {
            return item.getTables();
        }

        @Override
        public void updateDigest(MessageDigest md) {
            /* TODO: Each type can be converted to another. */
            Digestable.updateDigestForNullableStrings(md, type.name());
            item.updateDigest(md);
            Digestable.updateDigestForNullableDigestable(md, predicate);
        }

        public void bind(Source source) throws Exception {
            if (predicate != null) {
                predicate.bind(source, predicate.isKuduSupported());
                if (!predicate.isKuduSupported() && predicate instanceof Predicate.And) {
                    Predicate.And.optimize((Predicate.And) predicate, Collections.singletonList(source));
                }
            }
        }

        public static <T> Stream<T> defaultIfEmpty(Stream<T> stream, Supplier<T> supplier) {
            Iterator<T> iterator = stream.iterator();
            if (iterator.hasNext()) {
                return StreamSupport.stream(
                        Spliterators.spliteratorUnknownSize(
                                iterator, 0
                        ), false);
            } else {
                return Stream.of(supplier.get());
            }
        }

        public static Stream<Row> crossJoin(
                Stream<Row> leftStream,
                Supplier<Stream<Row>> rightStreamSupplier) {

            int[] rowNumber = {0};
            return leftStream.flatMap(
                    leftRow -> rightStreamSupplier.get()
                            .map(
                                    rightRow -> {
                                        Comparable[] joinedRow = new Comparable[
                                                leftRow.getFieldValues().length + rightRow.getFieldValues().length
                                                ];
                                        System.arraycopy(leftRow.getFieldValues(), 0, joinedRow
                                                , 0, leftRow.getFieldValues().length);
                                        System.arraycopy(rightRow.getFieldValues(), 0, joinedRow
                                                , leftRow.getFieldValues().length, rightRow.getFieldValues().length);
                                        return new Row(rowNumber[0]++, joinedRow);
                                    }
                            )
            );
        }

        public static Stream<Row> innerJoin(
                Stream<Row> leftStream,
                Supplier<Stream<Row>> rightStreamSupplier,
                Predicate predicate) {

            if (predicate == null || predicate.isKuduSupported()) {
                return crossJoin(leftStream, rightStreamSupplier);
            }
            int[] rowNumber = {0};
            return leftStream.flatMap(
                    leftRow -> rightStreamSupplier.get()
                            .filter(
                                    rightRow -> predicate.apply(leftRow, rightRow)
                            )
                            .map(
                                    rightRow -> {
                                        Comparable[] joinedRow = new Comparable[
                                                leftRow.getFieldValues().length + rightRow.getFieldValues().length
                                                ];
                                        System.arraycopy(leftRow.getFieldValues(), 0, joinedRow
                                                , 0, leftRow.getFieldValues().length);
                                        System.arraycopy(rightRow.getFieldValues(), 0, joinedRow
                                                , leftRow.getFieldValues().length, rightRow.getFieldValues().length);
                                        return new Row(rowNumber[0]++, joinedRow);
                                    }
                            )
            );
        }

        public static Stream<Row> leftOuterJoin(
                Stream<Row> leftStream,
                Supplier<Stream<Row>> rightStreamSupplier,
                Predicate predicate,
                Supplier<Row> nullRowSupplier) {

            if (predicate == null) {
                return crossJoin(leftStream, rightStreamSupplier);
            }
            int[] rowNumber = {0};
            if (predicate.isKuduSupported()) {

                return leftStream.flatMap(
                        leftRow -> defaultIfEmpty(
                                rightStreamSupplier.get()
                                , nullRowSupplier
                        ).map(
                                rightRow -> {
                                    Comparable[] joinedRow = new Comparable[
                                            leftRow.getFieldValues().length + rightRow.getFieldValues().length
                                            ];
                                    System.arraycopy(leftRow.getFieldValues(), 0, joinedRow
                                            , 0, leftRow.getFieldValues().length);
                                    System.arraycopy(rightRow.getFieldValues(), 0, joinedRow
                                            , leftRow.getFieldValues().length, rightRow.getFieldValues().length);
                                    return new Row(rowNumber[0]++, joinedRow);
                                }
                        )
                );
            }
            return leftStream.flatMap(
                    leftRow -> defaultIfEmpty(
                            rightStreamSupplier.get().filter(
                                    rightRow -> predicate.apply(leftRow, rightRow)
                            )
                            , nullRowSupplier
                    ).map(
                            rightRow -> {
                                Comparable[] joinedRow = new Comparable[
                                        leftRow.getFieldValues().length + rightRow.getFieldValues().length
                                        ];
                                System.arraycopy(leftRow.getFieldValues(), 0, joinedRow
                                        , 0, leftRow.getFieldValues().length);
                                System.arraycopy(rightRow.getFieldValues(), 0, joinedRow
                                        , leftRow.getFieldValues().length, rightRow.getFieldValues().length);
                                return new Row(rowNumber[0]++, joinedRow);
                            }
                    )
            );
        }

        public static Stream<Row> rightOuterJoin(
                Supplier<Stream<Row>> leftStreamSupplier,
                Stream<Row> rightStream,
                Predicate predicate,
                Supplier<Row> nullRowSupplier) {

            if (predicate == null) {
                List<Row> rightRows = rightStream.collect(Collectors.toList());
                Supplier<Stream<Row>> rightStreamSupplier = rightRows::stream;
                return crossJoin(leftStreamSupplier.get(), rightStreamSupplier);
            }
            int[] rowNumber = {0};
            if (predicate.isKuduSupported()) {
                return rightStream.flatMap(
                        rightRow -> defaultIfEmpty(
                                leftStreamSupplier.get()
                                , nullRowSupplier
                        ).map(
                                leftRow -> {
                                    Comparable[] joinedRow = new Comparable[
                                            leftRow.getFieldValues().length + rightRow.getFieldValues().length
                                            ];
                                    System.arraycopy(leftRow.getFieldValues(), 0, joinedRow
                                            , 0, leftRow.getFieldValues().length);
                                    System.arraycopy(rightRow.getFieldValues(), 0, joinedRow
                                            , leftRow.getFieldValues().length, rightRow.getFieldValues().length);
                                    return new Row(rowNumber[0]++, joinedRow);
                                }
                        )
                );
            }
            return rightStream.flatMap(
                    rightRow -> defaultIfEmpty(
                            leftStreamSupplier.get().filter(
                                    leftRow -> predicate.apply(leftRow, rightRow)
                            )
                            , nullRowSupplier
                    ).map(
                            leftRow -> {
                                Comparable[] joinedRow = new Comparable[
                                        leftRow.getFieldValues().length + rightRow.getFieldValues().length
                                        ];
                                System.arraycopy(leftRow.getFieldValues(), 0, joinedRow
                                        , 0, leftRow.getFieldValues().length);
                                System.arraycopy(rightRow.getFieldValues(), 0, joinedRow
                                        , leftRow.getFieldValues().length, rightRow.getFieldValues().length);
                                return new Row(rowNumber[0]++, joinedRow);
                            }
                    )
            );
        }

        public Stream<Row> apply(KuduClient client, Stream<Row> leftStream) throws Exception {

            List<Row> rightRows;
            Supplier<Stream<Row>> rightStreamSupplier;
//            Comparable[] nullRowArray = {"null", 0.0F, "null", "null"};
//            Comparable[] nullRowArray = {0.0F, "null", "null"};
            Comparable[] nullRowArray;
            Row nullRow;
            Supplier<Row> nullRowSupplier;

            switch (type) {
                case INNER:
                case CROSS:
                    rightRows = item.open(client).collect(Collectors.toList());
                    rightStreamSupplier = rightRows::stream;
                    return innerJoin(leftStream, rightStreamSupplier, predicate);
                case LEFT:
                    rightRows = item.open(client).collect(Collectors.toList());
                    rightStreamSupplier = rightRows::stream;
                    nullRowArray = new Comparable[item.getProjectColumnCount()];
                    nullRow = new Row(0, nullRowArray);
                    nullRowSupplier = () -> nullRow;
                    return leftOuterJoin(leftStream, rightStreamSupplier, predicate, nullRowSupplier);
                case RIGHT:
                    List<Row> leftRows = leftStream.collect(Collectors.toList());
                    Supplier<Stream<Row>> leftStreamSupplier = leftRows::stream;
                    nullRowArray = new Comparable[item.getOffset()];
                    nullRow = new Row(0, nullRowArray);
                    nullRowSupplier = () -> nullRow;
                    return rightOuterJoin(leftStreamSupplier, item.open(client), predicate, nullRowSupplier);
                case FULL:
                    throw new Exception("TODO: Full join is not supported.");
                default:
                    throw new Exception("Type '" + type.name() + "' is not supported for join.");
            }
        }

        public void bind(KuduClient client) throws Exception {
            item.bind(client);
        }

        public Item findItem(Expression expression) throws Exception {
            return item.findItem(expression);
        }

        public int setOffset(int beginOffset) {
            int itemOffset = item.setOffset(beginOffset); // Before predicate.setOffset()
            if (predicate != null) {
                predicate.setOffset();
            }
            return itemOffset;
        }

        public int getProjectColumnCount() {
            return item.getProjectColumnCount();
        }

        public Join(Type type, Item item) {
            this.type = type;
            this.item = item;
        }

        public Join(Type type, Item item, Predicate predicate) {
            this.type = type;
            this.item = item;
            this.predicate = predicate;
        }

        public Type getType() {
            return type;
        }

        public Item getItem() {
            return item;
        }

        public Predicate getPredicate() {
            return predicate;
        }

        public void setPredicate(Predicate predicate) {
            this.predicate = predicate;
        }
    }
}
