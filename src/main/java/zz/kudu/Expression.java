package zz.kudu;

import zz.common.Digestable;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Expression extends Projection.Element implements Digestable {

    private String alias;
    private Expression.Type type;
    private Object element;

    private boolean isAggregate = false;
    private org.apache.kudu.Type kuduType;
    private Collector collector;

    private String sourceItemAlias;
    private Source.Item sourceItem;
    private int sourceItemFieldIndex = -1;
    private int offset;
    private Expression link = null;

    public enum Type {
        COLUMN, FUNCTION, CONSTANT
    }

    @Override
    public List<Expression> getExpressions() {
        return Collections.singletonList(this);
    }

    public String toString() {
        if (alias != null) {
            return alias;
        }
        if (sourceItemAlias != null) {
            return sourceItemAlias + "." + element.toString();
        }
        return element.toString();
    }

    public void setSourceItemAlias(String sourceItemAlias) {
        this.sourceItemAlias = sourceItemAlias;
    }

    public String getSourceItemAlias() {
        return sourceItemAlias;
    }

    public Source.Item getSourceItem() {
        if (isLink()) {
            return link.getSourceItem();
        }
        return sourceItem;
    }

    public void bind(List<Source> sources) throws Exception {
        bind(sources, true);
    }

    void bind(Source.Item sourceItem) throws Exception {
        bind(sourceItem, true);
    }

    void bind(Source.Item sourceItem, boolean addAsProjectColumn) throws Exception {
        switch (type) {
            case COLUMN:
                if (isLink()) {
                    collector = link.getCollector();
                    kuduType = link.getKuduType();
                    sourceItemFieldIndex = -1;
                } else {
                    this.sourceItem = sourceItem;
                    if (addAsProjectColumn) { // It is only predicate's case
                        sourceItemFieldIndex = sourceItem.addProjectColumn(this);
                        if (sourceItemFieldIndex < 0) {
                            throw new Exception("Expression '" + this + "' does not belong to the source found.");
                        }
                    } else {
                        // Do nothing! Don't do sourceItemFieldIndex = -1;
                        // 'cause it might override setting before optimizing
                        // for not kudu predicates!
                    }
                    kuduType = sourceItem.getFieldKuduType(this);
                    collector = newNotAggregateColumnCollector();
                }
                break;
            case FUNCTION:
                Function fun = (Function) element;
                fun.bind(sourceItem);
                collector = fun.getCollector();
                kuduType = fun.getKuduType();
                sourceItemFieldIndex = -1;
                break;
            case CONSTANT:
                List<Source> sources = null; // Constants doesn't need source.
                bind(sources);
                break;
            default:
                throw new Exception("Unknown type '" + type + "' for binding expression '" + this + "'.");
        }
    }

    // package access
    void bind(List<Source> sources, boolean addAsProjectColumn) throws Exception {
        switch (type) {
            case COLUMN:
                int countSources = 0;
                for (Source source : sources) {
                    Source.Item item = source.findItem(this);
                    if (isLink()) {
                        break;
                    }
                    if (item != null) {
                        if (countSources > 0) {
                            throw new Exception("More than one source was found for expression '" + this + "'.");
                        }
                        sourceItem = item;
                        countSources++;
                    }
                }
                if (!isLink() && countSources <= 0) {
                    throw new Exception("Source not found for expression '" + this + "'.");
                }
                bind(sourceItem, addAsProjectColumn);
                break;
            case FUNCTION:
                Function fun = (Function) element;
                fun.bind(sources);
                collector = fun.getCollector();
                kuduType = fun.getKuduType();
                sourceItemFieldIndex = -1;
                break;
            case CONSTANT:
                sourceItemFieldIndex = -1;
                collector = newNotAggregateColumnCollector();
                if (element instanceof Integer) {
                    kuduType = org.apache.kudu.Type.INT32;
                } else if (element instanceof Long) {
                    kuduType = org.apache.kudu.Type.INT64;
                } else if (element instanceof Double) {
                    kuduType = org.apache.kudu.Type.DOUBLE;
                } else if (element instanceof String) {
                    kuduType = org.apache.kudu.Type.STRING;
                } else if (element instanceof Float) {
                    kuduType = org.apache.kudu.Type.FLOAT;
                } else if (element instanceof Boolean) {
                    kuduType = org.apache.kudu.Type.BOOL;
                } else if (element instanceof Byte) {
                    kuduType = org.apache.kudu.Type.INT8;
                } else if (element instanceof Short) {
                    kuduType = org.apache.kudu.Type.INT16;
                }
//                    else if (element instanceof ByteBuffer) {
//                        kuduType = org.apache.kudu.Type.BINARY;
//                    }
//                    else if (element instanceof Time) {
//                        kuduType = org.apache.kudu.Type.UNIXTIME_MICROS;
//                    }
                else {

                    throw new Exception("Unsupported constant type: " + element);
                }
                break;
            default:
                throw new Exception("Unknown type '" + type + "' for binding expression '" + this + "'.");
        }
    }

    public Comparable getValue(Row row) {
        switch (type) {
            case CONSTANT:
                return (Comparable) element;
            case COLUMN:
                if (isLink()) {
                    return link.getValue(row);
                }
                return row.getFieldValue(offset);
            case FUNCTION:
                if (offset < 0) {
                    return ((Function) element).getValue(row);
                }
                return row.getFieldValue(offset); // It usually happens after grouping
            default:
                return null;
        }
    }

    public Comparable getValue(Row leftRow, Row rightRow) { // For join
        switch (type) {
            case CONSTANT:
                return (Comparable) element;
            case COLUMN:
                if (isLink()) {
                    return link.getValue(leftRow, rightRow);
                }
                return Row.getFieldValue(leftRow, rightRow, offset);
            case FUNCTION:
                if (offset < 0) {
                    return ((Function) element).getValue(leftRow, rightRow);
                }
                return Row.getFieldValue(leftRow, rightRow, offset); // It usually happens after grouping
            default:
                return null;
        }
    }

    public void setOffset() {
        switch (type) {
            case CONSTANT:
                offset = -1;
                break;
            case COLUMN:
                if (isLink()) {
                    offset = -1;
                } else {
                    offset = sourceItem.getOffset() + sourceItemFieldIndex;
                }
                break;
            case FUNCTION:
                offset = -1;
                ((Function) element).setOffset();
                break;
        }
    }

    public void setOffset(int offset) {
        this.offset = offset;
        // We don't set an offset for children
        // because we assume that the functions were calculated at this point,
        // so we don't have to call Function.getValue(row),
        // and we just get the value from the row directly.
    }

    public int getOffset() {
        return offset;
    }

    public Expression(Expression.Type type, Object element) {
        this.type = type;
        this.element = element;
        if (this.type == Type.FUNCTION) {
            checkHasAggregates((Function) this.element);
        }
    }

    private void checkHasAggregates(Function fun) {
        if (fun.getType() == Function.Type.AGGREGATE) {
            isAggregate = true;
            return;
        }
        List<Expression> args = fun.getArguments();
        for (Expression arg : args) {
            if (arg.getType() == Type.FUNCTION) {
                checkHasAggregates((Function) arg.getElement());
            }
        }
    }

    public void setLink(Expression link) {
        this.link = link;
    }

    public Expression getLink() {
        return link;
    }

    public boolean isLink() {
        return link != null;
    }

    public int getSourceItemFieldIndex() {
        return sourceItemFieldIndex;
    }

    public org.apache.kudu.Type getKuduType() {
        return kuduType;
    }

    public Collector getCollector() {
        return collector;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Type getType() {
        if (isLink()) {
            return link.getType();
        }
        return type;
    }

    public Object getElement() {
        return element;
    }

    public boolean isAggregate() {
        return isAggregate;
    }

    @Override
    public void updateDigest(MessageDigest md) {
        Digestable.updateDigestForNullableStrings(md, type.name());
        switch (type) {
            case COLUMN:
                Digestable.updateDigestForNullableStrings(md, sourceItemAlias, (String) element, alias);
                break;
            case FUNCTION:
                ((Function) element).updateDigest(md);
                break;
            case CONSTANT:
                ByteBuffer buff = null;
                if (element instanceof Integer) {
                    buff = ByteBuffer.allocate(Integer.BYTES);
                    buff.putInt((Integer) element);
                } else if (element instanceof Long) {
                    buff = ByteBuffer.allocate(Long.BYTES);
                    buff.putLong((Long) element);
                } else if (element instanceof Double) {
                    buff = ByteBuffer.allocate(Double.BYTES);
                    buff.putDouble((Double) element);
                } else if (element instanceof String) {
                    byte[] stringBytes = ((String) element).getBytes(StandardCharsets.UTF_8);
                    buff = ByteBuffer.allocate(Integer.BYTES + stringBytes.length);
                    buff.putInt(stringBytes.length);
                    buff.put(stringBytes);
                } else if (element instanceof Float) {
                    buff = ByteBuffer.allocate(Float.BYTES);
                    buff.putFloat((Float) element);
                } else if (element instanceof Boolean) {
                    buff = ByteBuffer.allocate(Byte.BYTES);
                    byte boolByte = (Boolean) element ? (byte) 1 : (byte) 0;
                    buff.put(boolByte);
                } else if (element instanceof Byte) {
                    buff = ByteBuffer.allocate(Byte.BYTES);
                    buff.put((Byte) element);
                } else if (element instanceof Short) {
                    buff = ByteBuffer.allocate(Short.BYTES);
                    buff.putShort((Short) element);
                }

                /*TODO: Case for ByteBuffer*/

                else {

                    /*TODO: Maybe we should make method throwable?*/
                    new Exception("Unsupported constant type for Digest: " + element)
                            .printStackTrace();
                }

                buff.flip();
                md.update(buff);
                break;
        }
    }

    /* ----------------Collectors for not aggregate columns----------------- */

    public static Collector newNotAggregateColumnCollector() {
        return Collector.of(
                () -> new Comparable[1],
                (result, value) -> result[0] = (Comparable) value,
                (result1, result2) -> result1,
                result -> result[0]
        );
    }

    /* ----------------AVG Collectors----------------- */

    public static Collector newAvgIntCollector() {
        return zz.common.Collectors.averagingInt((value) -> ((Number) value).intValue());
    }

    public static Collector newAvgLongCollector() {
        return zz.common.Collectors.averagingLong((value) -> ((Number) value).longValue());
    }

    public static Collector newAvgDoubleCollector() {
        return zz.common.Collectors.averagingDouble((value) -> ((Number) value).doubleValue());
    }

    /* ----------------COUNT Collectors----------------- */

    public static Collector newCountCollector() {
        return Collectors.counting();
    }

    /* ----------------MAX MIN Collectors----------------- */

    public static Collector newMaxCollector() {
        return Collectors.collectingAndThen(
                Collectors.maxBy((v1, v2) -> ((Comparable) v1).compareTo(v2)),
                optional -> optional.orElse(null)
        );
    }

    public static Collector newMinCollector() {
        return Collectors.collectingAndThen(
                Collectors.minBy((v1, v2) -> ((Comparable) v1).compareTo(v2)),
                optional -> optional.orElse(null)
        );
    }

    /* ----------------SUM Collectors----------------- */

    public static Collector newSumIntCollector() {
        return zz.common.Collectors.summingInt((value) -> ((Number) value).intValue());
    }

    public static Collector newSumLongCollector() {
        return zz.common.Collectors.summingLong((value) -> ((Number) value).longValue());
    }

    public static Collector newSumDoubleCollector() {
        return zz.common.Collectors.summingDouble((value) -> ((Number) value).doubleValue());
    }


    /* ----------------equals, hashCode----------------- */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expression that = (Expression) o;
        if (type == Type.COLUMN && that.alias != null) {
            if (that.alias.equals(element)) {
                return true;
            }
        }
        if (that.type == Type.COLUMN && alias != null) {
            if (alias.equals(that.element)) {
                return true;
            }
        }
        if (type != that.type) return false;
        return element.equals(that.element);
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + element.hashCode();
        return result;
    }

    public boolean equalsNoAlias(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Expression that = (Expression) o;
        if (type != that.type) return false;
        if (type == Type.FUNCTION) {
            return ((Function) element).equalsNoAlias(that.element);
        }
        return element.equals(that.element);
    }
}
