package zz.kudu;

public class KuduResponse {

    public Statement.Type statementType;
    public String error;
    public Object data;

    public KuduResponse() {
    }

    public KuduResponse(Statement.Type statementType, String error, Object data) {
        this.statementType = statementType;
        this.error = error;
        this.data = data;
    }
}
