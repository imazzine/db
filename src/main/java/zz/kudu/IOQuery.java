package zz.kudu;

import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.kudu.client.KuduClient;
import sphere.kudu.protobuf.db.DbDataset;
import sphere.kudu.protobuf.io.IoMessage;
import sphere.kudu.protobuf.io.enums.IoEnum;

import java.io.IOException;
import java.util.HashMap;

public class IOQuery implements Runnable {

    private KuduClient kuduClient;
    private byte[] messageBytes;

    public IOQuery(KuduClient kuduClient, byte[] messageBytes) {
        this.kuduClient = kuduClient;
        this.messageBytes = messageBytes;
    }

    @Override
    public void run() {
        try {
            IoMessage.Message message = IoMessage.Message.parseFrom(messageBytes);
            IoMessage.Message.Builder messageBuilder = null;
            String query = message.getIo();
            try {
                KuduResponse kuduResponse = StatementExecutor.exec(kuduClient, query);
                if (!"".equals(kuduResponse.error)) {
                    throw new Exception(kuduResponse.error);
                }
                HashMap<Integer, KuduResponse> responses = (HashMap<Integer, KuduResponse>) kuduResponse.data;
                for (KuduResponse response : responses.values()) {
                    messageBuilder = IoMessage.Message.newBuilder(message);
                    messageBuilder.setType(IoEnum.Type.RESPONSE);
                    messageBuilder.setStatus(IoEnum.Status.OK);
                    if (response.statementType == Statement.Type.SELECT
                            || response.statementType == Statement.Type.SHOW_TABLES
                            || response.statementType == Statement.Type.SHOW_COLUMNS
                            ) {

                        DbDataset.Dataset responseBB =
                                (DbDataset.Dataset) response.data;
                        messageBuilder.setDataset(responseBB);
                    }
                }
            } catch (Exception e) {
                messageBuilder = IoMessage.Message.newBuilder(message);
                messageBuilder.setType(IoEnum.Type.RESPONSE);
                messageBuilder.setStatus(IoEnum.Status.ERROR);
                messageBuilder.setIo(e.toString());
            } finally {
                try {
                    System.out.write(messageBuilder.build().toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }
}
