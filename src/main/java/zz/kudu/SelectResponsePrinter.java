package zz.kudu;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import com.google.protobuf.NullValue;
import org.apache.kudu.Common;
import org.apache.kudu.Type;
import org.apache.kudu.client.Bytes;
import org.apache.kudu.shaded.org.jboss.netty.util.CharsetUtil;
import zz.kudu.sql.SelectResponse;

public class SelectResponsePrinter {

    private final static String defaultIndent = "";

    public static void print(SelectResponse.SelectResponsePB response, String indent) {

        SelectResponse.HeaderPB header = response.getHeader();
        SelectResponse.BodyPB body = response.getBody();

        SelectResponse.OutputType outputType = header.getOutputType();
        println("outputType: " + outputType, indent);

        long rowCount = header.getRowCount();
        println("rows: " + rowCount, indent);

        int columnCount = header.getColumnCount();
        println("columns: " + columnCount, indent);

        List<zz.kudu.sql.Common.FieldType> fieldTypes = header.getColumnTypesList();
        Type[] columnTypes = new Type[columnCount];
        long rowSize = 0;
        int colIndex = 0;
        for (zz.kudu.sql.Common.FieldType type : fieldTypes) {

            Type kuduType = Type.getTypeForDataType(Common.DataType.forNumber(type.getNumber()));
            rowSize += kuduType.getSize();
            columnTypes[colIndex++] = kuduType;
        }
        println("rowSize: " + rowSize, indent);

        String caption = "|";
        for (Type type : columnTypes) {
            caption += "\t----------\t|";
        }
        println(caption, indent);

        caption = "|";
        List<String> columnNames = header.getColumnNamesList();
        for (String columnName : columnNames) {

            caption += ("\t" + columnName + "\t|");
        }
        println(caption, indent);

        caption = "|";
        String captionMins = "|";
        String captionMaxs = "|";
//        for (Type type : columnTypes) {
        for (int i = 0; i < columnTypes.length; i++) {
            Type type = columnTypes[i];
            caption += (type.getName() + "(" + type.getSize() + "/" + (type.getSize() * rowCount) + ")" + "|");
            SelectResponse.AnySimpleValuePB min = header.getMinsList().get(i);
            captionMins += cellToString(type, min);
            SelectResponse.AnySimpleValuePB max = header.getMaxsList().get(i);
            captionMaxs += cellToString(type, max);
        }
        println(caption, indent);
        println(captionMins, indent);
        println(captionMaxs, indent);

        caption = "|";
        for (Type type : columnTypes) {
            caption += "\t----------\t|";
        }
        println(caption, indent);

        int currentCellOffset = 0;
        ByteBuffer data = body.getData().asReadOnlyByteBuffer().order(ByteOrder.LITTLE_ENDIAN);
        byte[] indirectData = body.getIndirectData().toByteArray();
        for (int iR = 0; iR < rowCount; iR++) {

            String rowStr = "|";
            for (int iC = 0; iC < columnCount; iC++) {

                Type type = columnTypes[iC];
                switch (type) {

                    case BOOL:
                        boolean boolValue = (data.get(currentCellOffset) == 1);
                        rowStr += ("\t" + boolValue + "\t|");
                        break;
                    case INT8:
                        byte byteValue = data.get(currentCellOffset);
                        rowStr += ("\t" + byteValue + "\t|");
                        break;
                    case INT16:
                        short shortValue = data.getShort(currentCellOffset);
                        rowStr += ("\t" + shortValue + "\t|");
                        break;
                    case INT32:
                        int intValue = data.getInt(currentCellOffset);
                        rowStr += ("\t" + intValue + "\t|");
                        break;
                    case INT64:
                        long longValue = data.getLong(currentCellOffset);
                        rowStr += ("\t" + longValue + "\t|");
                        break;
                    case FLOAT:
                        float floatValue = data.getFloat(currentCellOffset);
                        rowStr += ("\t" + floatValue + "\t|");
                        break;
                    case DOUBLE:
                        double doubleValue = data.getDouble(currentCellOffset);
                        rowStr += ("\t" + doubleValue + "\t|");
                        break;
                    case STRING:
                        String str = new String(
                                indirectData,
                                (int) data.getLong(currentCellOffset),
                                (int) data.getLong(currentCellOffset + 8),
                                CharsetUtil.UTF_8
                        );
                        rowStr += ("\t" + str + "\t|");
                        break;
                    default:
//                        throw new Exception("Unsupported type: " + type.getName());
                        System.out.println("Unsupported type: " + type.getName());
                }

                if (outputType == SelectResponse.OutputType.COLUMNS) {

                    long skipLen = 0;

                    for (int i = 0; (i <= iC) && (iC < columnCount - 1); i++) {

                        skipLen += columnTypes[i].getSize() * rowCount;
                    }

                    if (iC < columnCount - 1) {

                        skipLen += columnTypes[iC + 1].getSize() * iR;
                    } else {

                        skipLen += columnTypes[0].getSize() * (iR + 1);
                    }

                    currentCellOffset = (int) skipLen;
                } else {

                    currentCellOffset += type.getSize();
                }
            }

            println(rowStr, indent);
        }
    }

    public static String cellToString(Type type, SelectResponse.AnySimpleValuePB value) {
        if (value.hasNullValue()) {
            return "\tnull\t|";
        } else {
            switch (type) {
                case BOOL:
                    return "\t" + value.getBoolValue() + "\t|";
                case INT8:
                case INT16:
                case INT32:
                    return "\t" + value.getInt32Value() + "\t|";
                case INT64:
                case UNIXTIME_MICROS:
                case BINARY:
                case STRING:
                    return "\t" + value.getInt64Value() + "\t|";
                case FLOAT:
                    return "\t" + value.getFloatValue() + "\t|";
                case DOUBLE:
                    return "\t" + value.getDoubleValue() + "\t|";
            }
        }
        return "";
    }

    public static void print(SelectResponse.SelectResponsePB response) {

        print(response, defaultIndent);
    }

    public static void println(String out, String indent) {

        System.out.println(indent + out);
    }
}
