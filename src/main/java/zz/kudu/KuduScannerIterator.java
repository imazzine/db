package zz.kudu;

import org.apache.kudu.client.KuduException;
import org.apache.kudu.client.KuduScanner;
import org.apache.kudu.client.RowResultIterator;

import java.util.Iterator;

public class KuduScannerIterator implements Iterator<Row> {

    private KuduScanner scanner;
    private RowResultIterator rowResultIterator;
    private int rowNumber = 0;
    private Exception error = null;

    public KuduScannerIterator(KuduScanner scanner) throws KuduException {

        this.scanner = scanner;
        if (this.scanner.hasMoreRows()) {

            rowResultIterator = scanner.nextRows();
        }
    }

    @Override
    public boolean hasNext() {

        if (rowResultIterator.hasNext()) {
            return true;
        }
        if (scanner.hasMoreRows()) {
            try {
                rowResultIterator = scanner.nextRows();
            } catch (KuduException e) {
                error = e;
                return false;
            }
        }

        return rowResultIterator.hasNext();
    }

    @Override
    public Row next() {

        return new Row(rowResultIterator.next(), rowNumber++);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }

    public Exception getError() {
        return error;
    }
}
