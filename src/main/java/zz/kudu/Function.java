package zz.kudu;

import zz.common.Digestable;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collector;

public class Function implements Digestable {

    private Function.Name name;
    private Function.Type type;
    private List<Expression> arguments;

    private org.apache.kudu.Type kuduType;
    private Collector collector;
    private java.util.function.Function<List<Comparable>, Comparable> valueFunction;

    public enum Type {
        AGGREGATE, SCALAR
    }

    public enum Name {
        AVG, MAX, MIN, SUM, COUNT
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(name.name());
        sb.append("(");
        int count = 0;
        for (Expression expression : arguments) {
            if (count > 0) {
                sb.append(", ");
            }
            sb.append(expression.toString());
            count++;
        }
        sb.append(")");
        return sb.toString();
    }

    private void bindFunctionArtifacts() throws Exception {
        Expression arg = null;
        Expression.Type argumentType;
        if (type == Type.AGGREGATE) {
            arg = arguments.get(0);
            argumentType = arg.getType();
            if (argumentType == Expression.Type.FUNCTION) {
                Function argFun = (Function) arg.getElement();
                if (argFun.getType() == Type.AGGREGATE) {
                    throw new Exception("Aggregate functions cannot be nested: "
                            + name + "(" + argFun.getName() + ")");
                }
            }
            valueFunction = argValues -> argValues.get(0);
        }
        switch (name) {
            case AVG:
                kuduType = org.apache.kudu.Type.DOUBLE;
                switch (arg.getKuduType()) {
                    case INT32:
                    case INT16:
                    case INT8:
                        collector = Expression.newAvgIntCollector();
                        break;
                    case INT64:
                        collector = Expression.newAvgLongCollector();
                        break;
                    case FLOAT:
                    case DOUBLE:
                    case UNIXTIME_MICROS:
                        collector = Expression.newAvgDoubleCollector();
                        break;
                    default:
                        throw new Exception("Unsupported argument type for "
                                + name + ": " + arg.getKuduType());
                }
                break;
            case MAX:
            case MIN:
                kuduType = arg.getKuduType();
                if (name == Name.MAX) {
                    collector = Expression.newMaxCollector();
                } else {
                    collector = Expression.newMinCollector();
                }
                break;
            case SUM:
                switch (arg.getKuduType()) {
                    case INT32:
                    case INT16:
                    case INT8:
                        collector = Expression.newSumIntCollector();
                        kuduType = org.apache.kudu.Type.INT32;
                        break;
                    case INT64:
                        collector = Expression.newSumLongCollector();
                        kuduType = org.apache.kudu.Type.INT64;
                        break;
                    case FLOAT:
                    case DOUBLE:
                    case UNIXTIME_MICROS:
                        collector = Expression.newSumDoubleCollector();
                        kuduType = org.apache.kudu.Type.DOUBLE;
                        break;
                    default:
                        throw new Exception("Unsupported argument type for "
                                + name + ": " + arg.getKuduType());
                }
                break;
            case COUNT:
                kuduType = org.apache.kudu.Type.INT64;
                collector = Expression.newCountCollector();
                break;
            default:
                throw new Exception("Unsupported aggregate function: " + name);
        }
    }

    void bind(Source.Item sourceItem) throws Exception {
        for (Expression expression : arguments) {
            expression.bind(sourceItem);
        }
        bindFunctionArtifacts();
    }

    void bind(List<Source> sources) throws Exception {
        for (Expression expression : arguments) {
            expression.bind(sources);
        }
        bindFunctionArtifacts();
    }

    public Comparable getValue(Row row) {
        List<Comparable> argumentValues = new ArrayList<>(arguments.size());
        for (Expression expression : arguments) {
            argumentValues.add(expression.getValue(row));
        }
        return valueFunction.apply(argumentValues);
    }

    public Comparable getValue(Row leftRow, Row rightRow) { // For join
        List<Comparable> argumentValues = new ArrayList<>(arguments.size());
        for (Expression expression : arguments) {
            argumentValues.add(expression.getValue(leftRow, rightRow));
        }
        return valueFunction.apply(argumentValues);
    }

    public void setOffset() {
        for (Expression expression : arguments) {
            expression.setOffset();
        }
    }

    public Function(Function.Name name) {
        this.name = name;
        switch (this.name) {
            case AVG:
            case MAX:
            case MIN:
            case SUM:
            case COUNT:
                this.type = Type.AGGREGATE;
                break;
        }
        this.arguments = new ArrayList<>();
    }

    public void addArgument(Expression argument) {
        arguments.add(argument);
    }

    public void addAllArguments(List<Expression> args) {
        arguments.addAll(args);
    }

    public Collector getCollector() throws Exception {
        return collector;
    }

    public org.apache.kudu.Type getKuduType() {
        return kuduType;
    }

    public List<Expression> getArguments() {
        return arguments;
    }

    public Function.Name getName() {
        return name;
    }

    public Function.Type getType() {
        return type;
    }

    @Override
    public void updateDigest(MessageDigest md) {
        Digestable.updateDigestForNullableStrings(md, name.name(), type.name());
        Digestable.updateDigestForCollectionMember(md, arguments);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Function function = (Function) o;
        if (name != function.name) return false;
        if (type != function.type) return false;
        return arguments.equals(function.arguments);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + arguments.hashCode();
        return result;
    }

    public boolean equalsNoAlias(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Function function = (Function) o;
        if (name != function.name) return false;
        if (type != function.type) return false;
        return equalsArgumentsNoAlias(function.arguments);
    }

    public boolean equalsArgumentsNoAlias(List<Expression> otherArguments) {
        if (otherArguments == this)
            return true;
        ListIterator<Expression> e1 = arguments.listIterator();
        ListIterator<Expression> e2 = otherArguments.listIterator();
        while (e1.hasNext() && e2.hasNext()) {
            Expression expr1 = e1.next();
            Expression expr2 = e2.next();
            if (!(expr1 == null ? expr2 == null : expr1.equalsNoAlias(expr2)))
                return false;
        }
        return !(e1.hasNext() || e2.hasNext());
    }
}
