package zz.kudu;

import com.google.protobuf.ByteString;
import org.apache.kudu.Type;
import org.apache.kudu.client.Bytes;
import org.apache.kudu.client.KuduClient;
import zz.kudu.sql.SelectResponse;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class View {

    private SelectResponse.OutputType outputType = null;
    private Projection projection;
    private List<Source> sources;
    private Predicate predicate;
    private List<Expression> groupByElements;
    private Map<Expression, Integer> orderByMap;
    private long limit = -1L;
    private long offset = 0L;

    private long rowCount = 0;
    private Number[] mins = null;
    private Number[] maxs = null;
    private ByteString indirectData = ByteString.EMPTY;
    private ByteString[] data = null;
    private long currentIndirectOffest = 0L;
    private Stream<Row> stream = null;
    private int sourceOffset = 0;

    private boolean bound = false;
    private boolean sorted = false;
    private boolean ignoreOrderBy = false;

    public boolean isIgnoreOrderBy() {
        return ignoreOrderBy;
    }

    // For insert operations using View
    public void setIgnoreOrderBy(boolean ignoreOrderBy) {
        this.ignoreOrderBy = ignoreOrderBy;
    }

    public List<Row> toList() {
        List<Row> rows = new ArrayList<>();
        if (stream != null) {
            if (offset > 0L) {
                stream = stream.skip(offset);
            }
            if (limit >= 0L) {
                stream = stream.limit(limit);
            }
            if (isSorted()) {
                stream.forEachOrdered(row -> addRowToList(row, rows));
            } else {
                stream.forEach(row -> addRowToList(row, rows));
            }
        }
        return rows;
    }

    public synchronized void addRowToList(Row row, List rows) {
        Comparable[] values = new Comparable[getColumnCount()];
        for (int i = 0; i < getColumnCount(); i++) {
            Expression expression = getProjectColumns().get(i);
            values[i] = expression.getValue(row);
        }
        rows.add(new Row(rows.size(), values));
    }

    public void addRows() {
        clearData();
        if (stream != null) {
            if (offset > 0L) {
                stream = stream.skip(offset);
            }
            if (limit >= 0L) {
                stream = stream.limit(limit);
            }
            if (isSorted()) {
                stream.forEachOrdered(this::addRow);
            } else {
                stream.forEach(this::addRow);
            }
        }
    }

    public synchronized void addRow(Row row) {
        for (int i = 0; i < getColumnCount(); i++) {
            Expression expression = getProjectColumns().get(i);
            Object objectValue = expression.getValue(row);
            Type type = expression.getKuduType();
            if (type == Type.STRING) {
                String value = (String) objectValue;
                if (value == null) {
                    value = "";
                }
                if (mins[i] == null || value.length() < (Long) mins[i]) {
                    mins[i] = Long.valueOf(value.length());
                }
                if (maxs[i] == null || value.length() > (Long) maxs[i]) {
                    maxs[i] = Long.valueOf(value.length());
                }
                ByteString offsetBS = ByteString.copyFrom(Bytes.fromLong(currentIndirectOffest));
                ByteString lenBS = ByteString.copyFrom(Bytes.fromLong((long) value.length()));
                if (outputType == SelectResponse.OutputType.COLUMNS) {
                    data[i] = data[i].concat(offsetBS);
                    data[i] = data[i].concat(lenBS);
                } else {
                    data[0] = data[0].concat(offsetBS);
                    data[0] = data[0].concat(lenBS);
                }
                indirectData = indirectData.concat(ByteString.copyFromUtf8(value));
                currentIndirectOffest += value.length();
            } else if (type == Type.BINARY) {
                ByteBuffer value = (ByteBuffer) objectValue;
                if (mins[i] == null || value.remaining() < (Long) mins[i]) {
                    mins[i] = Long.valueOf(value.remaining());
                }
                if (maxs[i] == null || value.remaining() > (Long) maxs[i]) {
                    maxs[i] = Long.valueOf(value.remaining());
                }
                ByteString offsetBS = ByteString.copyFrom(Bytes.fromLong(currentIndirectOffest));
                ByteString lenBS = ByteString.copyFrom(Bytes.fromLong((long) value.remaining()));
                if (outputType == SelectResponse.OutputType.COLUMNS) {
                    data[i] = data[i].concat(offsetBS);
                    data[i] = data[i].concat(lenBS);
                } else {
                    data[0] = data[0].concat(offsetBS);
                    data[0] = data[0].concat(lenBS);
                }
                indirectData = indirectData.concat(ByteString.copyFrom(value));
                currentIndirectOffest += value.remaining();
            } else {
                byte[] valueBytes = getBytesFieldValueAndMinMax(objectValue, type, i, mins, maxs);
                ByteString valueBS = ByteString.copyFrom(valueBytes);
                if (outputType == SelectResponse.OutputType.COLUMNS) {
                    data[i] = data[i].concat(valueBS);
                } else {
                    data[0] = data[0].concat(valueBS);
                }
            }
        }
        rowCount++;
    }

    public static byte[] getBytesFieldValueAndMinMax(Object value, Type fieldType, int columnIndex, Number[] mins, Number[] maxs) {
        switch (fieldType) {
            case DOUBLE:
                if (value == null) {
                    return Bytes.fromDouble(-Double.MAX_VALUE); // -1.7976931348623157E308 or -4.9E-324?
                }
                Double valueDouble = (Double) value;
                if (mins != null) {
                    if (mins[columnIndex] == null || valueDouble.compareTo((Double) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueDouble;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueDouble.compareTo((Double) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueDouble;
                    }
                }
                return Bytes.fromDouble(valueDouble);
            case BOOL:
                if (value == null) {
                    return Bytes.fromUnsignedByte(Byte.MIN_VALUE); // -128
                }
                Byte valueBoolean = (Boolean) value ? Byte.valueOf((byte) 1) : Byte.valueOf((byte) 0);
                if (mins != null) {
                    if (mins[columnIndex] == null || valueBoolean.compareTo((Byte) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueBoolean;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueBoolean.compareTo((Byte) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueBoolean;
                    }
                }
                return Bytes.fromBoolean((Boolean) value);
            case UNIXTIME_MICROS:
            case INT64:
                if (value == null) {
                    return Bytes.fromLong(Long.MIN_VALUE); // -9223372036854775808
                }
                Long valueLong = (Long) value;
                if (mins != null) {
                    if (mins[columnIndex] == null || valueLong.compareTo((Long) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueLong;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueLong.compareTo((Long) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueLong;
                    }
                }
                return Bytes.fromLong(valueLong);
            case INT32:
                if (value == null) {
                    return Bytes.fromInt(Integer.MIN_VALUE); // -2147483648
                }
                Integer valueInt = (Integer) value;
                if (mins != null) {
                    if (mins[columnIndex] == null || valueInt.compareTo((Integer) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueInt;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueInt.compareTo((Integer) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueInt;
                    }
                }
                return Bytes.fromInt(valueInt);
            case INT16:
                if (value == null) {
                    return Bytes.fromShort(Short.MIN_VALUE); // -32768
                }
                Short valueShort = (Short) value;
                if (mins != null) {
                    if (mins[columnIndex] == null || valueShort.compareTo((Short) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueShort;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueShort.compareTo((Short) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueShort;
                    }
                }
                return Bytes.fromShort(valueShort);
            case INT8:
                if (value == null) {
                    return Bytes.fromUnsignedByte(Byte.MIN_VALUE); // -128
                }
                Byte valueByte = (Byte) value;
                if (mins != null) {
                    if (mins[columnIndex] == null || valueByte.compareTo((Byte) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueByte;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueByte.compareTo((Byte) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueByte;
                    }
                }
                return Bytes.fromUnsignedByte(valueByte);
            case FLOAT:
                if (value == null) {
                    return Bytes.fromFloat(-Float.MAX_VALUE);
                }
                Float valueFloat = (Float) value;
                if (mins != null) {
                    if (mins[columnIndex] == null || valueFloat.compareTo((Float) mins[columnIndex]) < 0) {
                        mins[columnIndex] = valueFloat;
                    }
                }
                if (maxs != null) {
                    if (maxs[columnIndex] == null || valueFloat.compareTo((Float) maxs[columnIndex]) > 0) {
                        maxs[columnIndex] = valueFloat;
                    }
                }
                return Bytes.fromFloat(valueFloat);
        }
        return null;
    }

    public Collector<Row, List<Object>, List<Object>> newRowCollector() {

        List<Collector> collectors = getProjectColumns().stream()
                .map(Expression::getCollector)
                .collect(Collectors.toList());

        Collector<Row, List<Object>, List<Object>> rowCollector = Collector.of(

                // supplier
                () -> collectors.stream().map(Collector::supplier)
                        .map(Supplier::get).collect(Collectors.toList()),

                // accumulator
                (supplierList, row) -> IntStream.range(0, collectors.size()).forEach(
                        i -> ((BiConsumer<Object, Comparable>) collectors.get(i).accumulator())
                                .accept(supplierList.get(i),
                                        getProjectColumns().get(i).getValue(row)
                                )),

                // combiner
                (supplierList1, supplierList2) -> {
                    IntStream.range(0, collectors.size()).forEach(
                            i -> supplierList1.set(i,
                                    ((BinaryOperator<Object>) collectors.get(i).combiner())
                                            .apply(supplierList1.get(i), supplierList2.get(i))));
                    return supplierList1;
                },

                // finisher
                supplierList -> {
                    IntStream.range(0, collectors.size()).forEach(
                            i -> supplierList.set(i,
                                    ((java.util.function.Function<Object, Object>) collectors
                                            .get(i).finisher())
                                            .apply(supplierList.get(i))));
                    return supplierList;
                }
        );

        return rowCollector;
    }

    public int getSourceOffset() {
        return sourceOffset;
    }

    public void setSourceOffset(int sourceOffset) {
        this.sourceOffset = sourceOffset;
    }

    public int setSourceOffsets() {
        int currentOffset = sourceOffset;
        for (Source source : sources) {
            currentOffset = source.setOffset(currentOffset);
        }
        for (Expression expression : getProjectColumns()) {
            expression.setOffset();
        }
        if (predicate != null) {
            predicate.setOffset(); // It sets offsets for predicate expressions.
        }
        return currentOffset;
    }

    public List<Source> getSources() {
        return sources;
    }

    public boolean isBound() {
        return bound;
    }

    public List<Expression> getProjectColumns() {
        return projection.getColumns();
    }

    int getColumnCount() {
        return getProjectColumns().size();
    }

    long getRowCount() {
        return rowCount;
    }

    Number[] getMins() {
        return mins;
    }

    Number[] getMaxs() {
        return maxs;
    }

    ByteString getIndirectData() {
        return indirectData;
    }

    ByteString[] getData() {
        return data;
    }

    public SelectResponse.OutputType getOutputType() {
        return outputType;
    }

    public Stream<Row> getStream() {
        return stream;
    }

    public Stream<Row> openStream(KuduClient client) throws Exception {

        // Extracting a stream from sources by join schema.
        if (sources.size() > 1) {
            int sourceCounter = 0;
            stream = sources.get(sourceCounter).open(client);
            List<Row> rightRows;
            Supplier<Stream<Row>> rightStreamSupplier;
            while (++sourceCounter < sources.size() - 1) {
                rightRows = sources.get(sourceCounter).open(client).collect(Collectors.toList());
                rightStreamSupplier = rightRows::stream;
                stream = Source.Join.crossJoin(stream, rightStreamSupplier);
            }
            rightRows = sources.get(sourceCounter).open(client).collect(Collectors.toList());
            rightStreamSupplier = rightRows::stream;
            stream = Source.Join.innerJoin(stream, rightStreamSupplier, predicate);
        } else {
            stream = sources.get(0).open(client);
            if (predicate != null && !predicate.isKuduSupported()) {
                stream = stream.filter(row -> predicate.apply(row)); // No joins.
            }
        }

        // Final operations.
        groupBy();
        orderBy();

        return stream;
    }

    public boolean isSorted() {
        return sorted;
    }

    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    public View(SelectResponse.OutputType outputType,
                Projection projection,
                List<Source> sources,
                Predicate predicate,
                List<Expression> groupByElements,
                Map<Expression, Integer> orderByMap,
                long limit,
                long offset) {

        this.outputType = outputType;
        this.projection = projection;
        this.sources = sources;
        this.predicate = predicate;
        this.groupByElements = groupByElements;
        this.orderByMap = orderByMap;
        this.limit = limit;
        this.offset = offset;
    }

    public View(Statement statement) {
        this(
                ((Statement.Options.Select) statement.getOptions()).getOutputType(),
                ((Statement.Options.Select) statement.getOptions()).getProjection(),
                statement.getSources(),
                ((Statement.Options.Select) statement.getOptions()).getPredicate(),
                ((Statement.Options.Select) statement.getOptions()).getGroupByList(),
                ((Statement.Options.Select) statement.getOptions()).getOrderByMap(),
                ((Statement.Options.Select) statement.getOptions()).getLimit(),
                ((Statement.Options.Select) statement.getOptions()).getOffset()
        );
    }

    private void clearData() {
        if (this.outputType == SelectResponse.OutputType.COLUMNS) {
            this.data = new ByteString[getColumnCount()];
        } else {
            this.data = new ByteString[1];
        }
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = ByteString.EMPTY;
        }
        rowCount = 0;
        mins = new Number[getColumnCount()];
        maxs = new Number[getColumnCount()];
    }

    public boolean linkAlias(Expression expression) {
        if (expression.getType() == Expression.Type.COLUMN) {
            for (Expression selectElement : getProjectColumns()) {
                if (selectElement.getAlias() != null
                        && selectElement.getAlias().equals(expression.getElement())) {
                    expression.setLink(selectElement);
                    return true;
                }
            }
        }
        return false;
    }

    public void bind(KuduClient client) throws Exception {
        for (Source source : sources) {
            source.bind(client);
        }
        projection.bind(sources);
        if (predicate != null) {
            predicate.bind(sources, predicate.isKuduSupported());
            if (!predicate.isKuduSupported() && predicate instanceof Predicate.And) {
                Predicate.And.optimize((Predicate.And) predicate, sources);
            }
        }
        if (groupByElements != null) {
            for (int i = 0; i < groupByElements.size(); i++) {
                Expression columnExpression = null;
                Expression groupByExpression = groupByElements.get(i);
                for (Expression expression : getProjectColumns()) {
                    if (groupByExpression.equals(expression)) { /*TODO: Check it*/
                        columnExpression = expression;
                        break;
                    }
                }
                if (columnExpression == null) {
                    String colName = groupByExpression.getAlias() == null ?
                            (String) groupByExpression.getElement() : groupByExpression.getAlias();
                    throw new Exception("Column '" + colName + "' not found for groupBy.");
                }
                groupByElements.set(i, columnExpression);
            }
        }
        bound = true;
    }

    private void groupBy() {
        if (projection.isHasAggregates() || (groupByElements != null && groupByElements.size() > 0)) {

            // If this view is used as a subquery
            // we need to set the offset to zero before grouping
            // and then restore the offsets on a global scale.
            int offsetForRestore = sourceOffset;
            sourceOffset = 0;
            setSourceOffsets();

            if (groupByElements == null || groupByElements.size() < 1) {
                List<Object> aggregatedFields = stream.collect(newRowCollector());
                Comparable[] oneRow = aggregatedFields.stream()
                        .map(value -> (Comparable) value)
                        .toArray(Comparable[]::new);
                stream = Stream.of(new Row(0, oneRow));
            } else {
                java.util.function.Function<Row, List<Comparable>> keyExtractor =
                        row -> groupByElements.stream()
                                .map(expression -> expression.getValue(row))
                                .collect(Collectors.toList());
                Collector<Row, List<Object>, List<Object>> rowCollector = newRowCollector();

                Collection<List<Object>> groupedRows = stream.collect(
                        Collectors.groupingBy(
                                keyExtractor,
                                rowCollector
                        )
                ).values();

                List<Row> rows = new ArrayList<>(groupedRows.size());
                int rowNumber = 0;
                for (List<Object> valuesList : groupedRows) {
                    rows.add(
                            new Row(
                                    rowNumber,
                                    valuesList.stream()
                                            .toArray(Comparable[]::new)
                            )
                    );
                    rowNumber++;
                }
                stream = rows.stream().parallel();
            }

            // Restore offsets for subquery case.
            sourceOffset = offsetForRestore;
            setSourceOffsets();
            // For select elements we breaks relations to their original sources.
            int offset = sourceOffset;
            for (Expression expression : getProjectColumns()) {
                expression.setOffset(offset++);
            }
        }
    }

    private void orderBy() {
        if (!ignoreOrderBy && orderByMap != null && orderByMap.size() > 0) {
            int[] sortOrder = new int[orderByMap.size()];
            int[] orderDirection = new int[orderByMap.size()];
            Iterator<Map.Entry<Expression, Integer>> iterator = orderByMap.entrySet().iterator();
            int index = 0;
            while (iterator.hasNext()) {
                Map.Entry<Expression, Integer> entry = iterator.next();
                int indexInSelectElements = getProjectColumns().indexOf(entry.getKey());
                sortOrder[index] = getProjectColumns().get(indexInSelectElements).getOffset();
                orderDirection[index] = entry.getValue();
                index++;
            }
            stream = stream.sorted(Row.getComparator(sortOrder, orderDirection));
            setSorted(true);
        }
    }

    public void open(KuduClient client) throws Exception {
        if (!bound) {
            bind(client); // Binding sources to kudu tables and expressions to sources
        }
        setSourceOffsets(); // At this moment kudu project columns are known so we can set offsets properly.
        openStream(client);
    }
}
