package zz.kudu;

import zz.common.Digestable;

import java.security.MessageDigest;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class Projection extends AbstractList<Projection.Element> implements Digestable {

    private List<Element> elements = new ArrayList<>();
    private List<Expression> columns = new ArrayList<>();
    private boolean hasAggregates = false;

    @Override
    public Element get(int index) {
        return elements.get(index);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean add(Element element) {
        return elements.add(element);
    }

    @Override
    public void updateDigest(MessageDigest md) {
        Digestable.updateDigestForCollectionMember(md, elements);
    }

    public void bind(List<Source> sources) throws Exception {
        for (Element element : elements) {
            element.bind(sources);
            columns.addAll(element.getExpressions());
        }
        for (Expression expression : columns) {
            if (expression.isAggregate()) {
                hasAggregates = true;
                break;
            }
        }
    }

    public List<Expression> getColumns() {
        return columns;
    }

    public boolean isHasAggregates() {
        return hasAggregates;
    }

    public static abstract class Element implements Digestable {

        public abstract void setSourceItemAlias(String sourceAlias);

        public abstract String getSourceItemAlias();

        public abstract void bind(List<Source> sources) throws Exception;

        public abstract List<Expression> getExpressions();

        public static class Asterisk extends Element {

            private String sourceItemAlias;
            private List<Expression> columns;

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(md, sourceItemAlias, "*");
            }

            @Override
            public void bind(List<Source> sources) throws Exception {
                if (sourceItemAlias == null) {
                    columns = new ArrayList<>();
                    for (Source source : sources) {
                        columns.addAll(source.getAsterisk(true));
                    }
                } else {
                    Source.Item item = Source.getItemByAlias(sourceItemAlias, sources);
                    if (item == null) {
                        throw new Exception("Source not found for expression '" + sourceItemAlias + ".*'.");
                    }
                    columns = item.getAsterisk(true);
                }
            }

            @Override
            public List<Expression> getExpressions() {
                return columns;
            }

            @Override
            public void setSourceItemAlias(String sourceItemAlias) {
                this.sourceItemAlias = sourceItemAlias;
            }

            @Override
            public String getSourceItemAlias() {
                return sourceItemAlias;
            }
        }
    }
}
