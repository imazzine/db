package zz.kudu;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.net.URI;

/**
 * WS Client for Apache Kudu
 *
 * @author imazzine
 */
@ServerEndpoint("/test")
public class KuduServerEndpoint {

    @OnOpen
    public void onOpen(Session session) {

        System.out.println("--------Server onOpen----------------");
    }

    @OnMessage
    public void onMessage(String message, Session session) {

        System.out.println("--------Server onMessage---: " + message);
    }

    @OnClose
    public void onClose(Session session) {

        System.out.println("--------Server onClose----------------");
    }

    @OnError
    public void onError(Throwable exception, Session session) {

        System.out.println("--------Server onError----------------");
    }
}
