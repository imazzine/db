package zz.kudu;

import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.client.KuduPredicate;
import zz.common.Digestable;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Predicate implements Digestable {

    abstract boolean isKuduSupported();

    abstract List<KuduPredicate> getKuduPredicates(Schema schema);

    abstract void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception;

    abstract public boolean apply(Row row);

    abstract public boolean apply(Row leftRow, Row rightRow); // join case

    abstract public void setOffset(); // default offset for expressions in predicate

    void bind(Source source, boolean addAsKuduPredicate) throws Exception {  // join case
        bind(Stream.of(source).collect(Collectors.toList()), addAsKuduPredicate);
    }

    public Predicate not() {
        return new Not(this);
    }

    public Predicate and(Predicate predicate) {
        return new And(predicate);
    }

    public Predicate or(Predicate predicate) {
        return new Or(predicate);
    }

    public static class Comparison extends Predicate {

        public enum Operator {
            GREATER,
            GREATER_EQUAL,
            EQUAL,
            LESS,
            LESS_EQUAL,
            NOT_EQUAL
        }

        private Expression leftExpression; // required
        private Expression rightExpression; // required
        private Operator operator; // required
        private KuduPredicate.ComparisonOp kuduOperator; // calculated
        private boolean kuduSupported = false;

        @Override
        public void updateDigest(MessageDigest md) {
            /* TODO: some comparison can be transformed to another form */
            leftExpression.updateDigest(md);
            Digestable.updateDigestForNullableStrings(md, operator.name());
            rightExpression.updateDigest(md);
        }

        public Comparison(Expression leftExpression, Operator operator, Expression rightExpression) {

            this.operator = operator;

            if (this.operator != Operator.NOT_EQUAL) {
                if (leftExpression.getType() == Expression.Type.COLUMN
                        && rightExpression.getType() == Expression.Type.CONSTANT) {
                    this.leftExpression = leftExpression;
                    this.rightExpression = rightExpression;
                    this.kuduSupported = true; // It might change after binding.
                } else if (leftExpression.getType() == Expression.Type.CONSTANT
                        && rightExpression.getType() == Expression.Type.COLUMN) {
                    this.leftExpression = rightExpression;
                    this.rightExpression = leftExpression;
                    this.kuduSupported = true; // It might change after binding.
                } else {
                    this.leftExpression = leftExpression;
                    this.rightExpression = rightExpression;
                    this.kuduSupported = false;
                }
            } else {
                this.leftExpression = leftExpression;
                this.rightExpression = rightExpression;
                this.kuduSupported = false;
            }

            switch (this.operator) {
                case LESS:
                    this.kuduOperator = KuduPredicate.ComparisonOp.LESS;
                    break;
                case EQUAL:
                    this.kuduOperator = KuduPredicate.ComparisonOp.EQUAL;
                    break;
                case GREATER:
                    this.kuduOperator = KuduPredicate.ComparisonOp.GREATER;
                    break;
                case LESS_EQUAL:
                    this.kuduOperator = KuduPredicate.ComparisonOp.LESS_EQUAL;
                    break;
                case GREATER_EQUAL:
                    this.kuduOperator = KuduPredicate.ComparisonOp.GREATER_EQUAL;
                    break;
                case NOT_EQUAL:
                    this.kuduOperator = null; // Kudu doesn't support
                    break;
            }
        }

        public KuduPredicate toKuduPredicate(Schema schema) {

            if (!kuduSupported) {
                return null;
            }

            ColumnSchema columnSchema = null;
            try {
                // The left expression always presents column,
                // the right expression always presents constant
                String columnName = (String) leftExpression.getElement();
                if (leftExpression.getSourceItem() != null) {
                    String columnNameByAlias = leftExpression.getSourceItem().getColumnByAlias(columnName);
                    if (columnNameByAlias != null) {
                        columnName = columnNameByAlias;
                    }
                }
                columnSchema = schema.getColumn(columnName);
            } catch (Exception e) {
                return null; // Schema doesn't present this column.
            }

            org.apache.kudu.Type columnType = columnSchema.getType();

            switch (columnType) {
                case STRING:
                    return KuduPredicate.newComparisonPredicate(
                            columnSchema,
                            kuduOperator,
                            (String) rightExpression.getElement());
                case DOUBLE:
                    return KuduPredicate.newComparisonPredicate(
                            columnSchema,
                            kuduOperator,
                            ((Number) rightExpression.getElement()).doubleValue());
                case FLOAT:
                    return KuduPredicate.newComparisonPredicate(
                            columnSchema,
                            kuduOperator,
                            ((Number) rightExpression.getElement()).floatValue());
                case INT32:
                case INT16:
                case INT8:
                case INT64:
                case UNIXTIME_MICROS:
                    return KuduPredicate.newComparisonPredicate(
                            columnSchema,
                            kuduOperator,
                            ((Number) rightExpression.getElement()).longValue());
                case BOOL:
                    return KuduPredicate.newComparisonPredicate(
                            columnSchema,
                            kuduOperator,
                            (Boolean) rightExpression.getElement());
                case BINARY:
                    return KuduPredicate.newComparisonPredicate(
                            columnSchema,
                            kuduOperator,
                            (byte[]) rightExpression.getElement());
            }

            return null;
        }

        @Override
        public boolean isKuduSupported() {
            return kuduSupported;
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            List<KuduPredicate> kuduPredicates = new ArrayList<>();
            KuduPredicate kuduPredicate = toKuduPredicate(schema);
            if (kuduPredicate != null) {
                kuduPredicates.add(kuduPredicate);
            }
            return kuduPredicates;
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            boolean addAsProjectColumn = !addAsKuduPredicate;
            leftExpression.bind(sources, addAsProjectColumn);
            if (leftExpression.isLink()) {
                kuduSupported = kuduSupported && leftExpression.getType() == Expression.Type.COLUMN;
            }
            if (addAsKuduPredicate && leftExpression.getSourceItem() != null) {
                kuduSupported = kuduSupported && leftExpression.getSourceItem().addPredicate(this);
                if (!kuduSupported) { // We need to include expression to source item's project columns.
                    bind(sources, false); // rebind as not kudu predicate
                }
            }
            rightExpression.bind(sources, addAsProjectColumn);
            if (addAsKuduPredicate && rightExpression.getSourceItem() != null) {
                kuduSupported = kuduSupported && rightExpression.getSourceItem().addPredicate(this);
                if (!kuduSupported) { // We need to include expression to source item's project columns.
                    bind(sources, false); // rebind as not kudu predicate
                }
            }
        }

        @Override
        public boolean apply(Row row) {
            switch (operator) {
                case EQUAL:
                    return leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) == 0;
                case GREATER:
                    return leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) > 0;
                case GREATER_EQUAL:
                    return leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) > 0
                            || leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) == 0;
                case LESS:
                    return leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) < 0;
                case LESS_EQUAL:
                    return leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) < 0
                            || leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) == 0;
                case NOT_EQUAL:
                    return leftExpression.getValue(row).compareTo(rightExpression.getValue(row)) != 0;
            }
            return false;
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            switch (operator) {
                case EQUAL:
                    return leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) == 0;
                case GREATER:
                    return leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) > 0;
                case GREATER_EQUAL:
                    return leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) > 0
                            || leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) == 0;
                case LESS:
                    return leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) < 0;
                case LESS_EQUAL:
                    return leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) < 0
                            || leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) == 0;
                case NOT_EQUAL:
                    return leftExpression.getValue(leftRow, rightRow)
                            .compareTo(rightExpression.getValue(leftRow, rightRow)) != 0;
            }
            return false;
        }

        @Override
        public void setOffset() {
            leftExpression.setOffset();
            rightExpression.setOffset();
        }
    }

    public static class InList extends Predicate {

        private Expression leftExpression; // required
        private List<Expression> rightExpressions; // required
        private boolean kuduSupported = false;

        @Override
        public void updateDigest(MessageDigest md) {
            leftExpression.updateDigest(md);
            /* TODO: sort expressions before */
            for (Expression expression : rightExpressions) {
                expression.updateDigest(md);
            }
        }

        public InList(Expression leftExpression, List<Expression> rightExpressions) {
            this.leftExpression = leftExpression;
            this.rightExpressions = rightExpressions;
            if (this.leftExpression.getType() == Expression.Type.COLUMN) {
                this.kuduSupported = true; // It might change after binding.
                for (Expression expression : this.rightExpressions) {
                    if (expression.getType() != Expression.Type.CONSTANT) {
                        this.kuduSupported = false;
                        break;
                    }
                }
            } else {
                this.kuduSupported = false;
            }
        }

        public KuduPredicate toKuduPredicate(Schema schema) {

            if (!kuduSupported) {
                return null;
            }

            ColumnSchema columnSchema = null;
            try {
                // The left expression always presents column.
                String columnName = (String) leftExpression.getElement();
                if (leftExpression.getSourceItem() != null) {
                    String columnNameByAlias = leftExpression.getSourceItem().getColumnByAlias(columnName);
                    if (columnNameByAlias != null) {
                        columnName = columnNameByAlias;
                    }
                }
                columnSchema = schema.getColumn(columnName);
            } catch (Exception e) {
                return null; // Schema doesn't present this column.
            }

            // The right expressions always present list of constants.
            List inValues = new ArrayList(rightExpressions.size());
            for (Expression expression : rightExpressions) {
                inValues.add(expression.getElement());
            }

            return KuduPredicate.newInListPredicate(columnSchema, inValues);
        }

        @Override
        public boolean isKuduSupported() {
            return kuduSupported;
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            List<KuduPredicate> kuduPredicates = new ArrayList<>();
            KuduPredicate kuduPredicate = toKuduPredicate(schema);
            if (kuduPredicate != null) {
                kuduPredicates.add(kuduPredicate);
            }
            return kuduPredicates;
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            boolean addAsProjectColumn = !addAsKuduPredicate;
            leftExpression.bind(sources, addAsProjectColumn);
            if (leftExpression.isLink()) {
                kuduSupported = kuduSupported && leftExpression.getType() == Expression.Type.COLUMN;
            }
            if (addAsKuduPredicate && leftExpression.getSourceItem() != null) {
                kuduSupported = kuduSupported && leftExpression.getSourceItem().addPredicate(this);
                if (!kuduSupported) { // We need to include expression to source item's project columns.
                    bind(sources, false); // rebind as not kudu predicate
                }
            }
            for (Expression expression : rightExpressions) {
                expression.bind(sources, addAsProjectColumn);
                if (addAsKuduPredicate && expression.getSourceItem() != null) {
                    expression.getSourceItem().addPredicate(this); /* TODO: Do I need to check kuduSupported here? */
                }
            }
        }

        @Override
        public boolean apply(Row row) {
            Comparable leftValue = leftExpression.getValue(row);
            for (Expression expression : rightExpressions) {
                if (expression.getValue(row).compareTo(leftValue) == 0) { /* TODO: Nulls */
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            Comparable leftValue = leftExpression.getValue(leftRow, rightRow);
            for (Expression expression : rightExpressions) {
                if (expression.getValue(leftRow, rightRow).compareTo(leftValue) == 0) { /* TODO: Nulls */
                    return true;
                }
            }
            return false;
        }

        @Override
        public void setOffset() {
            leftExpression.setOffset();
            for (Expression expression : rightExpressions) {
                expression.setOffset();
            }
        }
    }

    public static class IsNull extends Predicate {

        private Expression expression; // required
        private boolean kuduSupported = false;

        @Override
        public void updateDigest(MessageDigest md) {
            expression.updateDigest(md);
            Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
        }

        public IsNull(Expression expression) {
            this.expression = expression;
            if (this.expression.getType() == Expression.Type.COLUMN) {
                this.kuduSupported = true; // It might change after binding.
            } else {
                this.kuduSupported = false;
            }
        }

        public KuduPredicate toKuduPredicate(Schema schema) {

            if (!kuduSupported) {
                return null;
            }

            ColumnSchema columnSchema = null;
            try {
                // The left expression always presents column.
                String columnName = (String) expression.getElement();
                if (expression.getSourceItem() != null) {
                    String columnNameByAlias = expression.getSourceItem().getColumnByAlias(columnName);
                    if (columnNameByAlias != null) {
                        columnName = columnNameByAlias;
                    }
                }
                columnSchema = schema.getColumn(columnName);
            } catch (Exception e) {
                return null; // Schema doesn't present this column.
            }

            return KuduPredicate.newIsNullPredicate(columnSchema);
        }

        @Override
        public boolean isKuduSupported() {
            return kuduSupported;
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            List<KuduPredicate> kuduPredicates = new ArrayList<>();
            KuduPredicate kuduPredicate = toKuduPredicate(schema);
            if (kuduPredicate != null) {
                kuduPredicates.add(kuduPredicate);
            }
            return kuduPredicates;
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            boolean addAsProjectColumn = !addAsKuduPredicate;
            expression.bind(sources, addAsProjectColumn);
            if (expression.isLink()) {
                kuduSupported = kuduSupported && expression.getType() == Expression.Type.COLUMN;
            }
            if (addAsKuduPredicate && expression.getSourceItem() != null) {
                kuduSupported = kuduSupported && expression.getSourceItem().addPredicate(this);
                if (!kuduSupported) { // We need to include expression to source item's project columns.
                    bind(sources, false); // rebind as not kudu predicate
                }
            }
        }

        @Override
        public boolean apply(Row row) {
            return expression.getValue(row) == null;
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            return expression.getValue(leftRow, rightRow) == null;
        }

        @Override
        public void setOffset() {
            expression.setOffset();
        }
    }

    public static class IsNotNull extends Predicate {

        private Expression expression; // required
        private boolean kuduSupported = false;

        @Override
        public void updateDigest(MessageDigest md) {
            expression.updateDigest(md);
            Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
        }

        public IsNotNull(Expression expression) {
            this.expression = expression;
            if (this.expression.getType() == Expression.Type.COLUMN) {
                this.kuduSupported = true; // It might change after binding.
            } else {
                this.kuduSupported = false;
            }
        }

        public KuduPredicate toKuduPredicate(Schema schema) {

            if (!kuduSupported) {
                return null;
            }

            ColumnSchema columnSchema = null;
            try {
                // The expression always presents column.
                String columnName = (String) expression.getElement();
                if (expression.getSourceItem() != null) {
                    String columnNameByAlias = expression.getSourceItem().getColumnByAlias(columnName);
                    if (columnNameByAlias != null) {
                        columnName = columnNameByAlias;
                    }
                }
                columnSchema = schema.getColumn(columnName);
            } catch (Exception e) {
                return null; // Schema doesn't present this column.
            }

            return KuduPredicate.newIsNotNullPredicate(columnSchema);
        }

        @Override
        public boolean isKuduSupported() {
            return kuduSupported;
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            List<KuduPredicate> kuduPredicates = new ArrayList<>();
            KuduPredicate kuduPredicate = toKuduPredicate(schema);
            if (kuduPredicate != null) {
                kuduPredicates.add(kuduPredicate);
            }
            return kuduPredicates;
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            boolean addAsProjectColumn = !addAsKuduPredicate;
            expression.bind(sources, addAsProjectColumn);
            if (expression.isLink()) {
                kuduSupported = kuduSupported && expression.getType() == Expression.Type.COLUMN;
            }
            if (addAsKuduPredicate && expression.getSourceItem() != null) {
                kuduSupported = kuduSupported && expression.getSourceItem().addPredicate(this);
                if (!kuduSupported) { // We need to include expression to source item's project columns.
                    bind(sources, false); // rebind as not kudu predicate
                }
            }
        }

        @Override
        public boolean apply(Row row) {
            return expression.getValue(row) != null;
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            return expression.getValue(leftRow, rightRow) != null;
        }

        @Override
        public void setOffset() {
            expression.setOffset();
        }
    }

    //-----------------------Complex predicates-----------------------------------

    public static class Not extends Predicate {

        private Predicate predicate; // required

        @Override
        public void updateDigest(MessageDigest md) {
            Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
            predicate.updateDigest(md);
        }

        public Not(Predicate predicate) {
            this.predicate = predicate;
        }

        @Override
        public boolean isKuduSupported() {
            return false;
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            return new ArrayList<>();
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            predicate.bind(sources, addAsKuduPredicate);
        }

        @Override
        public boolean apply(Row row) {
            return !predicate.apply(row);
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            return !predicate.apply(leftRow, rightRow);
        }

        @Override
        public void setOffset() {
            predicate.setOffset();
        }

        @Override
        public Predicate not() {
            return predicate; /* TODO: is it right? */
        }
    }

    public static class And extends Predicate {

        private List<Predicate> predicates; // required

        public static void optimize(Predicate.And andPredicate, List<Source> sources) throws Exception {
            for (Predicate predicate : andPredicate.predicates) {
                if (predicate.isKuduSupported()) {
                    predicate.bind(sources, true);
                } else if (predicate instanceof Predicate.And) {
                    optimize((Predicate.And) predicate, sources);
                }
            }
        }

        @Override
        public void updateDigest(MessageDigest md) {
            Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
            /* TODO: sort predicates before */
            for (Predicate predicate : predicates) {
                predicate.updateDigest(md);
            }
        }

        public And(List<Predicate> predicates) {
            this.predicates = predicates;
        }

        private void append(Predicate predicate) {
            predicates.add(predicate);
        }

        public And(Predicate predicate) {
            this.predicates = new ArrayList<>();
            append(predicate);
        }

        @Override
        public boolean isKuduSupported() {
            boolean isSupported = true;
            Iterator<Predicate> iterator = predicates.iterator();
            while (isSupported && iterator.hasNext()) {
                isSupported = iterator.next().isKuduSupported();
            }
            return isSupported;
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            List<KuduPredicate> kuduPredicates = new ArrayList<>();
            if (isKuduSupported()) {
                for (Predicate predicate : predicates) {
                    kuduPredicates.addAll(predicate.getKuduPredicates(schema));
                }
            }
            return kuduPredicates;
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            for (Predicate predicate : predicates) {
                predicate.bind(sources, addAsKuduPredicate);
            }
        }

        @Override
        public boolean apply(Row row) {
            Iterator<Predicate> iterator = predicates.iterator();
            boolean result = iterator.hasNext() && iterator.next().apply(row);
            while (result && iterator.hasNext()) {
                result = iterator.next().apply(row);
            }
            return result;
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            Iterator<Predicate> iterator = predicates.iterator();
            boolean result = iterator.hasNext() && iterator.next().apply(leftRow, rightRow);
            while (result && iterator.hasNext()) {
                result = iterator.next().apply(leftRow, rightRow);
            }
            return result;
        }

        @Override
        public void setOffset() {
            for (Predicate predicate : predicates) {
                predicate.setOffset();
            }
        }

        @Override
        public Predicate and(Predicate predicate) {
            append(predicate);
            return this;
        }
    }

    public static class Or extends Predicate {

        private List<Predicate> predicates; // required

        @Override
        public void updateDigest(MessageDigest md) {
            Digestable.updateDigestForNullableStrings(md, this.getClass().getName());
            /* TODO: sort predicates before */
            for (Predicate predicate : predicates) {
                predicate.updateDigest(md);
            }
        }

        public Or(List<Predicate> predicates) {
            this.predicates = predicates;
        }

        private void append(Predicate predicate) {
            this.predicates.add(predicate);
        }

        public Or(Predicate predicate) {
            this.predicates = new ArrayList<>();
            append(predicate);
        }

        @Override
        public boolean isKuduSupported() {
            return predicates.size() == 1 && predicates.get(0).isKuduSupported();
        }

        @Override
        public List<KuduPredicate> getKuduPredicates(Schema schema) {
            List<KuduPredicate> kuduPredicates = new ArrayList<>();
            if (isKuduSupported()) {
                kuduPredicates.addAll(predicates.get(0).getKuduPredicates(schema));
            }
            return kuduPredicates;
        }

        @Override
        void bind(List<Source> sources, boolean addAsKuduPredicate) throws Exception {
            for (Predicate predicate : predicates) {
                predicate.bind(sources, addAsKuduPredicate);
            }
        }

        @Override
        public boolean apply(Row row) {
            Iterator<Predicate> iterator = predicates.iterator();
            boolean result = iterator.hasNext() && iterator.next().apply(row);
            while (!result && iterator.hasNext()) {
                result = iterator.next().apply(row);
            }
            return result;
        }

        @Override
        public boolean apply(Row leftRow, Row rightRow) {
            Iterator<Predicate> iterator = predicates.iterator();
            boolean result = iterator.hasNext() && iterator.next().apply(leftRow, rightRow);
            while (!result && iterator.hasNext()) {
                result = iterator.next().apply(leftRow, rightRow);
            }
            return result;
        }

        @Override
        public void setOffset() {
            for (Predicate predicate : predicates) {
                predicate.setOffset();
            }
        }

        @Override
        public Predicate or(Predicate predicate) {
            append(predicate);
            return this;
        }
    }
}
