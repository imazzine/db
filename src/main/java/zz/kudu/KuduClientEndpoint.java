package zz.kudu;

import javax.websocket.*;
import java.net.URI;

/**
 * WS Client for Apache Kudu
 *
 * @author imazzine
 */
@ClientEndpoint
public class KuduClientEndpoint{

    Session userSession = null;
    private MessageHandler messageHandler;

//    public KuduClientEndpoint(URI endpointURI) {
//
//        try {
//
//            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
//            container.connectToServer(this, endpointURI);
//
//        } catch (Exception e) {
//
//            throw new RuntimeException(e);
//        }
//    }

//    public KuduClientEndpoint(URI endpointURI) {
    public KuduClientEndpoint( ) {

        System.out.println("--------Constructor start-------------");

        try {

            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, new URI("ws://localhost:9999"));

            this.addMessageHandler(new KuduClientEndpoint.MessageHandler() {

                public void handleMessage(String message) {

                    System.out.println("--------handleMessage from client-------------");
                }
            });

            this.sendMessage("Hi There!!");

        } catch (Exception e) {

            throw new RuntimeException(e);
        }

        System.out.println("--------Constructor end-------------");
    }

    /**
     * Callback for Connection open events.
     *
     * @param userSession
     *            the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {

        this.userSession = userSession;
    }

    /**
     * Callback for Connection close events.
     *
     * @param userSession
     *            the userSession which is getting closed.
     * @param reason
     *            the reason for connection close
     */
    @OnClose
    public void onClose(Session userSession, CloseReason reason) {

        this.userSession = null;
    }

    /**
     * Callback for Message Events. This method will be invoked when a
     * client send a message.
     *
     * @param message
     *            The text message
     */
    @OnMessage
    public void onMessage(String message) {

        if (this.messageHandler != null) {

            this.messageHandler.handleMessage(message);
        }
    }

    /**
     * register message handler
     *
     * @param msgHandler
     */
    public void addMessageHandler(MessageHandler msgHandler) {

        this.messageHandler = msgHandler;
    }

    /**
     * Send a message.
     *
     * @param message
     */
    public void sendMessage(String message) {

        this.userSession.getAsyncRemote().sendText(message);
    }

    /**
     * Message handler.
     *
     * @author
     */
    public static interface MessageHandler {

        public void handleMessage(String message);
    }


//    /**
//     * main
//     * @param args
//     * @throws Exception
//     */
//    public static void main(String[] args) throws Exception {
//
//        final KuduClientEndpoint clientEndPoint = new KuduClientEndpoint(
//
//                new URI("ws://localhost:9999")
//        );
//
//        clientEndPoint.addMessageHandler(new KuduClientEndpoint.MessageHandler() {
//
//            public void handleMessage(String message) {
//
//                System.out.println("--------handleMessage from client-------------");
//            }
//        });
//
//        while (true) {
//
//            clientEndPoint.sendMessage("Hi There!!");
//            Thread.sleep(30000);
//        }
//    }



}
