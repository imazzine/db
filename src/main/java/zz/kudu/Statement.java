package zz.kudu;

import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.client.CreateTableOptions;
import zz.common.Digestable;
import zz.kudu.sql.SelectResponse;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Statement implements Digestable {

    private Type type;
    private List<Source> sources;
    private Options options;

    private MessageDigest md = null;

    public enum Type {
        CREATE_TABLE,
        DROP_TABLE,
        INSERT_INTO_TABLE,
        SELECT,
        DELETE,
        UPDATE,
        SHOW_TABLES,
        SHOW_COLUMNS
    }

    public Statement() {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public Statement(Type type, List<Source> sources, Options options) {
        this.type = type;
        this.sources = sources;
        this.options = options;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public Statement(Type type, String tableName, Options options) {
        this(type, Collections.singletonList(new Source(tableName)), options);
    }

    @Override
    public void updateDigest(MessageDigest md) {
        Digestable.updateDigestForNullableStrings(md, type.name());
        Digestable.updateDigestForCollectionMember(md, sources);
        options.updateDigest(md); // Recurse
    }

    public MessageDigest getMd() {
        updateDigest(md);
        return md;
    }

    public List<Source> getSources() {
        return sources;
    }

    public Options getOptions() {
        return options;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    public void setOptions(Options options) {
        this.options = options;
    }


    public abstract static class Options implements Digestable {

        public static class ShowColumns extends Options {

            private String table;
            private String filter;
            private Predicate predicate;

            public ShowColumns(String table) {
                this.table = table;
            }

            public String getTable() {
                return table;
            }

            public String getFilter() {
                return filter;
            }

            public void setFilter(String filter) {
                this.filter = filter;
            }

            public Predicate getPredicate() {
                return predicate;
            }

            public void setPredicate(Predicate predicate) {
                this.predicate = predicate;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(md, table, filter);
                Digestable.updateDigestForNullableDigestable(md, predicate);
            }
        }

        public static class ShowTables extends Options {

            private String filter;
            private Predicate predicate;

            public String getFilter() {
                return filter;
            }

            public void setFilter(String filter) {
                this.filter = filter;
            }

            public Predicate getPredicate() {
                return predicate;
            }

            public void setPredicate(Predicate predicate) {
                this.predicate = predicate;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(md, filter);
                Digestable.updateDigestForNullableDigestable(md, predicate);
            }
        }

        public static class CreateTable extends Options {

            private Schema schema;
            private CreateTableOptions createTableOptions;

            public CreateTable(Schema schema, CreateTableOptions createTableOptions) {
                this.schema = schema;
                this.createTableOptions = createTableOptions;
            }

            public Schema getSchema() {
                return schema;
            }

            public CreateTableOptions getCreateTableOptions() {
                return createTableOptions;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                schema.getColumns().stream()
                        .sorted(Comparator.comparing(ColumnSchema::getName))
                        .forEachOrdered(
                                columnSchema -> {
                                    byte[] columnNameBytes = columnSchema.getName().getBytes(StandardCharsets.UTF_8);
                                    byte[] columnTypeBytes = columnSchema.getType()
                                            .getName().getBytes(StandardCharsets.UTF_8);
                                    byte[] defaultValueBytes = null;
                                    if (columnSchema.getDefaultValue() != null) {

                                        defaultValueBytes = columnSchema.getDefaultValue()
                                                .toString().getBytes(StandardCharsets.UTF_8);
                                    }
                                    ByteBuffer buff = ByteBuffer.allocate(Integer.BYTES // For column name string's length
                                            + columnNameBytes.length
                                            + Integer.BYTES // For column type name string's length
                                            + columnTypeBytes.length
                                            + Byte.BYTES // For isKey
                                            + Byte.BYTES // For isNullable
                                            + (defaultValueBytes == null ? Byte.BYTES : Integer.BYTES) // For default value string's length
                                            + (defaultValueBytes == null ? 0 : defaultValueBytes.length)
                                    );
                                    buff.putInt(columnNameBytes.length);
                                    buff.put(columnNameBytes);
                                    buff.putInt(columnTypeBytes.length);
                                    buff.put(columnTypeBytes);
                                    buff.put(
                                            columnSchema.isKey() ? (byte) 1 : (byte) 0
                                    );
                                    buff.put(
                                            columnSchema.isNullable() ? (byte) 1 : (byte) 0
                                    );
                                    if (defaultValueBytes == null) {
                                        buff.put((byte) 0);
                                    } else {
                                        buff.putInt(defaultValueBytes.length);
                                        buff.put(defaultValueBytes);
                                    }

                                    /* TODO: Is it necessary to use createTableOptions? */

                                    buff.flip();
                                    md.update(buff);
                                }
                        );
            }
        }

        public static class DropTable extends Options { // Empty options.
            @Override
            public void updateDigest(MessageDigest md) { // do nothing
            }
        }

        public static class Delete extends Options {

            private Predicate predicate;

            public Delete() {
                this.predicate = null;
            }

            public Delete(Predicate predicate) {
                this.predicate = predicate;
            }

            public Predicate getPredicate() {
                return predicate;
            }

            public void setPredicate(Predicate predicate) {
                this.predicate = predicate;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableDigestable(md, predicate);
            }
        }

        public static class Update extends Options {

            private Predicate predicate;
            private Map<String, Expression> updateMap;

            public Update() {
                this.predicate = null;
                this.updateMap = new HashMap<>();
            }

            public Update(Predicate predicate, Map<String, Expression> updateMap) {
                this.predicate = predicate;
                this.updateMap = updateMap;
            }

            public Predicate getPredicate() {
                return predicate;
            }

            public void setPredicate(Predicate predicate) {
                this.predicate = predicate;
            }

            public Map<String, Expression> getUpdateMap() {
                return updateMap;
            }

            public void setUpdateMap(Map<String, Expression> updateMap) {
                this.updateMap = updateMap;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                ByteBuffer buff;
                if (updateMap.size() < 1) {
                    buff = ByteBuffer.allocate(Byte.BYTES);
                    buff.put((byte) 0);
                    buff.flip();
                    md.update(buff);
                } else {
                    Map<String, Expression> sortedMap = new TreeMap<>(updateMap); // By column name
                    for (Map.Entry<String, Expression> entry : sortedMap.entrySet()) {
                        Digestable.updateDigestForNullableStrings(md, entry.getKey());
                        entry.getValue().updateDigest(md); // Expression
                    }
                }
                Digestable.updateDigestForNullableDigestable(md, predicate);
            }
        }

        public static class InsertIntoTable extends Options {

            private List<String> columns; // optional
            private Statement selectStatement; // required

            public InsertIntoTable(List<String> columns, Statement selectStatement) {
                this.columns = columns;
                this.selectStatement = selectStatement;
            }

            public List<String> getColumns() {
                return columns;
            }

            public Statement getSelectStatement() {
                return selectStatement;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                if (columns == null || columns.size() < 1) {
                    ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES);
                    buff.put((byte) 0);
                    buff.flip();
                    md.update(buff);
                } else {
                    Digestable.updateDigestForNullableStrings(md, columns.toArray(new String[0]));
                }
                selectStatement.updateDigest(md);
            }
        }

        public static class Select extends Options {

            private SelectResponse.OutputType outputType;
            private Projection projection;
            private Predicate predicate;
            private List<Expression> groupByList;
            private Map<Expression, Integer> orderByMap;
            private long limit = -1;
            private long offset = -1;

            public Select() {
            }

            public Select(
                    SelectResponse.OutputType outputType,
                    Projection projection,
                    Predicate predicate,
                    List<Expression> groupByList,
                    Map<Expression, Integer> orderByMap,
                    long limit,
                    long offset) {

                this.outputType = outputType;
                this.projection = projection;
                this.predicate = predicate;
                this.groupByList = groupByList;
                this.orderByMap = orderByMap;
                this.limit = limit;
                this.offset = offset;
            }

            public void setLimit(long limit) {
                this.limit = limit;
            }

            public void setOffset(long offset) {
                this.offset = offset;
            }

            public long getLimit() {
                return limit;
            }

            public long getOffset() {
                return offset;
            }

            public SelectResponse.OutputType getOutputType() {
                return outputType;
            }

            public Projection getProjection() {
                return projection;
            }

            public Predicate getPredicate() {
                return predicate;
            }

            public List<Expression> getGroupByList() {
                return groupByList;
            }

            public Map<Expression, Integer> getOrderByMap() {
                return orderByMap;
            }

            public void setOutputType(SelectResponse.OutputType outputType) {
                this.outputType = outputType;
            }

            public void setProjection(Projection projection) {
                this.projection = projection;
            }

            public void setPredicate(Predicate predicate) {
                this.predicate = predicate;
            }

            public void setGroupByList(List<Expression> groupByList) {
                this.groupByList = groupByList;
            }

            public void setOrderByMap(Map<Expression, Integer> orderByMap) {
                this.orderByMap = orderByMap;
            }

            @Override
            public void updateDigest(MessageDigest md) {
                Digestable.updateDigestForNullableStrings(md, outputType.name());
                projection.updateDigest(md);
                Digestable.updateDigestForNullableDigestable(md, predicate);
                List<Expression> groupByColumnList = new ArrayList<>();
                if (groupByList != null && groupByList.size() > 0) {
                    for (Expression expression : groupByList) {
                        int index = projection.indexOf(expression); /* TODO: check */
                        if (index >= 0) {
                            groupByColumnList.add((Expression) projection.get(index));
                        } else {
                            /*TODO: Maybe we should make method throwable?*/
                            new Exception("Group by Expression was not found for Digest")
                                    .printStackTrace();
                        }
                    }
                }
                Digestable.updateDigestForCollectionMember(md, groupByColumnList);
                if (orderByMap == null || orderByMap.size() < 1) {
                    ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES);
                    buff.put((byte) 0);
                    buff.flip();
                    md.update(buff);
                } else {
                    for (Map.Entry<Expression, Integer> entry : orderByMap.entrySet()) {
                        int index = projection.indexOf(entry.getKey()); /* TODO: check */
                        if (index >= 0) {
                            projection.get(index).updateDigest(md);
                            ByteBuffer buff = ByteBuffer.allocate(Integer.BYTES);
                            buff.putInt(entry.getValue());
                            buff.flip();
                            md.update(buff);
                        } else {
                            /*TODO: Maybe we should make method throwable?*/
                            new Exception("Order by Expression was not found for Digest")
                                    .printStackTrace();
                        }
                    }
                }
                ByteBuffer buff = ByteBuffer.allocate(Long.BYTES + Long.BYTES);
                buff.putLong(limit);
                buff.putLong(offset);
                buff.flip();
                md.update(buff);
            }
        }
    }
}
