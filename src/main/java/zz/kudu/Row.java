package zz.kudu;

import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.RowResult;
import zz.common.Digestable;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Comparator;

public class Row {

    private final int rowNumber;
    private Comparable[] fieldValues;

    public Row(int rowNumber, Comparable[] fieldValues) {
        this.rowNumber = rowNumber;
        this.fieldValues = fieldValues;
    }

    Row(RowResult rowResult, int rowNumber) {
        this.rowNumber = rowNumber;
        Schema schema = rowResult.getSchema();
        this.fieldValues = new Comparable[schema.getColumnCount()];
        for (int i = 0; i < this.fieldValues.length; i++) {
            if (rowResult.isNull(i)) {
                fieldValues[i] = null;
            } else {
                Type fieldType = schema.getColumnByIndex(i).getType();
                switch (fieldType) {
                    case DOUBLE:
                        fieldValues[i] = rowResult.getDouble(i);
                        break;
                    case BOOL:
//                        fieldValues[i] = rowResult.getBoolean(i)? Byte.valueOf((byte) 1):Byte.valueOf((byte) 0);
                        fieldValues[i] = rowResult.getBoolean(i);
                        break;
                    case UNIXTIME_MICROS:
                    case INT64:
                        fieldValues[i] = rowResult.getLong(i);
                        break;
                    case INT32:
                        fieldValues[i] = rowResult.getInt(i);
                        break;
                    case INT16:
                        fieldValues[i] = rowResult.getShort(i);
                        break;
                    case INT8:
                        fieldValues[i] = rowResult.getByte(i);
                        break;
                    case FLOAT:
                        fieldValues[i] = rowResult.getFloat(i);
                        break;
                    case STRING:
                        fieldValues[i] = rowResult.getString(i);
                        break;
                    case BINARY:
                        fieldValues[i] = rowResult.getBinary(i);
                        break;
                    default:
                        fieldValues[i] = null;
                }
            }
        }
    }

    public void updateDigestUsingKuduTypes(MessageDigest md, Type[] kuduTypes) {
        ByteBuffer buff = ByteBuffer.allocate(Integer.BYTES);
        buff.putInt(rowNumber);
        buff.flip();
        md.update(buff);
        for (int i = 0; i < fieldValues.length; i++) {
            if (fieldValues[i] == null) {
                buff = ByteBuffer.allocate(Byte.BYTES);
                buff.put((byte) 0);
                buff.flip();
                md.update(buff);
            } else {
                switch (kuduTypes[i]) {
                    case DOUBLE:
                        buff = ByteBuffer.allocate(Double.BYTES);
                        buff.putDouble(((Number) fieldValues[i]).doubleValue());
                        buff.flip();
                        md.update(buff);
                        break;
                    case BOOL:
                        buff = ByteBuffer.allocate(Byte.BYTES);
                        boolean boolVal = ((Boolean) fieldValues[i]).booleanValue();
                        buff.put(boolVal ? (byte) 1 : (byte) 0);
                        buff.flip();
                        md.update(buff);
                        break;
                    case UNIXTIME_MICROS:
                    case INT64:
                        buff = ByteBuffer.allocate(Long.BYTES);
                        buff.putLong(((Number) fieldValues[i]).longValue());
                        buff.flip();
                        md.update(buff);
                        break;
                    case INT32:
                        buff = ByteBuffer.allocate(Integer.BYTES);
                        buff.putInt(((Number) fieldValues[i]).intValue());
                        buff.flip();
                        md.update(buff);
                        break;
                    case INT16:
                        buff = ByteBuffer.allocate(Short.BYTES);
                        buff.putShort(((Number) fieldValues[i]).shortValue());
                        buff.flip();
                        md.update(buff);
                        break;
                    case INT8:
                        buff = ByteBuffer.allocate(Byte.BYTES);
                        buff.put(((Number) fieldValues[i]).byteValue());
                        buff.flip();
                        md.update(buff);
                        break;
                    case FLOAT:
                        buff = ByteBuffer.allocate(Float.BYTES);
                        buff.putFloat(((Number) fieldValues[i]).floatValue());
                        buff.flip();
                        md.update(buff);
                        break;
                    case STRING:
                        Digestable.updateDigestForNullableStrings(md, (String) fieldValues[i]);
                        break;
                    case BINARY:
                        buff = (ByteBuffer) fieldValues[i];
                        buff.flip();
                        md.update(buff);
                        break;
                    default:
                        String strValue = fieldValues[i].toString();
                        Digestable.updateDigestForNullableStrings(md, strValue);
                }
            }
        }
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public Comparable[] getFieldValues() {
        return fieldValues;
    }

    public Comparable getFieldValue(int columnIndex) {
        return fieldValues[columnIndex];
    }

    public static Comparable getFieldValue(Row leftRow, Row rightRow, int columnIndex) {
        if (columnIndex > leftRow.fieldValues.length - 1) {
            return rightRow.getFieldValue(columnIndex - leftRow.fieldValues.length);
        }
        return leftRow.getFieldValue(columnIndex);
    }

    private static class RowComparator implements Comparator<Row> {

        private int[] sortOrder;
        private int[] orderDirection;

        private RowComparator(int[] sortOrder, int[] orderDirection) {
            this.sortOrder = sortOrder;
            this.orderDirection = orderDirection;
        }

        @Override
        public int compare(Row firstRow, Row secondRow) {
            if (sortOrder != null) {
                for (int i = 0; i < sortOrder.length; i++) {
                    Comparable firstValue = firstRow.getFieldValue(sortOrder[i]);
                    Comparable secondValue = secondRow.getFieldValue(sortOrder[i]);
                    if (firstValue == null && secondValue != null) {
                        return orderDirection[i];
                    } else if (firstValue != null && secondValue == null) {
                        return -1 * orderDirection[i];
                    } else if (firstValue != null) {
                        int compare = firstValue.compareTo(secondValue);
                        if (compare != 0) {
                            return compare * orderDirection[i];
                        }
                    }
                }
            }
            return firstRow.getRowNumber() - secondRow.getRowNumber();
        }
    }

    public static Comparator<Row> getComparator(int[] sortOrder, int[] orderDirection) {
        return new RowComparator(sortOrder, orderDirection);
    }
}
