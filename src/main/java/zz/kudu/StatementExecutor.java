package zz.kudu;

import com.google.protobuf.ByteString;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.*;
import sphere.kudu.protobuf.db.DbBody;
import sphere.kudu.protobuf.db.DbDataset;
import sphere.kudu.protobuf.db.DbHeader;
import sphere.kudu.protobuf.db.DbSimplevalue;
import sphere.kudu.protobuf.db.enums.DbEnum;
import zz.kudu.sql.KuduSQLLexer;
import zz.kudu.sql.KuduSQLParser;
import zz.kudu.sql.SelectResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class StatementExecutor {

    public static void main(String[] args) {

        String kuduMasterUrl = "localhost";
        if (args.length > 0) {
            kuduMasterUrl = args[0];
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[32 * 1024];
        KuduClient client = new KuduClient.KuduClientBuilder(kuduMasterUrl).build();
        try {
            int bytesRead;
            while ((bytesRead = System.in.read(buffer)) > 0) {
                baos.write(buffer, 0, bytesRead);
                byte[] bytes = baos.toByteArray();
                Thread queryTask = new Thread(new IOQuery(client, bytes));
                queryTask.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static KuduResponse exec(KuduClient client, Statement statement) {
        KuduResponse response = new KuduResponse(statement.getType(), "", null);
        String tableName;
        Predicate predicate;
//        SelectResponse.SelectResponsePB responsePB;
        DbDataset.Dataset responsePB;
        String filter;
        try {
            switch (statement.getType()) {

                case CREATE_TABLE:
                    Statement.Options.CreateTable createTableStatementOptions =
                            (Statement.Options.CreateTable) statement.getOptions();
                    Schema schema = createTableStatementOptions.getSchema();
                    CreateTableOptions createTableOptions = createTableStatementOptions.getCreateTableOptions();
                    tableName = ((Source.Item.Table) statement.getSources().get(0).getItem()).getTableName();
                    KuduTable table = createTable(client, tableName, schema, createTableOptions);
                    response.data = table;
                    break;
                case DROP_TABLE:
                    tableName = ((Source.Item.Table) statement.getSources().get(0).getItem()).getTableName();
                    DeleteTableResponse deleteTableResponse = dropTable(client, tableName);
                    response.data = deleteTableResponse;
                    break;
                case INSERT_INTO_TABLE:
                    Statement.Options.InsertIntoTable insertIntoTableStatementOptions =
                            (Statement.Options.InsertIntoTable) statement.getOptions();
                    List columns = insertIntoTableStatementOptions.getColumns();
//                    List values = insertIntoTableStatementOptions.getValues();
                    View view = new View(insertIntoTableStatementOptions.getSelectStatement());
                    tableName = ((Source.Item.Table) statement.getSources().get(0).getItem()).getTableName();
                    List<OperationResponse> insertResponses = insert(client, tableName, columns, view);
                    response.data = insertResponses;
                    for (OperationResponse resp : insertResponses) {
                        if (resp.hasRowError()) {
                            RowError rowError = resp.getRowError();
                            response.error += rowError.toString();
                        }
                    }
                    break;
                case SELECT:
                    responsePB = select(client, statement);
                    response.data = responsePB;
                    break;
                case DELETE:
                    Statement.Options.Delete deleteOptions =
                            (Statement.Options.Delete) statement.getOptions();
                    predicate = deleteOptions.getPredicate();
                    tableName = ((Source.Item.Table) statement.getSources().get(0).getItem()).getTableName();
                    List<OperationResponse> deleteResponses =
                            delete(client, tableName, predicate);
                    response.data = deleteResponses;
                    for (OperationResponse resp : deleteResponses) {
                        if (resp.hasRowError()) {
                            RowError rowError = resp.getRowError();
                            response.error += rowError.toString();
                        }
                    }
                    break;
                case UPDATE:
                    Statement.Options.Update updateOptions =
                            (Statement.Options.Update) statement.getOptions();
                    predicate = updateOptions.getPredicate();
                    Map<String, Expression> updateMap = updateOptions.getUpdateMap();
                    tableName = ((Source.Item.Table) statement.getSources().get(0).getItem()).getTableName();
                    List<OperationResponse> updateResponses =
                            update(client, tableName, updateMap, predicate);
                    response.data = updateResponses;
                    for (OperationResponse resp : updateResponses) {
                        if (resp.hasRowError()) {
                            RowError rowError = resp.getRowError();
                            response.error += rowError.toString();
                        }
                    }
                    break;
                case SHOW_TABLES:
                    Statement.Options.ShowTables showTablesOptions =
                            (Statement.Options.ShowTables) statement.getOptions();
                    filter = showTablesOptions.getFilter();
                    predicate = showTablesOptions.getPredicate();
                    responsePB = showTables(client, filter, predicate);
                    response.data = responsePB;
                    break;
                case SHOW_COLUMNS:
                    Statement.Options.ShowColumns showColumnsOptions =
                            (Statement.Options.ShowColumns) statement.getOptions();
                    tableName = showColumnsOptions.getTable();
                    filter = showColumnsOptions.getFilter();
                    predicate = showColumnsOptions.getPredicate();
                    responsePB = showColumns(client, tableName, filter, predicate);
                    response.data = responsePB;
                    break;
            }
        } catch (Exception e) {
            response.error += e.toString();
            e.printStackTrace();
        }

        return response;
    }

    public static KuduResponse exec(KuduClient client, List<Statement> statements) {
        KuduResponse response = new KuduResponse(null, "", new HashMap<Integer, KuduResponse>());
        try {
            int index = 0;
            for (Statement statement : statements) {
                KuduResponse eachResponse = exec(client, statement);
                if (!"".equals(eachResponse.error)) {
                    response.error += ("(" + index + "):" + eachResponse.error + ";");
                }
                ((Map<Integer, KuduResponse>) response.data).put(index, eachResponse);
                index++;
            }
        } catch (Exception e) {
            response.error += e.toString();
            e.printStackTrace();
        }

        return response;
    }

    public static KuduResponse exec(KuduClient client, String statementsScript) {
        KuduResponse response = new KuduResponse(null, "", new HashMap<Integer, KuduResponse>());
        try {
            KuduSQLLexer lexer = new KuduSQLLexer(CharStreams.fromString(statementsScript));
            lexer.removeErrorListeners();
            lexer.addErrorListener(zz.common.ANTLRErrorListener.getInstance());
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            KuduSQLParser parser = new KuduSQLParser(tokens);
            parser.removeErrorListeners();
            parser.addErrorListener(zz.common.ANTLRErrorListener.getInstance());
            List<Statement> statements = parser.getStatements();
            response = exec(client, statements);
        } catch (Exception e) {
            response.error += e.toString();
        }

        return response;
    }

    //    public static SelectResponse.SelectResponsePB showColumns(KuduClient client,
//                                                              String table) throws Exception {
    public static DbDataset.Dataset showColumns(KuduClient client,
                                                String table) throws Exception {
        return showColumns(client, table, null, null);
    }

    //    public static SelectResponse.SelectResponsePB showColumns(KuduClient client,
//                                                              String table,
//                                                              String columnNameFilter,
//                                                              Predicate predicate) throws Exception {
    public static DbDataset.Dataset showColumns(KuduClient client,
                                                String table,
                                                String columnNameFilter,
                                                Predicate predicate) throws Exception {
        Type[] columnTypes = {
                Type.STRING,
                Type.STRING,
                Type.STRING,
                Type.BOOL,
                Type.BOOL,
                Type.STRING,
                Type.STRING,
                Type.STRING,
                Type.INT32
        };
        String[] columnAliases = {
                "name",
                "type",
                "comment",
                "primary_key",
                "nullable",
                "default_value",
                "encoding",
                "compression",
                "block_size"
        };
        Source.Item.Values valuesSourceItem = new Source.Item.Values(columnTypes, columnAliases);
        KuduTable kuduTable = client.openTable(table);
        Schema schema = kuduTable.getSchema();
        List<ColumnSchema> columnSchemas = null;
        if (columnNameFilter == null) {
            columnSchemas = schema.getColumns();
        } else {
            try {
                columnSchemas = Collections.singletonList(schema.getColumn(columnNameFilter));
            } catch (Exception e) {
                columnSchemas = Collections.EMPTY_LIST;
            }
        }
        for (ColumnSchema columnSchema : columnSchemas) {
            valuesSourceItem.add(columnSchema.getName());
            valuesSourceItem.add(columnSchema.getType().getName());
            String comment = null; /* TODO: is comment presented? */
            valuesSourceItem.add(comment);
            valuesSourceItem.add(columnSchema.isKey());
            valuesSourceItem.add(columnSchema.isNullable());
            valuesSourceItem.add(columnSchema.getDefaultValue() == null ? null : columnSchema.getDefaultValue().toString());
            valuesSourceItem.add(columnSchema.getEncoding().name());
            valuesSourceItem.add(columnSchema.getCompressionAlgorithm().name());
            valuesSourceItem.add(columnSchema.getDesiredBlockSize());
        }

        SelectResponse.OutputType outputType = SelectResponse.OutputType.ROWS;
        Projection projection = new Projection();
        projection.add(new Projection.Element.Asterisk());
        List<Source> sources = new ArrayList<>();
        Source source = new Source(valuesSourceItem);
        sources.add(source);
        List<Expression> groupByElements = null;
        Map<Expression, Integer> orderByMap = null;
        long limit = -1L;
        long offset = 0L;
        View view = new View(
                outputType,
                projection,
                sources,
                predicate,
                groupByElements,
                orderByMap,
                limit,
                offset
        );

        return select(client, view);
    }

    //    public static SelectResponse.SelectResponsePB showTables(KuduClient client) throws Exception {
    public static DbDataset.Dataset showTables(KuduClient client) throws Exception {
        return showTables(client, null, null);
    }

    //    public static SelectResponse.SelectResponsePB showTables(KuduClient client,
//                                                             String nameFilter,
//                                                             Predicate predicate) throws Exception {
    public static DbDataset.Dataset showTables(KuduClient client,
                                               String nameFilter,
                                               Predicate predicate) throws Exception {
        Type[] columnTypes = {Type.STRING};
        String[] columnAliases = {"name"};
        Source.Item.Values valuesSourceItem = new Source.Item.Values(columnTypes, columnAliases);
        for (String table : client.getTablesList(nameFilter).getTablesList()) {
            valuesSourceItem.add(table);
        }

        SelectResponse.OutputType outputType = SelectResponse.OutputType.ROWS;
        Projection projection = new Projection();
        projection.add(new Projection.Element.Asterisk());
        List<Source> sources = new ArrayList<>();
        Source source = new Source(valuesSourceItem);
        sources.add(source);
        List<Expression> groupByElements = null;
        Map<Expression, Integer> orderByMap = null;
        long limit = -1L;
        long offset = 0L;
        View view = new View(
                outputType,
                projection,
                sources,
                predicate,
                groupByElements,
                orderByMap,
                limit,
                offset
        );

        return select(client, view);
    }

    public static DbDataset.Dataset select(KuduClient client,
                                           View view) throws Exception {
        view.open(client);
        view.addRows(); // Query to kudu and build ByteStrings

        Number mins[] = view.getMins();
        Number maxs[] = view.getMaxs();
//        SelectResponse.AnySimpleValuePB[] minsPB =
//                new SelectResponse.AnySimpleValuePB[view.getColumnCount()];
//        SelectResponse.AnySimpleValuePB[] maxsPB =
//                new SelectResponse.AnySimpleValuePB[view.getColumnCount()];
        DbSimplevalue.SimpleValue[] minsPB =
                new DbSimplevalue.SimpleValue[view.getColumnCount()];
        DbSimplevalue.SimpleValue[] maxsPB =
                new DbSimplevalue.SimpleValue[view.getColumnCount()];
        DbSimplevalue.SimpleValue[] nullsPB =
                new DbSimplevalue.SimpleValue[view.getColumnCount()];

//        SelectResponse.AnySimpleValuePB.Builder minMaxValueBuilder = SelectResponse.AnySimpleValuePB.newBuilder();
        DbSimplevalue.SimpleValue.Builder minMaxValueBuilder = DbSimplevalue.SimpleValue.newBuilder();

        for (int i = 0; i < view.getColumnCount(); i++) {

            Expression Expression = view.getProjectColumns().get(i);
            switch (Expression.getKuduType()) {
                case BOOL:
                    nullsPB[i] = minMaxValueBuilder.setBoolValue(Byte.MIN_VALUE).build();
                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        minsPB[i] = minMaxValueBuilder.setBoolValue(Byte.MIN_VALUE).build();
                    } else {
//                        minsPB[i] = minMaxValueBuilder.setBoolValue(mins[i].byteValue() == (byte) 1).build();
                        minsPB[i] = minMaxValueBuilder.setBoolValue(mins[i].byteValue() == (byte) 1 ? 1 : 0).build();
                    }
                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        maxsPB[i] = minMaxValueBuilder.setBoolValue(Byte.MIN_VALUE).build();
                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setBoolValue(maxs[i].byteValue() == (byte) 1).build();
                        maxsPB[i] = minMaxValueBuilder.setBoolValue(mins[i].byteValue() == (byte) 1 ? 1 : 0).build();
                    }
                    break;
                case INT8:
                    nullsPB[i] = minMaxValueBuilder.setBoolValue(Byte.MIN_VALUE).build();
                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        minsPB[i] = minMaxValueBuilder.setInt8Value(Byte.MIN_VALUE).build();
                    } else {
//                        minsPB[i] = minMaxValueBuilder.setInt32Value(mins[i].intValue()).build();
                        minsPB[i] = minMaxValueBuilder.setInt8Value(mins[i].byteValue()).build();
                    }
                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        maxsPB[i] = minMaxValueBuilder.setInt8Value(Byte.MIN_VALUE).build();
                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setInt32Value(maxs[i].intValue()).build();
                        maxsPB[i] = minMaxValueBuilder.setInt8Value(mins[i].byteValue()).build();
                    }
                    break;
                case INT16:
                    nullsPB[i] = minMaxValueBuilder.setInt16Value(Short.MIN_VALUE).build();
                    if (mins[i] == null) {
                        minsPB[i] = minMaxValueBuilder.setInt16Value(Short.MIN_VALUE).build();
                    } else {
                        minsPB[i] = minMaxValueBuilder.setInt16Value(mins[i].shortValue()).build();
                    }
                    if (maxs[i] == null) {
                        maxsPB[i] = minMaxValueBuilder.setInt16Value(Short.MIN_VALUE).build();
                    } else {
                        maxsPB[i] = minMaxValueBuilder.setInt16Value(mins[i].shortValue()).build();
                    }
                    break;
                case INT32:
                case INT64: /* TODO: */
                case UNIXTIME_MICROS: /* TODO: */
                    nullsPB[i] = minMaxValueBuilder.setInt32Value(Integer.MIN_VALUE).build();
                    if (mins[i] == null) {
                        minsPB[i] = minMaxValueBuilder.setInt32Value(Integer.MIN_VALUE).build();
                    } else {
                        minsPB[i] = minMaxValueBuilder.setInt32Value(mins[i].intValue()).build();
                    }
                    if (maxs[i] == null) {
                        maxsPB[i] = minMaxValueBuilder.setInt32Value(Integer.MIN_VALUE).build();
                    } else {
                        maxsPB[i] = minMaxValueBuilder.setInt32Value(mins[i].intValue()).build();
                    }
                    break;
                case BINARY:
                case STRING:
                    nullsPB[i] = minMaxValueBuilder.setInt32Value(-1).build();
                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        minsPB[i] = minMaxValueBuilder.setInt32Value(-1).build();
                    } else {
                        minsPB[i] = minMaxValueBuilder.setInt32Value(mins[i].intValue()).build();
                    }
                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        maxsPB[i] = minMaxValueBuilder.setInt32Value(-1).build();
                    } else {
                        maxsPB[i] = minMaxValueBuilder.setInt32Value(maxs[i].intValue()).build();
                    }
                    break;
                case FLOAT:
                    nullsPB[i] = minMaxValueBuilder.setFloatValue(-Float.MAX_VALUE).build();
                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        minsPB[i] = minMaxValueBuilder.setFloatValue(-Float.MAX_VALUE).build();
                    } else {
                        minsPB[i] = minMaxValueBuilder.setFloatValue(mins[i].floatValue()).build();
                    }
                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        maxsPB[i] = minMaxValueBuilder.setFloatValue(-Float.MAX_VALUE).build();
                    } else {
                        maxsPB[i] = minMaxValueBuilder.setFloatValue(maxs[i].floatValue()).build();
                    }
                    break;
                case DOUBLE:
                    nullsPB[i] = minMaxValueBuilder.setDoubleValue(-Double.MAX_VALUE).build();
                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        minsPB[i] = minMaxValueBuilder.setDoubleValue(-Double.MAX_VALUE).build();
                    } else {
                        minsPB[i] = minMaxValueBuilder.setDoubleValue(mins[i].doubleValue()).build();
                    }
                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
                        maxsPB[i] = minMaxValueBuilder.setDoubleValue(-Double.MAX_VALUE).build();
                    } else {
                        maxsPB[i] = minMaxValueBuilder.setDoubleValue(maxs[i].doubleValue()).build();
                    }
                    break;
            }
        }

//        SelectResponse.HeaderPB.Builder headerBuilder = SelectResponse.HeaderPB.newBuilder();
        DbHeader.Header.Builder headerBuilder = DbHeader.Header.newBuilder();

        headerBuilder
//                .setOutputType(SelectResponse.OutputType.valueOf(view.getOutputType().name()))
                .setOutputType(DbEnum.OutputType.valueOf(view.getOutputType().name()))
//                .setRowCount(view.getRowCount())
                .setRowsCount((int) view.getRowCount())
//                .setColumnCount(view.getColumnCount())
                .setColumnsCount(view.getColumnCount())
//                .addAllColumnTypes(
                .addAllColumnsTypes(
                        view.getProjectColumns().stream()
                                .map(
//                                        expression -> zz.kudu.sql.Common.FieldType
                                        expression -> DbEnum.FieldType
                                                .forNumber(expression.getKuduType().getDataType().getNumber())
                                ).collect(toList())
                )
//                .addAllMins(Arrays.asList(minsPB))
                .addAllColumnsMins(Arrays.asList(minsPB))
//                .addAllMaxs(Arrays.asList(maxsPB))
                .addAllColumnsMaxs(Arrays.asList(maxsPB))
//                .addAllColumnNames(
                .addAllColumnsNames(
                        view.getProjectColumns().stream()
                                .map(Expression::toString)
                                .collect(toList())
                )
                .addAllColumnsNulls(Arrays.asList(nullsPB));

//        SelectResponse.BodyPB.Builder bodyBuilder = SelectResponse.BodyPB.newBuilder();
        DbBody.Body.Builder bodyBuilder = DbBody.Body.newBuilder();

        if (view.getOutputType() == SelectResponse.OutputType.COLUMNS) {
            ByteString data = ByteString.EMPTY;
            for (ByteString bs : view.getData()) {
                data = data.concat(bs);
            }
            bodyBuilder.setData(data);
        } else {
            bodyBuilder.setData(view.getData()[0]);
        }
        bodyBuilder.setIndirectData(view.getIndirectData());

//        SelectResponse.SelectResponsePB.Builder responseBuilder =
//                SelectResponse.SelectResponsePB.newBuilder();
        DbDataset.Dataset.Builder responseBuilder =
                DbDataset.Dataset.newBuilder();

        responseBuilder
                .setHeader(headerBuilder.build())
                .setBody(bodyBuilder.build());

        return responseBuilder.build();
    }

//    public static SelectResponse.SelectResponsePB select(KuduClient client,
//                                                         View view) throws Exception {
//        view.open(client);
//        view.addRows(); // Query to kudu and build ByteStrings
//
//        Number mins[] = view.getMins();
//        Number maxs[] = view.getMaxs();
//        SelectResponse.AnySimpleValuePB[] minsPB =
//                new SelectResponse.AnySimpleValuePB[view.getColumnCount()];
//        SelectResponse.AnySimpleValuePB[] maxsPB =
//                new SelectResponse.AnySimpleValuePB[view.getColumnCount()];
//        SelectResponse.AnySimpleValuePB.Builder minMaxValueBuilder = SelectResponse.AnySimpleValuePB.newBuilder();
//        for (int i = 0; i < view.getColumnCount(); i++) {
//
//            Expression Expression = view.getProjectColumns().get(i);
//            switch (Expression.getKuduType()) {
//                case BOOL:
//                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        minsPB[i] = minMaxValueBuilder.setBoolValue(mins[i].byteValue() == (byte) 1).build();
//                    }
//                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setBoolValue(maxs[i].byteValue() == (byte) 1).build();
//                    }
//                    break;
//                case INT8:
//                case INT16:
//                case INT32:
//                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        minsPB[i] = minMaxValueBuilder.setInt32Value(mins[i].intValue()).build();
//                    }
//                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setInt32Value(maxs[i].intValue()).build();
//                    }
//                    break;
//                case INT64:
//                case UNIXTIME_MICROS:
//                case BINARY:
//                case STRING:
//                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        minsPB[i] = minMaxValueBuilder.setInt64Value(mins[i].longValue()).build();
//                    }
//                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setInt64Value(maxs[i].longValue()).build();
//                    }
//                    break;
//                case FLOAT:
//                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        minsPB[i] = minMaxValueBuilder.setFloatValue(mins[i].floatValue()).build();
//                    }
//                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setFloatValue(maxs[i].floatValue()).build();
//                    }
//                    break;
//                case DOUBLE:
//                    if (mins[i] == null) {
//                        minsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        minsPB[i] = minMaxValueBuilder.setDoubleValue(mins[i].doubleValue()).build();
//                    }
//                    if (maxs[i] == null) {
//                        maxsPB[i] = minMaxValueBuilder.setNullValue(NullValue.NULL_VALUE).build();
//                    } else {
//                        maxsPB[i] = minMaxValueBuilder.setDoubleValue(maxs[i].doubleValue()).build();
//                    }
//                    break;
//            }
//        }
//
//        SelectResponse.HeaderPB.Builder headerBuilder = SelectResponse.HeaderPB.newBuilder();
//        headerBuilder
//                .setOutputType(SelectResponse.OutputType.valueOf(view.getOutputType().name()))
//                .setRowCount(view.getRowCount())
//                .setColumnCount(view.getColumnCount())
//                .addAllColumnTypes(
//                        view.getProjectColumns().stream()
//                                .map(
//                                        expression -> zz.kudu.sql.Common.FieldType
//                                                .forNumber(expression.getKuduType().getDataType().getNumber())
//                                ).collect(toList())
//                ).addAllMins(Arrays.asList(minsPB))
//                .addAllMaxs(Arrays.asList(maxsPB))
//                .addAllColumnNames(
//                        view.getProjectColumns().stream()
//                                .map(Expression::toString)
//                                .collect(toList())
//                );
//
//        SelectResponse.BodyPB.Builder bodyBuilder = SelectResponse.BodyPB.newBuilder();
//        if (view.getOutputType() == SelectResponse.OutputType.COLUMNS) {
//
//            ByteString data = ByteString.EMPTY;
//            for (ByteString bs : view.getData()) {
//
//                data = data.concat(bs);
//            }
//
//            bodyBuilder.setData(data);
//        } else {
//
//            bodyBuilder.setData(view.getData()[0]);
//        }
//        bodyBuilder.setIndirectData(view.getIndirectData());
//
//        SelectResponse.SelectResponsePB.Builder responseBuilder =
//                SelectResponse.SelectResponsePB.newBuilder();
//        responseBuilder
//                .setHeader(headerBuilder.build())
//                .setBody(bodyBuilder.build());
//
//        return responseBuilder.build();
//    }

    //    public static SelectResponse.SelectResponsePB select(KuduClient client,
//                                                         Statement statement) throws Exception {
    public static DbDataset.Dataset select(KuduClient client,
                                           Statement statement) throws Exception {
        return select(client, new View(statement));
    }

    public static KuduTable createTable(KuduClient client,
                                        String tableName,
                                        Schema schema,
                                        CreateTableOptions createTableOptionsBuilder) throws KuduException {

        return client.createTable(tableName, schema, createTableOptionsBuilder);
    }

    public static DeleteTableResponse dropTable(KuduClient client,
                                                String tableName) throws KuduException {
        return client.deleteTable(tableName);
    }

    public static List<OperationResponse> insert(KuduClient client,
                                                 String tableName,
                                                 List<String> columns,
                                                 View view) throws Exception {
        view.setIgnoreOrderBy(true);
        view.setSorted(false);
        view.open(client);
        return insert(client, tableName, columns, view.toList().stream());
    }

    public static List<OperationResponse> insert(KuduClient client,
                                                 String tableName,
                                                 List<String> columns,
                                                 Stream<Row> rows) throws Exception {

        KuduTable table = client.openTable(tableName);
        Schema schema = table.getSchema();
        KuduSession session = client.newSession();
        List<OperationResponse> response = new ArrayList<>();
        String[] errors = {""};

        if (columns != null && columns.size() > 0 && rows != null) {
            rows.parallel().forEach(row -> {
                Insert insert = table.newInsert();
                PartialRow partialRow = insert.getRow();
                int index = 0;
                for (String colName : columns) {
                    Type type = schema.getColumn(colName).getType();
                    setPartialRowColumn(partialRow, type, colName, row.getFieldValue(index));
                    index++;
                }
                try {
                    response.add(session.apply(insert));
                } catch (KuduException e) {
                    errors[0] += (e.toString() + "; ");
                }
            });
        } else if (rows != null) {
            rows.parallel().forEach(row -> {
                Insert insert = table.newInsert();
                PartialRow partialRow = insert.getRow();
                int index = 0;
                for (Comparable value : row.getFieldValues()) {
                    Type type = schema.getColumnByIndex(index).getType();
                    setPartialRowColumn(partialRow, type, index, value);
                    index++;
                }
                try {
                    response.add(session.apply(insert));
                } catch (KuduException e) {
                    errors[0] += (e.toString() + "; ");
                }
            });
        }

        if (!"".equals(errors[0])) {
            throw new Exception(errors[0]);
        }

        return response;
    }

    public static List<OperationResponse> delete(KuduClient client,
                                                 String tableName,
                                                 Predicate predicate) throws Exception {

        KuduTable kuduTable = client.openTable(tableName);
        List<String> primaryKeys = kuduTable.getSchema().getPrimaryKeyColumns().stream()
                .map(ColumnSchema::getName)
                .collect(toList());
        Projection projection = new Projection();
        for (String key : primaryKeys) {
            projection.add(new Expression(Expression.Type.COLUMN, key));
        }
        Source source = new Source(tableName);
        View view = new View(
                SelectResponse.OutputType.ROWS,
                projection,
                Collections.singletonList(source),
                predicate,
                null,
                null,
                -1L,
                0L
        );
        view.open(client);
        List<OperationResponse> response = new ArrayList<>();

        KuduSession session = client.newSession();
        String[] errors = {""};
        view.getStream().parallel().forEach(
                row -> {
                    Delete delete = kuduTable.newDelete();
                    PartialRow partialRow = delete.getRow();
                    for (Expression expression : view.getProjectColumns()) {
                        setPartialRowColumn(
                                partialRow,
                                expression.getKuduType(),
                                (String) expression.getElement(),
                                expression.getValue(row)
                        );
                    }

                    try {
                        response.add(session.apply(delete));
                    } catch (KuduException e) {
                        errors[0] += (e.toString() + "; ");
                    }
                }
        );

        if (!"".equals(errors[0])) {
            throw new Exception(errors[0]);
        }

        return response;
    }

    public static List<OperationResponse> update(KuduClient client,
                                                 String tableName,
                                                 Map<String, Expression> updateMap,
                                                 Predicate predicate) throws Exception {

        KuduTable kuduTable = client.openTable(tableName);
        Schema schema = kuduTable.getSchema();
        List<String> primaryKeys = schema.getPrimaryKeyColumns().stream()
                .map(ColumnSchema::getName)
                .collect(toList());
        Projection projection = new Projection();
        for (String key : primaryKeys) {
            projection.add(new Expression(Expression.Type.COLUMN, key));
        }
        for (Map.Entry<String, Expression> entry : updateMap.entrySet()) {
            String columnNameForUpdate = entry.getKey();
            if (primaryKeys.indexOf(columnNameForUpdate) < 0) {
                projection.add(new Expression(Expression.Type.COLUMN, columnNameForUpdate));
                projection.add(entry.getValue()); // Expression to update.
            }
        }
        Source source = new Source(tableName);
        View view = new View(
                SelectResponse.OutputType.ROWS,
                projection,
                Collections.singletonList(source),
                predicate,
                null,
                null,
                -1L,
                0L
        );
        view.open(client);
        List<OperationResponse> response = new ArrayList<>();

        KuduSession session = client.newSession();
        String[] errors = {""};
        view.getStream().parallel().forEach(
                row -> {
                    Update update = kuduTable.newUpdate();
                    PartialRow partialRow = update.getRow();
                    for (int i = 0; i < primaryKeys.size(); i++) {
                        Expression keyExpression = view.getProjectColumns().get(i);
                        setPartialRowColumn(
                                partialRow,
                                keyExpression.getKuduType(),
                                (String) keyExpression.getElement(),
                                keyExpression.getValue(row)
                        );
                    }
                    for (int i = primaryKeys.size(); i < view.getColumnCount(); i += 2) {
                        Expression fieldExpression = view.getProjectColumns().get(i);
                        Expression valueExpression = view.getProjectColumns().get(i + 1);
                        setPartialRowColumn(
                                partialRow,
                                fieldExpression.getKuduType(),
                                (String) fieldExpression.getElement(),
                                valueExpression.getValue(row)
                        );
                    }

                    try {
                        response.add(session.apply(update));
                    } catch (KuduException e) {
                        errors[0] += (e.toString() + "; ");
                    }
                }
        );

        if (!"".equals(errors[0])) {
            throw new Exception(errors[0]);
        }

        return response;
    }

    /**
     * Add data to the given row for the column at colName {@code colName}
     * of type {@code type}
     *
     * @param row The row to add the field to
     */
    public static void setPartialRowColumn(PartialRow row, Type type, String colName, Object value) {

        if (value == null) {
            setNull(row, colName);
        } else {
            switch (type) {
                case INT8:
                    addByte(row, colName, value);
                    return;
                case INT16:
                    addShort(row, colName, value);
                    return;
                case INT32:
                    addInt(row, colName, value);
                    return;
                case INT64:
//            case TIMESTAMP:
                    addLong(row, colName, value);
                    return;
                case BINARY:
                    addBinary(row, colName, value);
                    return;
                case STRING:
                    addString(row, colName, value);
                    return;
                case BOOL:
                    addBoolean(row, colName, value);
                    return;
                case FLOAT:
                    addFloat(row, colName, value);
                    return;
                case DOUBLE:
                    addDouble(row, colName, value);
                    return;
                default:
                    throw new UnsupportedOperationException("Unknown type " + type);
            }
        }
    }

    /**
     * Add data to the given row for the column at index {@code index}
     * of type {@code type}
     *
     * @param row The row to add the field to
     */
    public static void setPartialRowColumn(PartialRow row, Type type, int index, Object value) {

        if (value == null) {
            setNull(row, index);
        } else {
            switch (type) {
                case INT8:
                    addByte(row, index, value);
                    return;
                case INT16:
                    addShort(row, index, value);
                    return;
                case INT32:
                    addInt(row, index, value);
                    return;
                case INT64:
//            case TIMESTAMP:
                    addLong(row, index, value);
                    return;
                case BINARY:
                    addBinary(row, index, value);
                    return;
                case STRING:
                    addString(row, index, value);
                    return;
                case BOOL:
                    addBoolean(row, index, value);
                    return;
                case FLOAT:
                    addFloat(row, index, value);
                    return;
                case DOUBLE:
                    addDouble(row, index, value);
                    return;
                default:
                    throw new UnsupportedOperationException("Unknown type " + type);
            }
        }
    }

    public static void setNull(PartialRow row, int index) {
        row.setNull(index);
    }

    public static void setNull(PartialRow row, String colName) {
        row.setNull(colName);
    }

    public static void addDouble(PartialRow row, int index, Object value) {
        row.addDouble(index, ((Number) value).doubleValue());
    }

    public static void addDouble(PartialRow row, String colName, Object value) {
        row.addDouble(colName, ((Number) value).doubleValue());
    }

    public static void addFloat(PartialRow row, int index, Object value) {
        row.addFloat(index, ((Number) value).floatValue());
    }

    public static void addFloat(PartialRow row, String colName, Object value) {
        row.addFloat(colName, ((Number) value).floatValue());
    }

    public static void addBoolean(PartialRow row, int index, Object value) {
        row.addBoolean(index, (Boolean) value);
    }

    public static void addBoolean(PartialRow row, String colName, Object value) {
        row.addBoolean(colName, (Boolean) value);
    }

    public static void addString(PartialRow row, int index, Object value) {
        row.addString(index, value.toString());
    }

    public static void addString(PartialRow row, String colName, Object value) {
        row.addString(colName, value.toString());
    }

    public static void addBinary(PartialRow row, int index, Object value) {
        row.addBinary(index, (ByteBuffer) value);
    }

    public static void addBinary(PartialRow row, String colName, Object value) {
        row.addBinary(colName, (ByteBuffer) value);
    }

    public static void addLong(PartialRow row, int index, Object value) {
        row.addLong(index, ((Number) value).longValue());
    }

    public static void addLong(PartialRow row, String colName, Object value) {
        row.addLong(colName, ((Number) value).longValue());
    }

    public static void addInt(PartialRow row, int index, Object value) {
        row.addInt(index, ((Number) value).intValue());
    }

    public static void addInt(PartialRow row, String colName, Object value) {
        row.addInt(colName, ((Number) value).intValue());
    }

    public static void addShort(PartialRow row, int index, Object value) {
        row.addShort(index, ((Number) value).shortValue());
    }

    public static void addShort(PartialRow row, String colName, Object value) {
        row.addShort(colName, ((Number) value).shortValue());
    }

    public static void addByte(PartialRow row, int index, Object value) {
        row.addByte(index, (Byte) value);
    }

    public static void addByte(PartialRow row, String colName, Object value) {
        row.addByte(colName, (Byte) value);
    }
}
