package zz.net;

import javax.websocket.*;
import java.net.URI;

@ClientEndpoint
public class WSClient {

    private ClientEndpointConfig clientConfig;
    private String user;
    private Session session = null;

    public WSClient(URI serverURI) throws Exception {

        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        container.connectToServer(this, serverURI);
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig clientConfig){

        this.clientConfig = (ClientEndpointConfig) clientConfig;
        if(session.getUserPrincipal() != null) {

            this.user = session.getUserPrincipal().getName();
        }
        this.session = session;

        System.out.println("-----WSClient:-----User " + user + " connected to WSServer");
    }

    @OnMessage
    public void onMessage(String msg){

        System.out.println("-----WSClient:-----Message from WSServer: " + msg);
    }

    @OnClose
    public void onClose(Session session, CloseReason reason){

        this.session = null;

        System.out.println("-----WSClient:-----User "+ user + " disconnected as a result of: code = " + reason.getCloseCode() + "; ReasonPhrase = " + reason.getReasonPhrase());
    }

    @OnError
    public void onError(Session session, Throwable error){

//        System.out.println("Error communicating with server: " + error.getMessage());
        System.out.println("-----WSClient:-----Error communicating with server: " + error);
    }
}
