package zz.net;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class Starter implements ServletContextListener {

    private static String KUDU_MASTER_HOST = "localhost";
    private static String RABBIT_MQ_HOST = "localhost";
    private static String REGISTER_IN_QUEUE_NAME = "register_in";
    private static String REGISTER_OUT_EXCHANGE_NAME = "register_out";
    private static String SELECT_EXCHANGE_NAME = "select";
    private static String REFRESH_EXCHANGE_NAME = "refresh";
    private static String OUT_EXCHANGE_NAME = "out";
    private static String MANIPULATE_QUEUE_NAME = "manipulate";

    private Registrar registrar;

    @Override
    public void contextInitialized(ServletContextEvent event) {

        System.out.println("Sphere application is started");

        String kuduMasterHost = event.getServletContext().getInitParameter("kuduMasterHost");
        if (kuduMasterHost != null) {
            KUDU_MASTER_HOST = kuduMasterHost;
        }
        String rabbitMQHost = event.getServletContext().getInitParameter("rabbitMQHost");
        if (rabbitMQHost != null) {
            RABBIT_MQ_HOST = rabbitMQHost;
        }
        String registerInQueueName = event.getServletContext().getInitParameter("registerInQueueName");
        if (registerInQueueName != null) {
            REGISTER_IN_QUEUE_NAME = registerInQueueName;
        }
        String registerOutExchangeName = event.getServletContext().getInitParameter("registerOutExchangeName");
        if (registerOutExchangeName != null) {
            REGISTER_OUT_EXCHANGE_NAME = registerOutExchangeName;
        }
        String selectExchangeName = event.getServletContext().getInitParameter("selectExchangeName");
        if (selectExchangeName != null) {
            SELECT_EXCHANGE_NAME = selectExchangeName;
        }
        String refreshExchangeName = event.getServletContext().getInitParameter("refreshExchangeName");
        if (refreshExchangeName != null) {
            REFRESH_EXCHANGE_NAME = refreshExchangeName;
        }
        String outExchangeName = event.getServletContext().getInitParameter("outExchangeName");
        if (outExchangeName != null) {
            OUT_EXCHANGE_NAME = outExchangeName;
        }
        String manipulateQueueName = event.getServletContext().getInitParameter("manipulateQueueName");
        if (manipulateQueueName != null) {
            MANIPULATE_QUEUE_NAME = manipulateQueueName;
        }

        registrar = new Registrar(
                KUDU_MASTER_HOST,
                RABBIT_MQ_HOST,
                REGISTER_IN_QUEUE_NAME,
                REGISTER_OUT_EXCHANGE_NAME,
                SELECT_EXCHANGE_NAME,
                REFRESH_EXCHANGE_NAME,
                OUT_EXCHANGE_NAME,
                MANIPULATE_QUEUE_NAME
        );
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (registrar != null) {
            try {
                registrar.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Sphere application is destroyed");
    }
}
