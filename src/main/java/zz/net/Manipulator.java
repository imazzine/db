package zz.net;

import com.rabbitmq.client.*;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.kudu.client.KuduClient;
import zz.kudu.*;
import zz.kudu.sql.KuduSQLLexer;
import zz.kudu.sql.KuduSQLParser;
import zz.kudu.sql.SelectResponse;

import java.io.IOException;
import java.util.*;

public class Manipulator {

    private String kuduMasterURL;
    private Connection rabbitConnection;
    private String refreshExchangeName;
    private String manipulateQueueName;
    private Channel channelIn;
    private Channel channelOut;
    private Consumer consumerIn;

    private static final boolean DURABLE = true;
    private static final boolean NO_DURABLE = false;
    private static final boolean NO_EXCLUSIVE = false;
    private static final boolean NO_AUTODELETE = false;
    private static final Map<String, Object> NULL_ARGUMENTS = null;
    private static final int PREFETCH_COUNT = 1;
    private static final boolean NO_MULTIPLE = false;
    private static final boolean NO_AUTOACK = false;

    public Manipulator(
            String kuduMasterURL,
            Connection rabbitConnection,
            String refreshExchangeName,
            String manipulateQueueName) throws IOException {

        this.kuduMasterURL = kuduMasterURL;
        this.rabbitConnection = rabbitConnection;
        this.refreshExchangeName = refreshExchangeName;
        this.manipulateQueueName = manipulateQueueName;
        this.kuduMasterURL = kuduMasterURL;

        this.channelIn = this.rabbitConnection.createChannel();
        this.channelOut = this.rabbitConnection.createChannel();

        this.channelIn.queueDeclare(
                this.manipulateQueueName,
                DURABLE,
                NO_EXCLUSIVE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );
        channelIn.basicQos(PREFETCH_COUNT);

        channelOut.exchangeDeclare(
                this.refreshExchangeName,
                BuiltinExchangeType.DIRECT, /*TODO: topic?*/
                DURABLE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );

        this.consumerIn = new DefaultConsumer(channelIn) {
            @Override
            public void handleDelivery(String consumerTag,
                                       Envelope envelope,
                                       AMQP.BasicProperties properties,
                                       byte[] body) throws IOException {
                try {
                    String query = new String(body, "UTF-8");
                    System.out.println(" [Manipulator] Received query " + " '" + query + "'");
                    execAndRefresh(query, properties);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    System.out.println(" [Manipulator] Done");
                    channelIn.basicAck(envelope.getDeliveryTag(), NO_MULTIPLE);
                }
            }
        };

        this.channelIn.basicConsume(
                this.manipulateQueueName,
                NO_AUTOACK,
                this.consumerIn
        );

        System.out.println(" [Manipulator] Waiting for messages...");
    }

    private void execAndRefresh(String query, AMQP.BasicProperties properties) throws Exception {
        KuduClient client = new KuduClient.KuduClientBuilder(kuduMasterURL).build();
        try {
            KuduSQLLexer lexer = new KuduSQLLexer(CharStreams.fromString(query));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            KuduSQLParser parser = new KuduSQLParser(tokens);
            List<Statement> statements = parser.getStatements();

            KuduResponse response = StatementExecutor.exec(client, statements);
            HashMap<Integer, KuduResponse> responses = (HashMap<Integer, KuduResponse>) response.data;
            Set<String> tablesForRefresh = new HashSet<>();
            for (Map.Entry<Integer, KuduResponse> entry : responses.entrySet()) {
                KuduResponse kuduResponse = entry.getValue();
                if (kuduResponse.error == null || "".equals(kuduResponse.error)) {
                    Statement statement = statements.get(entry.getKey());
                    if (statement.getType() != Statement.Type.SELECT) {
                        for (Source source : statement.getSources()) {
                            tablesForRefresh.addAll(source.getTables());
                        }
                    }
                }
            }

            for (String topicForUpdate : tablesForRefresh) {
                channelOut.basicPublish(
                        refreshExchangeName,
                        topicForUpdate,
                        properties, /* TODO: does it simple proxies? */
                        topicForUpdate.getBytes("UTF-8")
                );
            }

            if (response.error != null && !"".equals(response.error)) {
                throw new Exception(response.error);
            }

        } finally {
            client.shutdown();
        }
    }
}
