package zz.net;

import com.rabbitmq.client.*;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import zz.common.Pool;
import zz.kudu.Statement;
import zz.kudu.sql.KuduSQLLexer;
import zz.kudu.sql.KuduSQLParser;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class Registrar {

    private String kuduMasterURL;
    private String rabbitMqHost;
    private String registerInQueueName;
    private String registerOutExchangeName;
    private String selectExchangeName;
    private String refreshExchangeName;
    private String outExchangeName;
    private String manipulateQueueName;

    private ConnectionFactory connectionFactory;
    private Connection rabbitConnection;
    private Channel channelIn;
    private Channel channelOut;
    private Consumer consumerIn;

    private Pool poolSelectors;

    private static final boolean DURABLE = true;
    private static final boolean NO_DURABLE = false;
    private static final boolean NO_EXCLUSIVE = false;
    private static final boolean NO_AUTODELETE = false;
    private static final Map<String, Object> NULL_ARGUMENTS = null;
    private static final int PREFETCH_COUNT = 1;
    private static final boolean NO_MULTIPLE = false;
    private static final boolean NO_AUTOACK = false;

    public Registrar(
            String kuduMasterURL,
            String rabbitMqHost,
            String registerInQueueName,
            String registerOutExchangeName,
            String selectExchangeName,
            String refreshExchangeName,
            String outExchangeName,
            String manipulateQueueName) {

        this.kuduMasterURL = kuduMasterURL;
        this.rabbitMqHost = rabbitMqHost;
        this.registerInQueueName = registerInQueueName;
        this.registerOutExchangeName = registerOutExchangeName;
        this.selectExchangeName = selectExchangeName;
        this.refreshExchangeName = refreshExchangeName;
        this.outExchangeName = outExchangeName;
        this.manipulateQueueName = manipulateQueueName;

        this.connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(this.rabbitMqHost);

        /*TODO: reconnect?*/
        try {
            connectAndInit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    private void connectAndInit() throws IOException, TimeoutException {

        rabbitConnection = connectionFactory.newConnection();
        channelIn = rabbitConnection.createChannel();
        channelOut = rabbitConnection.createChannel();

        channelIn.queueDeclare(
                registerInQueueName,
                NO_DURABLE,
                NO_EXCLUSIVE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );
        channelIn.basicQos(PREFETCH_COUNT);

        channelOut.exchangeDeclare(
                registerOutExchangeName,
                BuiltinExchangeType.DIRECT,
                DURABLE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );

        poolSelectors = Pool.getInstance(Pool.Name.SELECTOR);

        /*TODO: member?*/
        Manipulator manipulator = new Manipulator(
                kuduMasterURL,
                rabbitConnection,
                refreshExchangeName,
                manipulateQueueName
        );

        consumerIn = new DefaultConsumer(channelIn) {
            @Override
            public void handleDelivery(
                    String consumerTag,
                    Envelope envelope,
                    AMQP.BasicProperties properties,
                    byte[] body) throws IOException {

                try {

                    String query = new String(body, "UTF-8");
                    System.out.println(" [Registrator] Received for " + properties.getReplyTo() + " '" + query + "'");

                    KuduSQLLexer lexer = new KuduSQLLexer(CharStreams.fromString(query));
                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    KuduSQLParser parser = new KuduSQLParser(tokens);
                    List<Statement> statements = parser.getStatements();

                    if (statements.size() > 0) {
                        MessageDigest md = statements.get(0).getMd();
                        if (statements.size() > 1) {
                            for (int i = 1; i < statements.size(); i++) {
                                md.update(statements.get(i).getMd().digest());
                            }
                        }

                        byte[] key = md.digest();

                        // Convert to string to be able to use as a hash
                        BigInteger mediator = new BigInteger(1, key);
                        String hash = String.format("%040x", mediator);

                        if (statements.size() == 1
                                && statements.get(0).getType() == Statement.Type.SELECT) {
                            Channel checkChannel = null;
                            try {

                                checkChannel = rabbitConnection.createChannel();
                                AMQP.Queue.DeclareOk declareOk = checkChannel.queueDeclarePassive(hash);
                                System.out.println(" [Registrator] Selector for '" + hash + "' already exists.");
                            } catch (IOException e) {
                                Object borrowedSelector = poolSelectors.borrow();
                                if (borrowedSelector == null) {
                                    poolSelectors.addAndBorrow(
                                            new Selector(
                                                    kuduMasterURL,
                                                    rabbitConnection,
                                                    selectExchangeName,
                                                    refreshExchangeName,
                                                    outExchangeName,
                                                    hash,
                                                    statements.get(0)
                                            )
                                    );
                                } else {
                                    Selector selector = (Selector) borrowedSelector;
                                    selector.open(hash, statements.get(0));
                                }
                            } finally {
                                if (checkChannel != null && checkChannel.isOpen()){
                                    checkChannel.close();
                                }
                            }
                        }

                        String correlationId = properties.getCorrelationId();
                        AMQP.BasicProperties propsOut = new AMQP.BasicProperties
                                .Builder()
                                .correlationId(correlationId)
                                .build();
                        channelOut.basicPublish(
                                registerOutExchangeName,
                                properties.getReplyTo(),
                                propsOut,
                                hash.getBytes("UTF-8"));

                        System.out.println(
                                " [Registrator] Sent to '"
                                        + registerOutExchangeName
                                        + "', direct, replyTo '"
                                        + properties.getReplyTo()
                                        + "', correlationId '"
                                        + correlationId
                                        + "', hash '"
                                        + hash
                                        + "'"
                        );
                    } else {
                        throw new Exception("No statements was found by parser.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    System.out.println(" [Registrator] Done");
                    channelIn.basicAck(envelope.getDeliveryTag(), NO_MULTIPLE);
                }
            }
        };

        this.channelIn.basicConsume(
                this.registerInQueueName,
                NO_AUTOACK,
                this.consumerIn
        );

        System.out.println(" [Registrator] Waiting for messages...");
    }

    private boolean isRabbitConnected() {
        return rabbitConnection != null && rabbitConnection.isOpen();
    }

    public void close() throws IOException, TimeoutException {
        // Closing the channels will be done automatically anyway when the underlying connection is closed.
        if (isRabbitConnected()) {
            rabbitConnection.close();
        }
        poolSelectors.shutdown();
    }
}
