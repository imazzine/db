package zz.net;

import com.rabbitmq.client.*;
import org.apache.kudu.client.KuduClient;
import sphere.kudu.protobuf.db.DbDataset;
import zz.common.Pool;
import zz.kudu.*;
import zz.kudu.sql.SelectResponse;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public class Selector {

    private String kuduMasterURL;
    private Connection rabbitConnection;
    private String selectExchangeName;
    private String refreshExchangeName;
    private String outExchangeName;
    private String queueInName;
    private Statement statement;
    private View view;

    private Channel channelIn;
    private Channel channelOut;
    private Consumer consumerIn;

    private static final boolean DURABLE = true;
    private static final boolean NO_DURABLE = false;
    private static final boolean EXCLUSIVE = true;
    private static final boolean AUTODELETE = true;
    private static final boolean NO_AUTODELETE = false;
    private static final Map<String, Object> NULL_ARGUMENTS = null;
    private static final int PREFETCH_COUNT = 1;
    //    private static final boolean NO_AUTOACK = false;
    private static final boolean AUTOACK = true;
    private static final boolean NO_MULTIPLE = false;

    private static final String DESTROY_COMMAND = "DESTROY";

    public Selector(
            String kuduMasterURL,
            Connection rabbitConnection,
            String selectExchangeName,
            String refreshExchangeName,
            String outExchangeName,
            String queueInName,
            Statement statement) throws Exception {

        this.kuduMasterURL = kuduMasterURL;
        this.rabbitConnection = rabbitConnection;
        this.selectExchangeName = selectExchangeName;
        this.refreshExchangeName = refreshExchangeName;
        this.outExchangeName = outExchangeName;
        this.queueInName = queueInName;
        this.statement = statement;

        connectAndInit();
    }

    public void open(String queueInName, Statement statement) throws IOException {
        this.queueInName = queueInName;
        this.statement = statement;
        connectAndInit();
    }

    private void connectAndInit() throws IOException {
        this.channelOut = this.rabbitConnection.createChannel();
        this.channelOut.exchangeDeclare(
                this.outExchangeName,
                BuiltinExchangeType.DIRECT, /*TODO: topic?*/
                DURABLE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );

        this.channelIn = this.rabbitConnection.createChannel();
        this.channelIn.basicQos(PREFETCH_COUNT); /*TODO: does it need?*/

        this.channelIn.exchangeDeclare(
                this.selectExchangeName,
                BuiltinExchangeType.DIRECT,
                DURABLE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );
        this.channelIn.exchangeDeclare(
                this.refreshExchangeName,
                BuiltinExchangeType.DIRECT,
                DURABLE,
                NO_AUTODELETE,
                NULL_ARGUMENTS
        );

        this.channelIn.queueDeclare(
                this.queueInName,
                NO_DURABLE,
                EXCLUSIVE,
                AUTODELETE,
                NULL_ARGUMENTS
        );

        this.channelIn.queueBind(
                this.queueInName,
                this.selectExchangeName,
                this.queueInName // binding key
        );

        Statement.Options.Select selectOptions = (Statement.Options.Select) statement.getOptions();
        this.view = new View(
                selectOptions.getOutputType(),
                selectOptions.getProjection(),
                statement.getSources(),
                selectOptions.getPredicate(),
                selectOptions.getGroupByList(),
                selectOptions.getOrderByMap(),
                selectOptions.getLimit(),
                selectOptions.getOffset()
        );
        Set<String> tables = new HashSet<>();
        for (Source source : this.statement.getSources()) {
            tables.addAll(source.getTables());
        }
        for (String table : tables) {
            this.channelIn.queueBind(
                    this.queueInName,
                    this.refreshExchangeName,
                    table // binding key
            );
        }

        this.consumerIn = new DefaultConsumer(channelIn) {

            @Override
            public void handleDelivery(String consumerTag,
                                       Envelope envelope,
                                       AMQP.BasicProperties properties,
                                       byte[] body) throws IOException {
                try {

                    String command = new String(body, "UTF-8");
                    if (DESTROY_COMMAND.equals(command)) {
                        close();
                    } else {
//                        SelectResponse.SelectResponsePB response = exec();
                        DbDataset.Dataset response = exec();

                        //                    SelectResponsePrinter.print(response);
                        channelOut.basicPublish(
                                outExchangeName,
                                queueInName,
                                properties, /* TODO: does it simple proxies? */
                                response.toByteArray()
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    System.out.println(" [Selector] Done");
                    /*TODO: auto acknowledge?*/
//                    channelIn.basicAck(envelope.getDeliveryTag(), NO_MULTIPLE);
                }
            }
        };

        this.channelIn.basicConsume(
                this.queueInName,
                AUTOACK, /*TODO: auto acknowledge?*/
                this.consumerIn
        );

        System.out.println(" [Selector] Waiting for messages...");
    }

//    private SelectResponse.SelectResponsePB exec() throws Exception {
    private DbDataset.Dataset exec() throws Exception {
        KuduClient kuduClient = new KuduClient.KuduClientBuilder(kuduMasterURL).build();
        try {
            return StatementExecutor.select(kuduClient, view);
        } finally {
            kuduClient.shutdown();
        }
    }

    public void close() throws IOException, TimeoutException {
        if (channelIn != null && channelIn.isOpen()) {
            channelIn.close();
        }
        if (channelOut != null && channelOut.isOpen()) {
            channelOut.close();
        }
        Pool.getInstance(Pool.Name.SELECTOR).release(this);
    }
}
