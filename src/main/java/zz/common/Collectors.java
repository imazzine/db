package zz.common;

import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;

public abstract class Collectors {

    /**
     * Returns a {@code Collector} that produces the arithmetic mean of an integer-valued
     * function applied to the input elements.  If no elements are present,
     * the result is not 0 but null according to SQL conventions.
     *
     * null values are skipped according to SQL conventions.
     *
     * @param <T> the type of the input elements
     * @param mapper a function extracting the property to be summed
     * @return a {@code Collector} that produces the sum of a derived property
     */
    public static <T> Collector<T, ?, Double>
    averagingInt(ToIntFunction<? super T> mapper) {
        return Collector.of(

                // supplier
                () -> new long[3], // We use additionally index 2 to signal that there was at least one value.

                // accumulator
                (a, t) -> {
                    if (t != null) { // Ignore nulls.
                        a[0] += mapper.applyAsInt(t);
                        a[1]++;
                        a[2] = 1L; // There were values.
                    }
                },

                // combiner
                (a, b) -> {
                    a[0] += b[0];
                    a[1] += b[1];
                    if (b[2] > 0L){
                        a[2] = 1L; // There were values.
                    }
                    return a;
                },

                // finisher
                a -> {
                    if (a[2] > 0L){ // Were there any values?
                        return (a[1] == 0) ? 0.0d : (double) a[0] / a[1];
                    }
                    return null; // Allows nulls instead double.
                }
        );
    }

    /**
     * Returns a {@code Collector} that produces the arithmetic mean of a long-valued
     * function applied to the input elements.  If no elements are present,
     * the result is not 0 but null according to SQL conventions.
     *
     * null values are skipped according to SQL conventions.
     *
     * @param <T> the type of the input elements
     * @param mapper a function extracting the property to be summed
     * @return a {@code Collector} that produces the sum of a derived property
     */
    public static <T> Collector<T, ?, Double>
    averagingLong(ToLongFunction<? super T> mapper) {
        return Collector.of(

                // supplier
                () -> new long[3], // We use additionally index 2 to signal that there was at least one value.

                // accumulator
                (a, t) -> {
                    if (t != null) { // Ignore nulls.
                        a[0] += mapper.applyAsLong(t);
                        a[1]++;
                        a[2] = 1L; // There were values.
                    }
                },

                // combiner
                (a, b) -> {
                    a[0] += b[0];
                    a[1] += b[1];
                    if (b[2] > 0L){
                        a[2] = 1L; // There were values.
                    }
                    return a;
                },

                // finisher
                a -> {
                    if (a[2] > 0L){ // Were there any values?
                        return (a[1] == 0) ? 0.0d : (double) a[0] / a[1];
                    }
                    return null; // Allows nulls instead double.
                }
        );
    }

    /**
     * Returns a {@code Collector} that produces the arithmetic mean of a double-valued
     * function applied to the input elements.  If no elements are present,
     * the result is not 0 but null according to SQL conventions.
     *
     * null values are skipped according to SQL conventions.
     *
     * <p>The average returned can vary depending upon the order in which
     * values are recorded, due to accumulated rounding error in
     * addition of values of differing magnitudes. Values sorted by increasing
     * absolute magnitude tend to yield more accurate results.  If any recorded
     * value is a {@code NaN} or the sum is at any point a {@code NaN} then the
     * average will be {@code NaN}.
     *
     * @implNote The {@code double} format can represent all
     * consecutive integers in the range -2<sup>53</sup> to
     * 2<sup>53</sup>. If the pipeline has more than 2<sup>53</sup>
     * values, the divisor in the average computation will saturate at
     * 2<sup>53</sup>, leading to additional numerical errors.
     *
     * @param <T> the type of the input elements
     * @param mapper a function extracting the property to be summed
     * @return a {@code Collector} that produces the sum of a derived property
     */
    public static <T> Collector<T, ?, Double>
    averagingDouble(ToDoubleFunction<? super T> mapper) {
        /*
         * In the arrays allocated for the collect operation, index 0
         * holds the high-order bits of the running sum, index 1 holds
         * the low-order bits of the sum computed via compensated
         * summation, and index 2 holds the number of values seen.
         *
         * !!! We use additionally index 4 to signal that there was at least one value.
         */
        return Collector.of(

                // supplier
                () -> new double[5],

                // accumulator
                (a, t) -> {
                    if (t != null) { // Ignore nulls.
                        sumWithCompensation(a, mapper.applyAsDouble(t));
                        a[2]++;
                        a[3] += mapper.applyAsDouble(t);
                        a[4] = 1D; // There were values.
                    }
                },

                // combiner
                (a, b) -> {
                    sumWithCompensation(a, b[0]);
                    sumWithCompensation(a, b[1]);
                    a[2] += b[2];
                    a[3] += b[3];
                    if (b[4] > 0D){
                        a[4] = 1D; // There were values.
                    }
                    return a;
                },

                // finisher
                a -> {
                    if (a[4] > 0D){ // Were there any values?
                        return (a[2] == 0) ? 0.0d : (computeFinalSum(a) / a[2]);
                    }
                    return null; // Allows nulls instead double.
                }
        );
    }

    /**
     * Returns a {@code Collector} that produces the sum of a integer-valued
     * function applied to the input elements.  If no elements are present,
     * the result is not 0 but null according to SQL conventions.
     *
     * null values are skipped according to SQL conventions.
     *
     * @param <T> the type of the input elements
     * @param mapper a function extracting the property to be summed
     * @return a {@code Collector} that produces the sum of a derived property
     */
    public static <T> Collector<T, ?, Integer>
    summingInt(ToIntFunction<? super T> mapper) {
        return Collector.of(

                // supplier
                () -> new int[2], // We use additionally index 1 to signal that there was at least one value.

                // accumulator
                (a, t) -> {
                    if (t != null) { // Ignore nulls.
                        a[0] += mapper.applyAsInt(t);
                        a[1] = 1;
                    }
                },

                // combiner
                (a, b) -> {
                    a[0] += b[0];
                    if (b[1] > 0){
                        a[1] = 1; // There were values.
                    }
                    return a;
                },

                // finisher
                a -> {
                    if (a[1] > 0){ // Were there any values?
                        return a[0];
                    }
                    return null; // Allows nulls instead int.
                }
        );
    }

    /**
     * Returns a {@code Collector} that produces the sum of a long-valued
     * function applied to the input elements.  If no elements are present,
     * the result is not 0 but null according to SQL conventions.
     *
     * null values are skipped according to SQL conventions.
     *
     * @param <T> the type of the input elements
     * @param mapper a function extracting the property to be summed
     * @return a {@code Collector} that produces the sum of a derived property
     */
    public static <T> Collector<T, ?, Long>
    summingLong(ToLongFunction<? super T> mapper) {
        return Collector.of(

                // supplier
                () -> new long[2], // We use additionally index 1 to signal that there was at least one value.

                // accumulator
                (a, t) -> {
                    if (t != null) { // Ignore nulls.
                        a[0] += mapper.applyAsLong(t);
                        a[1] = 1L;
                    }
                },

                // combiner
                (a, b) -> {
                    a[0] += b[0];
                    if (b[1] > 0L){
                        a[1] = 1L; // There were values.
                    }
                    return a;
                },

                // finisher
                a -> {
                    if (a[1] > 0L){ // Were there any values?
                        return a[0];
                    }
                    return null; // Allows nulls instead long.
                }
        );
    }

    /**
     * Returns a {@code Collector} that produces the sum of a double-valued
     * function applied to the input elements.  If no elements are present,
     * the result is not 0 but null according to SQL conventions.
     *
     * null values are skipped according to SQL conventions.
     *
     * <p>The sum returned can vary depending upon the order in which
     * values are recorded, due to accumulated rounding error in
     * addition of values of differing magnitudes. Values sorted by increasing
     * absolute magnitude tend to yield more accurate results.  If any recorded
     * value is a {@code NaN} or the sum is at any point a {@code NaN} then the
     * sum will be {@code NaN}.
     *
     * @param <T> the type of the input elements
     * @param mapper a function extracting the property to be summed
     * @return a {@code Collector} that produces the sum of a derived property
     */
    public static <T> Collector<T, ?, Double>
    summingDouble(ToDoubleFunction<? super T> mapper) {
        /*
         * In the arrays allocated for the collect operation, index 0
         * holds the high-order bits of the running sum, index 1 holds
         * the low-order bits of the sum computed via compensated
         * summation, and index 2 holds the simple sum used to compute
         * the proper result if the stream contains infinite values of
         * the same sign.
         *
         * !!! We use additionally index 3 to signal that there was at least one value.
         */
        return Collector.of(

                // supplier
                () -> new double[4],

                // accumulator
                (a, t) -> {
                    if (t != null) { // Ignore nulls.
                        sumWithCompensation(a, mapper.applyAsDouble(t));
                        a[2] += mapper.applyAsDouble(t);
                        a[3] = 1D; // There were values.
                    }
                },

                // combiner
                (a, b) -> {
                    sumWithCompensation(a, b[0]);
                    a[2] += b[2];
                    if (b[3] > 0D){
                        a[3] = 1D; // There were values.
                    }
                    return sumWithCompensation(a, b[1]);
                },

                // finisher
                a -> {
                    if (a[3] > 0D){ // Were there any values?
                        return computeFinalSum(a);
                    }
                    return null; // Allows nulls instead double.
                }
        );
    }

    /**
     * Incorporate a new double value using Kahan summation /
     * compensation summation.
     *
     * High-order bits of the sum are in intermediateSum[0], low-order
     * bits of the sum are in intermediateSum[1], any additional
     * elements are application-specific.
     *
     * @param intermediateSum the high-order and low-order words of the intermediate sum
     * @param value the name value to be included in the running sum
     */
    private static double[] sumWithCompensation(double[] intermediateSum, double value) {
        double tmp = value - intermediateSum[1];
        double sum = intermediateSum[0];
        double velvel = sum + tmp; // Little wolf of rounding error
        intermediateSum[1] = (velvel - sum) - tmp;
        intermediateSum[0] = velvel;
        return intermediateSum;
    }

    /**
     * If the compensated sum is spuriously NaN from accumulating one
     * or more same-signed infinite values, return the
     * correctly-signed infinity stored in the simple sum.
     */
    private static double computeFinalSum(double[] summands) {
        // Better error bounds to add both terms as the final sum
        double tmp = summands[0] + summands[1];
        double simpleSum = summands[summands.length - 1];
        if (Double.isNaN(tmp) && Double.isInfinite(simpleSum))
            return simpleSum;
        else
            return tmp;
    }
}
