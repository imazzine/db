package zz.common;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public abstract class Pool {

    public enum Name {

        COMMON(""),
        SELECTOR("selector");

        private final String name;

        Name(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static ConcurrentHashMap<String, Cache> pools;

    private Pool() {
    }

    public synchronized static Pool getInstance(String poolName) {
        if (pools == null) {
            pools = new ConcurrentHashMap<>();
        }
        Cache cache = pools.get(poolName);
        if (cache == null) {
            cache = new Cache();
            pools.put(poolName, cache);
        }
        return cache;
    }

    public static Pool getInstance(Name poolName) {
        return getInstance(poolName.getName());
    }

    public abstract Object borrow();

    public abstract void release(Object element);

    public abstract boolean add(Object element);

    public abstract boolean addAndBorrow(Object element);

    public abstract void shutdown();

    private static class Cache extends Pool {

        private ArrayList available;
        private ArrayList inUse;

        Cache() {
            available = new ArrayList();
            inUse = new ArrayList();
        }

        @Override
        public synchronized Object borrow() {
            if (available.size() > 0) {
                Object element = available.get(0);
                inUse.add(element);
                return element;
            }
            return null;
        }

        @Override
        public synchronized void release(Object element) {
            if (inUse.remove(element)) {
                available.add(element);
            }
        }

        @Override
        public synchronized boolean add(Object element) {
            if (!available.contains(element)) {
                if (!inUse.contains(element)) {
                    available.add(element);
                    return true;
                }
                return false;
            }
            return false;
        }

        @Override
        public synchronized boolean addAndBorrow(Object element) {
            if (inUse.contains(element)) {
                return false;
            }
            available.remove(element);
            inUse.add(element);
            return true;
        }

        @Override
        public synchronized void shutdown() {
            available.clear();
            inUse.clear();
        }
    }
}
