package zz.common;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Collection;

public interface Digestable {

    void updateDigest(MessageDigest md);

    static void updateDigestForCollectionMember(MessageDigest md, Collection members) {
        if (members == null || members.size() < 1) {
            ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES);
            buff.put((byte) 0);
            buff.flip();
            md.update(buff);
        } else {
            for (Object element : members) {
                ((Digestable) element).updateDigest(md);
            }
        }
    }

    static void updateDigestForNullableStrings(MessageDigest md, String... strings) {
        int capacity = 0;
        byte[][] bytes = new byte[strings.length][];
        int index = 0;
        for (String str : strings) {
            if (str == null) {
                bytes[index] = null;
                capacity += Byte.BYTES;
            } else {
                bytes[index] = str.getBytes(StandardCharsets.UTF_8);
                capacity += (Integer.BYTES + bytes[index].length);
            }
            index++;
        }
        ByteBuffer buff = ByteBuffer.allocate(capacity);
        for (int i = 0; i < bytes.length; i++) {
            byte[] b = bytes[i];
            if (b == null) {
                buff.put((byte) 0);
            } else {
                buff.putInt(b.length);
                buff.put(b);
            }
        }
        buff.flip();
        md.update(buff);
    }

    static void updateDigestForNullableDigestable(MessageDigest md, Digestable... digestables) {
        for (Digestable digestable : digestables) {
            if (digestable == null) {
                ByteBuffer buff = ByteBuffer.allocate(Byte.BYTES);
                buff.put((byte) 0);
                buff.flip();
                md.update(buff);
            } else {
                digestable.updateDigest(md);
            }
        }
    }
}
