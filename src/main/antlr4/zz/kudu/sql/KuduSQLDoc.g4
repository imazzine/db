grammar KuduSQLDoc;

statements
    : ( statement SEMICOLON* )*
    ;

statement
    : create_table_statement
    | drop_table_statement
    | insert_into_table_statement
    | select_statement
    | delete_statement
    | update_statement
    ;

delete_statement
    : DELETE FROM? table_name ( WHERE predicate_expression )?
    ;

update_statement
    : UPDATE table_name SET update_elem ( COMMA update_elem )* ( WHERE predicate_expression )?
    ;

update_elem
    : column_name '=' expression_update_elem
    ;

expression_update_elem
    : constant | column_name
    ;

select_statement
    : SELECT ( AS ( ROWS | COLUMNS ) )? select_spec* select_list
    FROM table_sources
    ( WHERE predicate_expression )?
    ( GROUP BY group_by_list )?
    ( order_by_clause )?
    ( limit_clause )?
    ;

limit_clause
    : LIMIT (
        DECIMAL_LITERAL COMMA DECIMAL_LITERAL
        | DECIMAL_LITERAL ( OFFSET DECIMAL_LITERAL )?
    )
    ;

group_by_list
    : column_expression ( COMMA column_expression )*
    ;

order_by_clause
    : ORDER BY order_by_expression ( COMMA order_by_expression )*
    ;

order_by_expression
    : column_expression ( ASC | DESC )?
    ;

predicate_expression
    : atom_predicate
    | not_predicate
    | and_predicate
    | or_predicate
    | complex_predicate
    ;

/* It is just alias for simplicity */
complex_predicate
    : or_predicate
    ;

or_predicate
    : and_predicate ( or_logical_operator and_predicate )*
    ;

and_predicate
    : not_predicate ( and_logical_operator not_predicate )*
    ;

not_predicate
    : NOT atom_predicate | atom_predicate
    ;

atom_predicate
    : predicate_element
    | LEFT_PAREN predicate_expression RIGHT_PAREN
    ;

and_logical_operator
    : AND | '&' '&'
    ;

or_logical_operator
    : OR | '|' '|'
    ;

predicate_element
    : is_null_predicate
    | is_not_null_predicate
    | in_list_predicate
    | comparison_predicate
    ;

is_null_predicate
    : not_aggregate_expression IS NULL
    ;

is_not_null_predicate
    : not_aggregate_expression IS NOT NULL
    ;

in_list_predicate
    : not_aggregate_expression IN paren_predicate_expression_list
    ;

paren_predicate_expression_list
    : LEFT_PAREN predicate_expression_list RIGHT_PAREN
    ;

predicate_expression_list
    : not_aggregate_expression (COMMA not_aggregate_expression )*
    ;

comparison_predicate
    : not_aggregate_expression comparison_operator not_aggregate_expression
    | not_aggregate_expression IS boolean_literal
    ;

comparison_operator
    : '='
    | '>'
    | '<'
    | '<' '='
    | '>' '='
    | '<' '>'
    | '!' '='
    | '<' '=' '>'
    ;

table_sources
    : table_source ( COMMA table_source )*
    ;

table_source
    : table_source_item ( join_part )*
    | LEFT_PAREN table_source_item ( join_part )* RIGHT_PAREN
    ;

table_source_item
    : table_name ( AS? ID )?
    | LEFT_PAREN table_sources RIGHT_PAREN
    ;

join_part
    : ( INNER | CROSS )? JOIN table_source_item ( ON predicate_expression )?
    | ( LEFT | RIGHT ) OUTER? JOIN table_source_item ( ON predicate_expression )?
    ;

select_list
    : ( '*' | column_expression ( AS? ID )? ) ( COMMA column_expression ( AS? ID )? )*
    ;

column_expression
    : not_aggregate_expression
    | function_call
    ;

not_aggregate_expression
    : column_name
    | ID DOT column_name
    | constant
    ;

function_call
    : aggregate_function
    /*| scalar_function LEFT_PAREN function_args? RIGHT_PAREN*/
    ;

aggregate_function
    : ( AVG | MAX | MIN | SUM ) LEFT_PAREN ( ALL | DISTINCT )? function_arg RIGHT_PAREN
    | COUNT LEFT_PAREN ( '*' | ALL? function_arg ) RIGHT_PAREN
    ;

/*scalar_function
    : function_name_base
   | ASCII | CURDATE | CURRENT_DATE | CURRENT_TIME
   | CURRENT_TIMESTAMP | CURTIME | DATE_ADD | DATE_SUB
   | IF | LOCALTIME | LOCALTIMESTAMP | MID | NOW | REPLACE
   | SUBSTR | SUBSTRING | SYSDATE | TRIM
   | UTC_DATE | UTC_TIME | UTC_TIMESTAMP
   ;*/

function_args
    : function_arg ( COMMA function_arg )*
    ;

function_arg
    : constant
    | column_name
    | ID DOT column_name
    | function_call
    ;

select_spec
    : ALL
    ;

insert_into_table_statement
    : INSERT INTO TABLE? table_name paren_column_list VALUES value_list
    ;

value_list
    : LEFT_PAREN value_typed_list RIGHT_PAREN
    ;

value_typed_list
    : value ( COMMA value )*
    ;

paren_column_list
    : LEFT_PAREN column_list RIGHT_PAREN
    ;

column_list
    : column_name ( COMMA column_name )*
    ;

drop_table_statement
    : DROP TABLE table_name
    ;

create_table_statement
    : CREATE TABLE table_name schema
    ;

schema
    : column_schema_list
    ;

column_schema_list
    : LEFT_PAREN column_schema ( COMMA column_schema )* RIGHT_PAREN
    ;

column_schema
    : column_name kudu_data_type ( kudu_column_attribute )*
    ;

kudu_data_type
    : data_type
    ;

kudu_column_attribute
    : primary_key
    | nullable
    | default_attribute
    ;

nullable
    : not_null_attribute | null_attribute
    ;

null_attribute
    : NULL;

not_null_attribute
    : NOT NULL
    ;

default_attribute
    : DEFAULT value
    ;

value
    : NULL | constant
    ;

paren_constant_list
    : LEFT_PAREN constant_list RIGHT_PAREN
    ;

constant_list
    : constant (COMMA constant )*
    ;

constant
    : string_literal
    | REAL_LITERAL
    | DECIMAL_LITERAL
    | boolean_literal
    ;

string_literal
    : STRING_LITERAL
    ;

boolean_literal
    : TRUE | FALSE
    ;

primary_key
    : PRIMARY? KEY
    ;

table_name
    : ID
    ;

column_name
    : ID
    ;

data_type
    : FIELD_TYPE
    ;

/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

CREATE: ('C'|'c')('R'|'r')('E'|'e')('A'|'a')('T'|'t')('E'|'e');
DROP: ('D'|'d')('R'|'r')('O'|'o')('P'|'p');
INSERT: ('I'|'i')('N'|'n')('S'|'s')('E'|'e')('R'|'r')('T'|'t');
INTO: ('I'|'i')('N'|'n')('T'|'t')('O'|'o');
TABLE: ('T'|'t')('A'|'a')('B'|'b')('L'|'l')('E'|'e');
VALUES: ('V'|'v')('A'|'a')('L'|'l')('U'|'u')('E'|'e')('S'|'s');
DELETE: ('D'|'d')('E'|'e')('L'|'l')('E'|'e')('T'|'t')('E'|'e');
UPDATE: ('U'|'u')('P'|'p')('D'|'d')('A'|'a')('T'|'t')('E'|'e');
SET: ('S'|'s')('E'|'e')('T'|'t');
SELECT: ('S'|'s')('E'|'e')('L'|'l')('E'|'e')('C'|'c')('T'|'t');
AS: ('A'|'a')('S'|'s');
ROWS: ('R'|'r')('O'|'o')('W'|'w')('S'|'s');
COLUMNS: ('C'|'c')('O'|'o')('L'|'l')('U'|'u')('M'|'m')('N'|'n')('S'|'s');
ALL: ('A'|'a')('L'|'l')('L'|'l');
FROM: ('F'|'f')('R'|'r')('O'|'o')('M'|'m');
WHERE: ('W'|'w')('H'|'h')('E'|'e')('R'|'r')('E'|'e');
IN: ('I'|'i')('N'|'n');
IS: ('I'|'i')('S'|'s');
AND: ('A'|'a')('N'|'n')('D'|'d');
OR: ('O'|'o')('R'|'r');
ORDER: ('O'|'o')('R'|'r')('D'|'d')('E'|'e')('R'|'r');
BY: ('B'|'b')('Y'|'y');
GROUP: ('G'|'g')('R'|'r')('O'|'o')('U'|'u')('P'|'p');
ASC: ('A'|'a')('S'|'s')('C'|'c');
DESC: ('D'|'d')('E'|'e')('S'|'s')('C'|'c');
AVG: ('A'|'a')('V'|'v')('G'|'g');
MAX: ('M'|'m')('A'|'a')('X'|'x');
MIN: ('M'|'m')('I'|'i')('N'|'n');
SUM: ('S'|'s')('U'|'u')('M'|'m');
COUNT: ('C'|'c')('O'|'o')('U'|'u')('N'|'n')('T'|'t');
DISTINCT: ('D'|'d')('I'|'i')('S'|'s')('T'|'t')('I'|'i')('N'|'n')('C'|'c')('T'|'t');
INNER: ('I'|'i')('N'|'n')('N'|'n')('E'|'e')('R'|'r');
CROSS: ('C'|'c')('R'|'r')('O'|'o')('S'|'s')('S'|'s');
LEFT: ('L'|'l')('E'|'e')('F'|'f')('T'|'t');
RIGHT: ('R'|'r')('I'|'i')('G'|'g')('H'|'h')('T'|'t');
OUTER: ('O'|'o')('U'|'u')('T'|'t')('E'|'e')('R'|'r');
JOIN: ('J'|'j')('O'|'o')('I'|'i')('N'|'n');
ON: ('O'|'o')('N'|'n');
LIMIT: ('L'|'l')('I'|'i')('M'|'m')('I'|'i')('T'|'t');
OFFSET: ('O'|'o')('F'|'f')('F'|'f')('S'|'s')('E'|'e')('T'|'t');
fragment INT8: 'int8';
fragment INT16: 'int16';
fragment INT32: 'int32';
fragment INT64: 'int64';
fragment BINARY: 'binary';
fragment STRING: 'string';
fragment BOOL: 'bool';
fragment FLOAT: 'float';
fragment DOUBLE: 'double';
fragment UNIXTIME_MICROS: 'unixtime_micros';
FIELD_TYPE: (INT8|INT16|INT32|INT64|BINARY|STRING|BOOL|FLOAT|DOUBLE|UNIXTIME_MICROS);
PRIMARY: ('P'|'p')('R'|'r')('I'|'i')('M'|'m')('A'|'a')('R'|'r')('Y'|'y');
KEY: ('K'|'k')('E'|'e')('Y'|'y');
NULL: ('N'|'n')('U'|'u')('L'|'l')('L'|'l');
NOT: ('N'|'n')('O'|'o')('T'|'t');
DEFAULT: ('D'|'d')('E'|'e')('F'|'f')('A'|'a')('U'|'u')('L'|'l')('T'|'t');
TRUE: ('T'|'t')('R'|'r')('U'|'u')('E'|'e');
FALSE: ('F'|'f')('A'|'a')('L'|'l')('S'|'s')('E'|'e');

STRING_LITERAL: DQUOTA_STRING | SQUOTA_STRING;
DECIMAL_LITERAL: '-'? DEC_DIGIT+;
REAL_LITERAL: '-'? (DEC_DIGIT+)? '.' DEC_DIGIT+
    | '-'? DEC_DIGIT+ '.' EXPONENT_NUM_PART
    | '-'? (DEC_DIGIT+)? '.' (DEC_DIGIT+ EXPONENT_NUM_PART)
    | '-'? DEC_DIGIT+ EXPONENT_NUM_PART;
fragment EXPONENT_NUM_PART: ('E'|'e') '-'? DEC_DIGIT+;
fragment DEC_DIGIT: [0-9];
fragment DQUOTA_STRING: '"' ( '\\'. | '""' | ~('"'| '\\') )* '"';
fragment SQUOTA_STRING: '\'' ('\\'. | '\'\'' | ~('\'' | '\\'))* '\'';


LEFT_PAREN : '(';
RIGHT_PAREN : ')';
COMMA : ',';
SEMICOLON : ';';
DOT :    '.';
NUMBER  :   (DIGIT)+;
ID  : (('a'..'z'|'A'..'Z' | '_' | '-') ((DIGIT)*))+ ;
NEWLINE:'\r'? '\n' ;
WS : ( '\t' | ' ' | '\r' | '\n' | '\u000C' )+ -> channel(HIDDEN);
fragment DIGIT :   '0'..'9' ;