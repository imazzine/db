grammar KuduSQL;

@header {

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.kudu.ColumnSchema;
import org.apache.kudu.Schema;
import org.apache.kudu.Type;
import org.apache.kudu.client.CreateTableOptions;
import org.apache.kudu.client.KuduPredicate;

import zz.kudu.Statement;
import zz.kudu.Expression;
import zz.kudu.Function;
import zz.kudu.Predicate;
import zz.kudu.Projection;
import zz.kudu.Source;
import zz.kudu.sql.SelectResponse;
}

@parser::members {

    public List getStatements(){
        return statements().statementList;
    }
}

statements returns [ List<Statement> statementList ]
    : { $statementList = new ArrayList<Statement>(); }
    ( statement{
        $statementList.add( $statement.someStatement );
    } SEMICOLON* )*
    ;

statement returns [ Statement someStatement ]
    : create_table_statement {
        $someStatement = $create_table_statement.createTableStatement;
    } | drop_table_statement {
        $someStatement = $drop_table_statement.dropTableStatement;
    } | insert_into_table_statement{
             $someStatement = $insert_into_table_statement.insertIntoTableStatement;
    } | select_statement {
            $someStatement = $select_statement.selectStatement;
    } | delete_statement {
            $someStatement = $delete_statement.deleteStatement;
    } | update_statement {
            $someStatement = $update_statement.updateStatement;
    } | show_statement {
        $someStatement = $show_statement.showStatement;
    } | decribe_statement {
        $someStatement = $decribe_statement.decribeStatement;
    }
    ;

show_statement returns [ Statement showStatement ]
    : show_tables_statement { $showStatement = $show_tables_statement.showTablesStatement; }
    | show_columns_statement { $showStatement = $show_columns_statement.showColumnsStatement; }
    ;

show_tables_statement returns [ Statement showTablesStatement ]
    locals [ Statement.Options.ShowTables showTablesSpecification = new Statement.Options.ShowTables(); ]
    : SHOW TABLES {
        List<Source> sources = null;
        $showTablesStatement =
            new Statement( Statement.Type.SHOW_TABLES, sources, $showTablesSpecification );
    }
    (
        ( LIKE? string_literal {
            $showTablesSpecification.setFilter($string_literal.stringValue);
        } | WHERE predicate_expression {
            $showTablesSpecification.setPredicate($predicate_expression.predicate);
        } )
    )?
    ;

/* TODO: like */
show_columns_statement returns [ Statement showColumnsStatement ]
    locals [ Statement.Options.ShowColumns showColumnsSpecification = null; ]
    : SHOW ( COLUMNS | FIELDS ) ( FROM | IN ) table_name {
        $showColumnsSpecification = new Statement.Options.ShowColumns($table_name.text);
        List<Source> sources = null;
        $showColumnsStatement =
            new Statement( Statement.Type.SHOW_COLUMNS, sources, $showColumnsSpecification );
    }
    ( WHERE predicate_expression {
            $showColumnsSpecification.setPredicate($predicate_expression.predicate);
    } )?
    ;

decribe_statement returns [ Statement decribeStatement ]
    locals [ Statement.Options.ShowColumns showColumnsSpecification = null; ]
    : ( DESCRIBE | DESC ) table_name {
        $showColumnsSpecification = new Statement.Options.ShowColumns($table_name.text);
        List<Source> sources = null;
        $decribeStatement = new Statement( Statement.Type.SHOW_COLUMNS, sources, $showColumnsSpecification );
    } (
        ( DOT? column_name {
            $showColumnsSpecification.setFilter($column_name.colName);
        } | string_literal {
            $showColumnsSpecification.setFilter($string_literal.stringValue);
        })
    )?
    ;

delete_statement returns [ Statement deleteStatement ]
    locals [ Statement.Options.Delete deleteSpecification = new Statement.Options.Delete(); ]
    : DELETE FROM? table_name {
        $deleteStatement =
            new Statement( Statement.Type.DELETE, $table_name.text, $deleteSpecification );
    }
    ( WHERE predicate_expression {
        $deleteSpecification.setPredicate($predicate_expression.predicate);
    } )?
    ;

update_statement returns [ Statement updateStatement ]
    locals [ Statement.Options.Update updateSpecification = new Statement.Options.Update(); ]
    : UPDATE table_name SET update_elem [ $updateSpecification.getUpdateMap() ] {
        $updateStatement =
            new Statement( Statement.Type.UPDATE, $table_name.text, $updateSpecification );
    }
    ( COMMA update_elem [ $updateSpecification.getUpdateMap() ] )*
    ( WHERE predicate_expression {
        $updateSpecification.setPredicate($predicate_expression.predicate);
    } )?
    ;

update_elem [ Map<String, Expression> updateElements ]
    : column_name '=' expression_update_elem {
        $updateElements.put($column_name.text, $expression_update_elem.expression);
    }
    ;

expression_update_elem returns [ Expression expression ]
    : constant [ Type.DOUBLE ] { $expression = new Expression(Expression.Type.CONSTANT, $constant.objectValue); }
    | column_name { $expression = new Expression(Expression.Type.COLUMN, $column_name.text); }
    ;

select_statement returns [ Statement selectStatement ]
    locals [ Statement.Options.Select querySpecification = new Statement.Options.Select(); ]
    : {
        $querySpecification.setOutputType(SelectResponse.OutputType.COLUMNS);
    }
    SELECT ( AS ( ROWS {
        $querySpecification.setOutputType(SelectResponse.OutputType.ROWS);
    } | COLUMNS {
        $querySpecification.setOutputType(SelectResponse.OutputType.COLUMNS);
    } ) )?
    select_spec*
    projection { $querySpecification.setProjection($projection.projectionElements); }
    FROM table_sources {
        $selectStatement = new Statement(Statement.Type.SELECT, $table_sources.tableSources, $querySpecification);
    }
    ( WHERE predicate_expression {
        $querySpecification.setPredicate($predicate_expression.predicate);
    } )?
    ( GROUP BY group_by_list {
        $querySpecification.setGroupByList($group_by_list.groupByList);
    } )?
    ( order_by_clause {
        $querySpecification.setOrderByMap($order_by_clause.orderByMap);
    } )?
    ( limit_clause [$querySpecification] )?
    ;

limit_clause [ Statement.Options.Select querySpecification ]
    : LIMIT ( offset = DECIMAL_LITERAL COMMA limit = DECIMAL_LITERAL {
        $querySpecification.setLimit(Long.valueOf( $limit.text ));
        $querySpecification.setOffset(Long.valueOf( $offset.text ));
    } | limit = DECIMAL_LITERAL {
        $querySpecification.setLimit(Long.valueOf( $limit.text ));
    } ( OFFSET offset = DECIMAL_LITERAL {
        $querySpecification.setOffset(Long.valueOf( $offset.text ));
    } )? )
    ;

group_by_list returns [ List<Expression> groupByList ]
    : { $groupByList = new ArrayList<Expression>(); }
    ( column_expression {
        $groupByList.add($column_expression.expression);
    } )
    ( COMMA column_expression {
        $groupByList.add($column_expression.expression);
    } )*
    ;

order_by_clause returns [ Map orderByMap ]
    : { $orderByMap = new LinkedHashMap<Expression, Integer>(); }
    ORDER BY order_by_expression {
        $orderByMap.put(
            (Expression)((Object[])$order_by_expression.orderPair)[0],
            (Integer)((Object[])$order_by_expression.orderPair)[1]
        );
    } ( COMMA order_by_expression {
        $orderByMap.put(
            (Expression)((Object[])$order_by_expression.orderPair)[0],
            (Integer)((Object[])$order_by_expression.orderPair)[1]
        );
    } )*
    ;

order_by_expression returns [ Object orderPair ]
    : column_expression {
        $orderPair = new Object[2];
        ((Object[])$orderPair)[0] = $column_expression.expression;
        ((Object[])$orderPair)[1] = 1;
    } (ASC { ((Object[])$orderPair)[1] = 1; } | DESC { ((Object[])$orderPair)[1] = -1; })?
    ;

predicate_expression returns [ Predicate predicate ]
    : atom_predicate { $predicate = $atom_predicate.predicate; }
    | not_predicate { $predicate = $not_predicate.predicate; }
    | and_predicate { $predicate = $and_predicate.predicate; }
    | or_predicate { $predicate = $or_predicate.predicate; }
    | complex_predicate { $predicate = $complex_predicate.predicate; }
    ;

/* It is just alias for simplicity */
complex_predicate returns [ Predicate predicate ]
    : or_predicate { $predicate = $or_predicate.predicate; }
    ;

or_predicate returns [ Predicate predicate ]
    : and_predicate {
        $predicate = new Predicate.Or($and_predicate.predicate);
    } (or_logical_operator and_predicate {
        $predicate = $predicate.or($and_predicate.predicate);
    } )*
    ;

and_predicate returns [ Predicate predicate ]
    : not_predicate {
        $predicate = new Predicate.And($not_predicate.predicate);
    } (and_logical_operator not_predicate {
        $predicate = $predicate.and($not_predicate.predicate);
    } )*
    ;

not_predicate returns [ Predicate predicate ]
    : NOT atom_predicate {
        $predicate = new Predicate.Not($atom_predicate.predicate);
    }
    | atom_predicate { $predicate = $atom_predicate.predicate; }
    ;

atom_predicate returns [ Predicate predicate ]
    : predicate_element { $predicate = $predicate_element.predicateElement; }
    | LEFT_PAREN predicate_expression {
        $predicate = $predicate_expression.predicate;
    } RIGHT_PAREN
    ;

and_logical_operator
    : AND | '&' '&'
    ;

or_logical_operator
    : OR | '|' '|'
    ;

predicate_element returns [ Predicate predicateElement ]
    : is_null_predicate { $predicateElement = $is_null_predicate.isNullPredicate; }
    | is_not_null_predicate { $predicateElement = $is_not_null_predicate.isNotNullPredicate; }
    | in_list_predicate { $predicateElement = $in_list_predicate.inListPredicate; }
    | comparison_predicate { $predicateElement = $comparison_predicate.comparisonPredicate; }
    ;

is_null_predicate returns [ Predicate isNullPredicate ]
    : not_aggregate_expression IS NULL {
        $isNullPredicate = new Predicate.IsNull(
            $not_aggregate_expression.expression
        );
    }
    ;

is_not_null_predicate returns [ Predicate isNotNullPredicate ]
    : not_aggregate_expression IS NOT NULL {
        $isNotNullPredicate = new Predicate.IsNotNull(
            $not_aggregate_expression.expression
        );
    }
    ;

in_list_predicate returns [ Predicate inListPredicate ]
    : not_aggregate_expression IN paren_predicate_expression_list {
        $inListPredicate = new Predicate.InList(
            $not_aggregate_expression.expression,
            $paren_predicate_expression_list.expressionList
        );
    }
    ;

paren_predicate_expression_list returns [ List<Expression> expressionList ]
    : LEFT_PAREN predicate_expression_list RIGHT_PAREN { $expressionList = $predicate_expression_list.expressionList; }
    ;

predicate_expression_list returns [ List<Expression> expressionList ]
    : { $expressionList = new ArrayList<>(); }
    not_aggregate_expression { $expressionList.add( $not_aggregate_expression.expression ); }
    (COMMA not_aggregate_expression { $expressionList.add( $not_aggregate_expression.expression ); } )*
    ;

comparison_predicate returns [ Predicate comparisonPredicate ]
    : left = not_aggregate_expression comparison_operator right = not_aggregate_expression {
        $comparisonPredicate = new Predicate.Comparison(
            $left.expression,
            $comparison_operator.comparisonOperator,
            $right.expression
        );
    }
    | not_aggregate_expression IS boolean_literal {
        $comparisonPredicate = new Predicate.Comparison(
            $not_aggregate_expression.expression,
            Predicate.Comparison.Operator.EQUAL,
            new Expression(Expression.Type.CONSTANT, $boolean_literal.booleanValue)
        );
    }
    ;

comparison_operator returns [ Predicate.Comparison.Operator comparisonOperator ]
    : '=' { $comparisonOperator = Predicate.Comparison.Operator.EQUAL; }
    | '>' { $comparisonOperator = Predicate.Comparison.Operator.GREATER; }
    | '<' { $comparisonOperator = Predicate.Comparison.Operator.LESS; }
    | '<' '=' { $comparisonOperator = Predicate.Comparison.Operator.LESS_EQUAL; }
    | '>' '=' { $comparisonOperator = Predicate.Comparison.Operator.GREATER_EQUAL; }
    | '<' '>' { $comparisonOperator = Predicate.Comparison.Operator.NOT_EQUAL; }
    | '!' '=' { $comparisonOperator = Predicate.Comparison.Operator.NOT_EQUAL; }
    | '<' '=' '>' { $comparisonOperator = Predicate.Comparison.Operator.NOT_EQUAL; }
    ;

table_sources returns [ List<Source> tableSources ]
    : { $tableSources = new ArrayList<Source>(); }
    table_source { $tableSources.add($table_source.tableSource); }
    ( COMMA table_source { $tableSources.add($table_source.tableSource); } )*
    ;

table_source returns [ Source tableSource ]
    : table_source_item {
        $tableSource = new Source($table_source_item.tableSourceItem);
    } ( join_part { $tableSource.appendJoin($join_part.join); } )*
    | LEFT_PAREN table_source_item {
        $tableSource = new Source($table_source_item.tableSourceItem);
    } ( join_part { $tableSource.appendJoin($join_part.join); } )* RIGHT_PAREN
    ;

table_source_item returns [ Source.Item tableSourceItem ]
    locals [ List<String> valuesAliases = new ArrayList<>(); ]
    : table_name { $tableSourceItem = new Source.Item.Table($table_name.text); }
    ( AS? ID { $tableSourceItem.setAlias($ID.text); } )?

    | ( subquery { $tableSourceItem = new Source.Item.Subquery($subquery.selectStatement); }
        | LEFT_PAREN subquery { $tableSourceItem = new Source.Item.Subquery($subquery.selectStatement); } RIGHT_PAREN
    ) AS? ID { $tableSourceItem.setAlias($ID.text); }

    | LEFT_PAREN table_sources {
        $tableSourceItem = new Source.Item.Sources($table_sources.tableSources);
    } RIGHT_PAREN

    | values_source_item { $tableSourceItem = $values_source_item.valuesItem; }

    | LEFT_PAREN values_source_item {
        $tableSourceItem = $values_source_item.valuesItem;
    } RIGHT_PAREN ( AS? ID { $tableSourceItem.setAlias($ID.text); }
    ( LEFT_PAREN ID { $valuesAliases.add($ID.text); } ( COMMA ID { $valuesAliases.add($ID.text); } )* RIGHT_PAREN {
        ((Source.Item.Values)$tableSourceItem).setColumnAliases($valuesAliases.toArray(new String[0]));
    } )? )?
    ;

/* TODO: throws */
values_source_item returns [ Source.Item.Values valuesItem ]
    locals [ List<Expression> firstRow = new ArrayList<>(); ]
    : ( VALUES | VALUE )
    (
        value_expression ( AS? ID { $value_expression.expression.setAlias($ID.text); } )? {
            $firstRow.add($value_expression.expression);
        }
        ( COMMA value_expression ( AS? ID { $value_expression.expression.setAlias($ID.text); } )? {
            $firstRow.add($value_expression.expression);
        } )* { try{$valuesItem = new Source.Item.Values($firstRow);}catch(Exception e){} }

        | LEFT_PAREN value_expression ( AS? ID { $value_expression.expression.setAlias($ID.text); } )? {
            $firstRow.add($value_expression.expression);
        }
        ( COMMA value_expression ( AS? ID { $value_expression.expression.setAlias($ID.text); } )? {
            $firstRow.add($value_expression.expression);
        } )*
        RIGHT_PAREN { try{$valuesItem = new Source.Item.Values($firstRow);}catch(Exception e){} }
        ( COMMA paren_values_element [ $valuesItem ] )*
    )
    ;

paren_values_element [ Source.Item.Values valuesItem ]
    : LEFT_PAREN value_expression { $valuesItem.add($value_expression.expression); }
    ( COMMA value_expression { $valuesItem.add($value_expression.expression); } )* RIGHT_PAREN
    ;

/* TODO: scalar function call */
value_expression returns [ Expression expression ]
    : constant [ Type.DOUBLE ] { $expression = new Expression(Expression.Type.CONSTANT, $constant.objectValue); }
    ;

subquery returns [ Statement selectStatement ]
    : select_statement { $selectStatement = $select_statement.selectStatement; }
    ;

join_part returns [ Source.Join join ]
    locals [ Source.Join.Type type = Source.Join.Type.INNER; ]
    : ( INNER | CROSS { $type = Source.Join.Type.CROSS; } )? JOIN
    table_source_item {
        $join = new Source.Join($type, $table_source_item.tableSourceItem);
    }
    (
        ON predicate_expression { $join.setPredicate($predicate_expression.predicate); }
        /*| USING '(' id_list ')'*/
    )?
    | ( LEFT { $type = Source.Join.Type.LEFT; } | RIGHT { $type = Source.Join.Type.RIGHT; } ) OUTER? JOIN
    table_source_item {
        $join = new Source.Join($type, $table_source_item.tableSourceItem);
    }
    (
        ON predicate_expression { $join.setPredicate($predicate_expression.predicate); }
        /*| USING '(' id_list ')'*/
    )?
    /*| NATURAL ((LEFT | RIGHT) OUTER?)? JOIN table_source_item*/
    ;

projection returns [ Projection projectionElements ]
    : { $projectionElements = new Projection(); }
    projection_element { $projectionElements.add($projection_element.element); }
    ( COMMA projection_element { $projectionElements.add($projection_element.element); } )*
    ;

projection_element returns [ Projection.Element element ]
    : column_expression ( AS? ID { $column_expression.expression.setAlias($ID.text); } )? {
        $element = $column_expression.expression;
    }
    | asterisk_expression { $element = $asterisk_expression.element; }
    ;

asterisk_expression returns [ Projection.Element element ]
    : '*' { $element = new Projection.Element.Asterisk(); }
    | ID DOT '*' {
        $element = new Projection.Element.Asterisk();
        $element.setSourceItemAlias($ID.text);
    }
    ;

column_expression returns [ Expression expression ]
    : not_aggregate_expression { $expression = $not_aggregate_expression.expression; }
    | function_call { $expression = new Expression(Expression.Type.FUNCTION, $function_call.function); }
    ;

not_aggregate_expression returns [ Expression expression ]
    : column_name { $expression = new Expression(Expression.Type.COLUMN, $column_name.text); }
    | table_name DOT column_name {
        $expression = new Expression(Expression.Type.COLUMN, $column_name.text);
        $expression.setSourceItemAlias($table_name.text);
    }
    /*| function_call { $expression = new Expression(Expression.Type.FUNCTION, $function_call.function); }*/
    | constant [ Type.DOUBLE ] { $expression = new Expression(Expression.Type.CONSTANT, $constant.objectValue); }
    ;

function_call returns [ Function function ]
    : aggregate_function { $function = $aggregate_function.aggregateFunction; }
    /*| scalar_function LEFT_PAREN function_args? RIGHT_PAREN*/
    ;

aggregate_function returns [ Function aggregateFunction ]
    :
    (
        AVG { $aggregateFunction = new Function(Function.Name.AVG); }
        | MAX { $aggregateFunction = new Function(Function.Name.MAX); }
        | MIN { $aggregateFunction = new Function(Function.Name.MIN); }
        | SUM { $aggregateFunction = new Function(Function.Name.SUM); }
    )
        LEFT_PAREN ( ALL | DISTINCT )? function_arg {
            $aggregateFunction.addArgument($function_arg.argument);
        } RIGHT_PAREN
    | COUNT { $aggregateFunction = new Function(Function.Name.COUNT); }
        LEFT_PAREN ( '*' | ALL? function_arg {
            $aggregateFunction.addArgument($function_arg.argument);
        } ) RIGHT_PAREN
    /*| COUNT LEFT_PAREN DISTINCT function_args RIGHT_PAREN*/
    ;

/*scalar_function
    : function_name_base
   | ASCII | CURDATE | CURRENT_DATE | CURRENT_TIME
   | CURRENT_TIMESTAMP | CURTIME | DATE_ADD | DATE_SUB
   | IF | LOCALTIME | LOCALTIMESTAMP | MID | NOW | REPLACE
   | SUBSTR | SUBSTRING | SYSDATE | TRIM
   | UTC_DATE | UTC_TIME | UTC_TIMESTAMP
   ;*/

function_args returns [ List<Expression> arguments ]
    : { $arguments = new ArrayList<Expression>(); }
    function_arg { $arguments.add($function_arg.argument); }
    ( COMMA function_arg { $arguments.add($function_arg.argument); } )*
    ;

function_arg returns [ Expression argument ]
    : constant [ Type.DOUBLE ] {
        $argument = new Expression(Expression.Type.CONSTANT, $constant.objectValue);
    } | column_name {
        $argument = new Expression(Expression.Type.COLUMN, $column_name.text);
    } | table_name DOT column_name {
        $argument = new Expression(Expression.Type.COLUMN, $column_name.text);
        $argument.setSourceItemAlias($table_name.text);
    }
    | function_call {
        $argument = new Expression(Expression.Type.FUNCTION, $function_call.function);
    } /*| expression*/
    ;

select_spec
    : ALL ;
    /*(ALL|DISTINCT|DISTINCTROW)
   | HIGH_PRIORITY | STRAIGHT_JOIN | SQL_SMALL_RESULT
   | SQL_BIG_RESULT | SQL_BUFFER_RESULT
   | (SQL_CACHE|SQL_NO_CACHE)
   | SQL_CALC_FOUND_ROWS
   ;*/

insert_into_table_statement returns [ Statement insertIntoTableStatement ]
    locals [ Statement selectStatement = null, List<String> columns = null ]
    : INSERT INTO TABLE? table_name ( paren_column_list { $columns = $paren_column_list.columnList; } )? (
        values_source_item {
            Projection projection = new Projection();
            projection.add(new Projection.Element.Asterisk());
            Statement.Options.Select selectOptions = new Statement.Options.Select(
                SelectResponse.OutputType.ROWS,
                projection,
                null, /* predicate */
                null, /* groupByList */
                null, /* orderByMap */
                -1L,  /* limit */
                0L    /* offset */
            );
            List<Source> sources = new ArrayList<>();
            sources.add(new Source($values_source_item.valuesItem));
            $selectStatement = new Statement(Statement.Type.SELECT, sources, selectOptions);
        } | select_statement {
            $selectStatement = $select_statement.selectStatement;
        }
    ) {
        $insertIntoTableStatement = new Statement(
            Statement.Type.INSERT_INTO_TABLE,
            $table_name.text,
            new Statement.Options.InsertIntoTable(
                $columns,
                $selectStatement
            )
        );
    }
    ;

paren_column_list returns [ List<String> columnList ]
    : LEFT_PAREN column_list RIGHT_PAREN { $columnList = $column_list.columnList; } ;

column_list returns [ List<String> columnList ]
    : { $columnList = new ArrayList<String>(); }
    column_name { $columnList.add( $column_name.text ); }
    ( COMMA column_name { $columnList.add( $column_name.text ); } )*
    ;

drop_table_statement returns [ Statement dropTableStatement ]
    : DROP TABLE table_name {
        $dropTableStatement =
            new Statement( Statement.Type.DROP_TABLE, $table_name.text, null );
    }
    ;

create_table_statement returns [ Statement createTableStatement ]
    : CREATE TABLE table_name schema {
        Schema schema = $schema.kudu_schema;
        List<ColumnSchema> columnSchemaList = schema.getColumns();
        List<String> rangeKeys = new ArrayList<String>();
        for( ColumnSchema columnSchema: columnSchemaList ){
            if( columnSchema.isKey() ){
                rangeKeys.add( columnSchema.getName() );
            }
        }
        CreateTableOptions createTableOptions = new CreateTableOptions()
            .setRangePartitionColumns(rangeKeys)
                .setNumReplicas( 1 );
        $createTableStatement = new Statement(
                    Statement.Type.CREATE_TABLE,
                    $table_name.text,
                    new Statement.Options.CreateTable(schema, createTableOptions)
        );
    }
    ;

schema returns[ Schema kudu_schema ]
    : col_list = column_schema_list { $kudu_schema = new Schema( $col_list.cols ); } ;

column_schema_list returns [ List cols ]
    : { $cols = new ArrayList(); }
    LEFT_PAREN col_schema = column_schema { $cols.add( $col_schema.columnSchema ); }
    ( COMMA col_schema = column_schema { $cols.add( $col_schema.columnSchema ); } )*
    RIGHT_PAREN
    ;

column_schema returns [ ColumnSchema columnSchema ]
    locals [ ColumnSchema.ColumnSchemaBuilder columnSchemaBuilder = null, boolean isPrimaryKey = false; ]
    : column_name kudu_data_type {
        $columnSchemaBuilder = new ColumnSchema.ColumnSchemaBuilder(
            $column_name.colName, $kudu_data_type.kudu_type
        ).key( false )
            .nullable( true )
                .defaultValue( null );
    }
    ( kudu_column_attribute [ $columnSchemaBuilder, $kudu_data_type.kudu_type, $isPrimaryKey ] {
        $columnSchemaBuilder = $kudu_column_attribute.columnSchemaBuilderOut;
        $isPrimaryKey = $kudu_column_attribute.isPrimaryKeyOut;
    } )*
    {
        $columnSchema = $columnSchemaBuilder.build();
    }
    ;

kudu_data_type returns [ Type kudu_type ]
    : data_type {
        if( "string".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.STRING;
        else if( "binary".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.BINARY;
        else if( "bool".equalsIgnoreCase( $data_type.tp ) || "boolean".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.BOOL;
        else if( "int8".equalsIgnoreCase( $data_type.tp ) || "integer8".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.INT8;
        else if( "int16".equalsIgnoreCase( $data_type.tp ) || "integer16".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.INT16;
        else if( "int32".equalsIgnoreCase( $data_type.tp ) || "integer32".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.INT32;
        else if( "int64".equalsIgnoreCase( $data_type.tp )
            || "integer64".equalsIgnoreCase( $data_type.tp )
            || "long".equalsIgnoreCase( $data_type.tp )
        )
            $kudu_type = Type.INT64;
        else if( "float".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.FLOAT;
        else if( "double".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.DOUBLE;
        else if( "unixtime_micros".equalsIgnoreCase( $data_type.tp ) )
            $kudu_type = Type.UNIXTIME_MICROS;
        else
            throw new IllegalArgumentException( "The provided data type doesn't map" +
                " to know any known one: " + $data_type.tp );
    }
    ;

kudu_column_attribute [ ColumnSchema.ColumnSchemaBuilder columnSchemaBuilder, Type columnType, boolean isPrimaryKey ]
    returns [ ColumnSchema.ColumnSchemaBuilder columnSchemaBuilderOut, boolean isPrimaryKeyOut ]
    : { $columnSchemaBuilderOut = $columnSchemaBuilder; $isPrimaryKeyOut = $isPrimaryKey; }
    primary_key{
        $columnSchemaBuilderOut = $columnSchemaBuilderOut.key( true ).nullable( false );
        $isPrimaryKeyOut = true;
    } | nullable{
        $columnSchemaBuilderOut = $columnSchemaBuilderOut.nullable( $isPrimaryKeyOut? false: $nullable.isNullable );
    } | default_attribute [ $columnType ]{
        $columnSchemaBuilderOut = $columnSchemaBuilderOut.defaultValue( $default_attribute.defaultValue );
    }
    ;

nullable returns [ boolean isNullable ]
    : ( not_null_attribute { $isNullable = false; } | null_attribute { $isNullable = true; } ) ;

null_attribute
    : NULL ;

not_null_attribute
    : NOT NULL ;

default_attribute [ Type columnType ] returns [ Object defaultValue ]
    : DEFAULT value[ $columnType ]{ $defaultValue = $value.objectValue; } ;

value [ Type columnType ] returns [ Object objectValue ]
    : NULL{ $objectValue = null; }
    | constant[ $columnType ]{ $objectValue = $constant.objectValue; }
    ;

constant [ Type columnType ] returns [ Object objectValue ]
    : string_literal{
        $objectValue = $string_literal.stringValue;
    } | REAL_LITERAL{
        switch( $columnType ){
            case DOUBLE:
                $objectValue = Double.valueOf( $REAL_LITERAL.text );
                break;
            case FLOAT:
                $objectValue = Float.valueOf( $REAL_LITERAL.text );
                break;
            default:
                $objectValue = Double.valueOf( $REAL_LITERAL.text );
        }
    } | DECIMAL_LITERAL{
        $objectValue = Integer.valueOf( $DECIMAL_LITERAL.text );
    } | boolean_literal{
        $objectValue = $boolean_literal.booleanValue;
    }
    ;

string_literal returns [ String stringValue ]
    : STRING_LITERAL{ $stringValue = $STRING_LITERAL.text.substring( 1, $STRING_LITERAL.text.length() - 1 ); } ;

boolean_literal returns [ boolean booleanValue ]
    : TRUE{ $booleanValue = true; } | FALSE{ $booleanValue = false; } ;

primary_key
    : PRIMARY? KEY ;

table_name
    : ID ;

column_name returns [ String colName ]
    :   ID { $colName = String.valueOf( $ID.text ); };

data_type returns [String tp]
    : FIELD_TYPE { $tp = $FIELD_TYPE.text; };

/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

CREATE: ('C'|'c')('R'|'r')('E'|'e')('A'|'a')('T'|'t')('E'|'e');
DROP: ('D'|'d')('R'|'r')('O'|'o')('P'|'p');
INSERT: ('I'|'i')('N'|'n')('S'|'s')('E'|'e')('R'|'r')('T'|'t');
INTO: ('I'|'i')('N'|'n')('T'|'t')('O'|'o');
TABLE: ('T'|'t')('A'|'a')('B'|'b')('L'|'l')('E'|'e');
VALUES: ('V'|'v')('A'|'a')('L'|'l')('U'|'u')('E'|'e')('S'|'s');
VALUE: ('V'|'v')('A'|'a')('L'|'l')('U'|'u')('E'|'e');
DELETE: ('D'|'d')('E'|'e')('L'|'l')('E'|'e')('T'|'t')('E'|'e');
UPDATE: ('U'|'u')('P'|'p')('D'|'d')('A'|'a')('T'|'t')('E'|'e');
SET: ('S'|'s')('E'|'e')('T'|'t');
SELECT: ('S'|'s')('E'|'e')('L'|'l')('E'|'e')('C'|'c')('T'|'t');
AS: ('A'|'a')('S'|'s');
ROWS: ('R'|'r')('O'|'o')('W'|'w')('S'|'s');
COLUMNS: ('C'|'c')('O'|'o')('L'|'l')('U'|'u')('M'|'m')('N'|'n')('S'|'s');
ALL: ('A'|'a')('L'|'l')('L'|'l');
FROM: ('F'|'f')('R'|'r')('O'|'o')('M'|'m');
WHERE: ('W'|'w')('H'|'h')('E'|'e')('R'|'r')('E'|'e');
IN: ('I'|'i')('N'|'n');
IS: ('I'|'i')('S'|'s');
AND: ('A'|'a')('N'|'n')('D'|'d');
OR: ('O'|'o')('R'|'r');
ORDER: ('O'|'o')('R'|'r')('D'|'d')('E'|'e')('R'|'r');
BY: ('B'|'b')('Y'|'y');
GROUP: ('G'|'g')('R'|'r')('O'|'o')('U'|'u')('P'|'p');
ASC: ('A'|'a')('S'|'s')('C'|'c');
DESC: ('D'|'d')('E'|'e')('S'|'s')('C'|'c');
AVG: ('A'|'a')('V'|'v')('G'|'g');
MAX: ('M'|'m')('A'|'a')('X'|'x');
MIN: ('M'|'m')('I'|'i')('N'|'n');
SUM: ('S'|'s')('U'|'u')('M'|'m');
COUNT: ('C'|'c')('O'|'o')('U'|'u')('N'|'n')('T'|'t');
DISTINCT: ('D'|'d')('I'|'i')('S'|'s')('T'|'t')('I'|'i')('N'|'n')('C'|'c')('T'|'t');
INNER: ('I'|'i')('N'|'n')('N'|'n')('E'|'e')('R'|'r');
CROSS: ('C'|'c')('R'|'r')('O'|'o')('S'|'s')('S'|'s');
LEFT: ('L'|'l')('E'|'e')('F'|'f')('T'|'t');
RIGHT: ('R'|'r')('I'|'i')('G'|'g')('H'|'h')('T'|'t');
OUTER: ('O'|'o')('U'|'u')('T'|'t')('E'|'e')('R'|'r');
JOIN: ('J'|'j')('O'|'o')('I'|'i')('N'|'n');
ON: ('O'|'o')('N'|'n');
LIMIT: ('L'|'l')('I'|'i')('M'|'m')('I'|'i')('T'|'t');
OFFSET: ('O'|'o')('F'|'f')('F'|'f')('S'|'s')('E'|'e')('T'|'t');
SHOW: ('S'|'s')('H'|'h')('O'|'o')('W'|'w');
TABLES: ('T'|'t')('A'|'a')('B'|'b')('L'|'l')('E'|'e')('S'|'s');
LIKE: ('L'|'l')('I'|'i')('K'|'k')('E'|'e');
FIELDS: ('F'|'f')('I'|'i')('E'|'e')('L'|'l')('D'|'d')('S'|'s');
DESCRIBE: ('D'|'d')('E'|'e')('S'|'s')('C'|'c')('R'|'r')('I'|'i')('B'|'b')('E'|'e');
PRIMARY: ('P'|'p')('R'|'r')('I'|'i')('M'|'m')('A'|'a')('R'|'r')('Y'|'y');
KEY: ('K'|'k')('E'|'e')('Y'|'y');
NULL: ('N'|'n')('U'|'u')('L'|'l')('L'|'l');
NOT: ('N'|'n')('O'|'o')('T'|'t');
DEFAULT: ('D'|'d')('E'|'e')('F'|'f')('A'|'a')('U'|'u')('L'|'l')('T'|'t');
TRUE: ('T'|'t')('R'|'r')('U'|'u')('E'|'e');
FALSE: ('F'|'f')('A'|'a')('L'|'l')('S'|'s')('E'|'e');

fragment INTEGER: ('I'|'i')('N'|'n')('T'|'t')( ('E'|'e')('G'|'g')('E'|'e')('R'|'r') )?;
fragment LONG: ('L'|'l')('O'|'o')('N'|'n')('G'|'g');
fragment INT8: INTEGER'8';
fragment INT16: INTEGER'16';
fragment INT32: INTEGER'32';
fragment INT64: ( INTEGER'64' | LONG );
fragment BINARY: ('B'|'b')('I'|'i')('N'|'n')('A'|'a')('R'|'r')('Y'|'y');
fragment STRING: ('S'|'s')('T'|'t')('R'|'r')('I'|'i')('N'|'n')('G'|'g');
fragment BOOL: ('B'|'b')('O'|'o')('O'|'o')('L'|'l')( ('E'|'e')('A'|'a')('N'|'n') )?;
fragment FLOAT: ('F'|'f')('L'|'l')('O'|'o')('A'|'a')('T'|'t');
fragment DOUBLE: ('D'|'d')('O'|'o')('U'|'u')('B'|'b')('L'|'l')('E'|'e');
fragment UNIXTIME_MICROS: ('U'|'u')('N'|'n')('I'|'i')('X'|'x')('T'|'t')('I'|'i')('M'|'m')('E'|'e')'_'('M'|'m')('I'|'i')('C'|'c')('R'|'r')('O'|'o')('S'|'s');
FIELD_TYPE: (INT8|INT16|INT32|INT64|BINARY|STRING|BOOL|FLOAT|DOUBLE|UNIXTIME_MICROS);

STRING_LITERAL: DQUOTA_STRING | SQUOTA_STRING;
DECIMAL_LITERAL: '-'? DEC_DIGIT+;
REAL_LITERAL: '-'? (DEC_DIGIT+)? '.' DEC_DIGIT+
    | '-'? DEC_DIGIT+ '.' EXPONENT_NUM_PART
    | '-'? (DEC_DIGIT+)? '.' (DEC_DIGIT+ EXPONENT_NUM_PART)
    | '-'? DEC_DIGIT+ EXPONENT_NUM_PART;
fragment EXPONENT_NUM_PART: ('E'|'e') '-'? DEC_DIGIT+;
fragment DEC_DIGIT: [0-9];
fragment DQUOTA_STRING: '"' ( '\\'. | '""' | ~('"'| '\\') )* '"';
fragment SQUOTA_STRING: '\'' ('\\'. | '\'\'' | ~('\'' | '\\'))* '\'';
fragment DIGIT :   '0'..'9' ;


LEFT_PAREN : '(';
RIGHT_PAREN : ')';
COMMA : ',';
SEMICOLON : ';';
DOT :    '.';
NUMBER  :   (DIGIT)+;
ID  : (('a'..'z'|'A'..'Z' | '_' | '-') ((DIGIT)*))+ ;
/* NEWLINE:'\r'? '\n'; */
/* WS : ( '\t' | ' ' | '\r' | '\n' | '\u000C' )+ -> channel(HIDDEN); */
WS: [ \t\r\n\u000C]+ -> channel(HIDDEN);